import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class FaqAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        faq: {
            faq_description: '',
            faq_title: '',
            

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        faq_categories:[],
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const faq_id = this.props.match.params.faq_id;
            this.setState({ faq_id: faq_id });
            this.setState({ faq: { faq_id: faq_id } });

            if(faq_id)
            {
                axios({
                    url: API_URL + "admin/faq/details/" + faq_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ faq: response.data.faq, showAlertActionURL: '/program/category/faq/details/' + this.state.faq_id});
                    }
                    else {
                        //fail
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            }


            //faq/categoris

            axios({
                url: API_URL + "admin/faq/categoris/",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                           
                    this.setState({ faq_categories: response.data.faq_categories});
                }
                else {
                    //fail
                    this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }
 
    handleChangetask = (e) => {
        this.setState({ faq: { ...this.state.faq, faq_title: e.target.value } });
    }
 

    //add task
    FaqAddBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.faq.faq_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter faq title." });
                return false;

            }

            if ((this.state.faq.faq_title).trim().length ==0) {
                this.setState({ faq: { ...this.state.faq, faq_title: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid faq title." });
                return false;
            }

    
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/faq/add",
                method: 'post',
                data: this.state.faq,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success 
                        const faq_id = response.data.faq.faq_id;
                        
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/faqs' });
                         
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    handleSummerNote = (e) => {
        this.setState({

            faq: {
                ...this.state.faq,
                faq_description: e
            }
        })
    }

    
    handleChangeFaqCategory = (e) => {
        this.setState({ faq: { ...this.state.faq, faq_category_id: e.target.value } });
    }

    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Topic</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>    
                                    <li className="breadcrumb-item"><NavLink to="/faqs">Help Center</NavLink></li>                                
                                    <li className="breadcrumb-item active" aria-current="page">Add Topic</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-12 col-md-12">
                        <form className='form-horizontal' onSubmit={this.FaqAddBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Title</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.faq.faq_title} name='faq_title' onChange={this.handleChangetask} required />
                                        </div>
                                    </div>                                
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Category</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group  row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='faq_category_id' onChange={this.handleChangeFaqCategory} required >
                                                <option value="" hidden selected>Please Select Category</option>
                                                 
                                                {
                                                    this.state.faq_categories.map((fc) => (
                                                        <option value={fc.faq_category_id}>{fc.category_name}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>
                                    </div>                                
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value=""

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}
                                    />
                                </div>
                            </div>
                             
                             
                            <div className="card-footer text-right">                                
                                <button type="submit" className="btn btn-brand">Add</button>
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default FaqAdd
