import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class FaqCategoryUpdate extends Component {

    state = {
        name: '',
        notes: '',
        faq_category_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        category: {
            category_description: '',
            category_title: '',
            category_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        category_permissions:[],
        category_category_permissions:[],
        members:[],
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            const faq_category_id = this.props.match.params.faq_category_id;
            this.setState({ faq_category_id: faq_category_id });
            this.setState({ category: { faq_category_id: faq_category_id } });

            if(faq_category_id)
            {
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "admin/faq/category/details/" + faq_category_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ category: response.data.faq_category_details});
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
 

        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }
    }
 
    handleChangetask = (e) => {
        this.setState({ category: { ...this.state.category, category_name: e.target.value } });
    }

    
    handleChangeStatus = (e) => {
        this.setState({ category: { ...this.state.category, category_status: e.target.value } });
    }
 

    //add task
    FaqCategoryUpdateBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.category.category_name) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter category name." });
                return false;

            }

            if ((this.state.category.category_name).trim().length ==0) {
                this.setState({ category: { ...this.state.category, category_name: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid category name." });
                return false;
            }

            this.setState({ category: { ...this.state.category, status: '1' } });
  
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/faq/category/update",
                method: 'post',
                data: this.state.category,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {

                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success 
                        
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message,  showAlertActionURL: '/faq/category'});
                    
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    handleChangecategoryStatus = (e) => {
        this.setState({ category: { ...this.state.category, category_status: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            category: {
                ...this.state.category,
                category_description: e
            }
        })
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Category Edit</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>  
                                    <li className="breadcrumb-item"><NavLink to="/faqs">FAQs</NavLink></li>                                  
                                    <li className="breadcrumb-item active" aria-current="page">Category Edit</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-12 col-md-12">
                        <form className='form-horizontal' onSubmit={this.FaqCategoryUpdateBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.category.category_name} name='category_name' onChange={this.handleChangetask} required />
                                        </div>
                                    </div>                                
                                </div>
                            </div>


                             


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value={this.state.category.category_description}

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}
                                    />
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='category_status' onChange={this.handleChangecategoryStatus}>
                                                <option selected={this.state.category.category_status == 1} value="1">Active</option>
                                                <option selected={this.state.category.category_status == 0} value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             
                             
                            <div className="card-footer text-right">                                
                                <button type="submit" className="btn btn-brand">Update</button>
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}


export default FaqCategoryUpdate
