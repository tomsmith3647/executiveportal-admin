import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class FaqCategoryList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        categories: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_faq_category_id: '',
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/faq/category/list",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ categories: response.data.faq_categories });


                        var FaqCategoryTable = $('#usersTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [5], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        FaqCategoryTable.columns().iterator('column', function (ctx, idx) {
                            $(FaqCategoryTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

 

                        FaqCategoryTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = FaqCategoryTable.row(diff[i].node).data();
                                const faq_category_id = $(rowData[1]).attr("data-id");
                                const catsort = { "faq_category_id": faq_category_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/faq/category/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });
 
                        
                    }
                    else {
                        //fail
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    //FaqCategoryDelete    
    FaqCategoryDelete = () => (e) => {
        const faq_category_id = e.currentTarget.value;
        this.setState({ selected_faq_category_id: faq_category_id });
        this.setState({ showConfirmationAlert: true });
    }

    //FaqCategoryDeleteProcess
    FaqCategoryDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const faq_category_id = this.state.selected_faq_category_id;

            axios({
                url: API_URL + "admin/faq/category/delete",
                method: 'post',
                data: {
                    faq_category_id: faq_category_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            Faq/category/module/details        
                        
                        axios({
                            url: API_URL + "admin/faq/category/list",
                            method: 'get',
                            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                        })
                            .then(response => {
                                const error_code = response.data.error_code;
                                const error_message = response.data.error_message;
                                const ApiURL = response.data.api_url;
                                this.setState({ baseApiURL: ApiURL });
                                this.setState({ showLoader: false });
                                if (error_code == '200') {
                                    //success                        
                                    this.setState({ categories: response.data.faq_categories });
            
             
             
                                    
                                }
                                else {
                                    //fail
                                    this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });

                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message});
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }

    convertUTCDateToLocalDate =(date)=> {        
        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.FaqCategoryDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Category List</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item"><NavLink to="/faqs">Help Center</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">Category List</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className="tab-pane" id="Holiday-all">

                    <div className="card-footer text-right">
                        <NavLink to="/faq/category/add" className="btn btn-brand">Add Category</NavLink>
                        
                    </div>

                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th >#</th>
                                             
                                            <th className='text-left' >Name</th>
                                            <th className='text-center'>Status</th>                                            
                                            <th className='text-center' >Created Date</th>
                                            <th className='text-center' >Last Activity</th>
                                            <th className='text-center' >Action</th> 

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.categories.map((category, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>
                                                    <td className="text-left m-l-0 text-capitalize table_content">
                                                        <NavLink data-id={category.faq_category_id} to={`/faq/category/update/${category.faq_category_id}`} className="text-info brandColor" title="View Detail">
                                                        {category.category_name}
                                                        </NavLink>
                                                    </td>
                                                    
                                                    <td className='text-center'>
                                                        {category.category_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                    </td>
                                                     
                                                    
                                                    <td className='text-center'>
                                                        <span className='dateFormat'>{(new Date(this.convertUTCDateToLocalDate(new Date(category.created_date)))).toLocaleString()}</span>
                                                        {(new Date(this.convertUTCDateToLocalDate(new Date(category.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                    </td>

                                                    <td className='text-center'>
                                                        <span className='dateFormat'>{(new Date(this.convertUTCDateToLocalDate(new Date(category.modified_date)))).toLocaleString()}</span>
                                                        {(new Date(this.convertUTCDateToLocalDate(new Date(category.modified_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                    </td>


                                                    <td className='text-center'>
                                                        
                                                        
                                                        <button value={category.faq_category_id} onClick={this.FaqCategoryDelete()} className="btn btn-icon btn-sm " title="delete"><i className="fa fa-trash text-danger"></i></button>
                                                        
                                                    </td>
                                                     
                                                </tr>
                                            ))
                                        }

                                    </tbody>
                                    {this.state.categories.length == 0 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'No Faq'}</td></tr></tfoot>)}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default FaqCategoryList
