import React, { Component,Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class UserListing extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        showAlert:false,
        showLoader:false,
        admins: [],
    }

    componentDidMount() {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });
            axios({
                url: API_URL+"admin/subadmin/list",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {                   
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ admins: response.data.admins });
                                   
                        $('#usersTable').DataTable({                          
                            'columnDefs': [ {
                               'targets': [6], // column index (start from 0)
                               'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                          });
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });                        
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }

    //subAdminDelete
    subAdminDelete = () => (e) => {
        
        const admin_id = e.currentTarget.dataset.id ;  
        this.setState({ selected_admin_id: admin_id});     
        this.setState({ showAlert: true});                
    }
 

    //subAdminDelete
    subAdminDeleteProcess = () => {
        
 
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,                 
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })  
            //const admin_id = e.currentTarget.value;
            const admin_id = this.state.selected_admin_id;
            console.log(admin_id);
             
            axios({
                url: API_URL + "admin/subadmin/delete",
                method: 'post',
                data:this.state.admin,
                data: {
                    admin_id: admin_id,                             
                },  
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                this.setState({ showAlert: false});  
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                if (error_code == '200') {
                    //success                        
                    this.setState({ showAlert: true, showAlertMessage: error_message });
                    window.location.href = '/subadmin'
                    window.location.reload();
                }
                else {
                    //fail
                    this.setState({ showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
              
                         
        }
        //Get user permissions
    }

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment> 

                
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes, delete it!"
                confirmBtnBsStyle="danger"
                title="Are you sure?"
                onConfirm={this.subAdminDeleteProcess}
                onCancel={() => this.setState({ showAlert: false })}
                focusCancelBtn
                show={this.state.showAlert}
                >
                {this.state.showAlertMessage}
                </SweetAlert>


                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Staff List</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                                                                
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />

            <div className="tab-pane" id="Holiday-all">
                 
                <div className="card-footer text-right">
                    <NavLink to="/subadmin/add" className="btn btn-brand">Add Staff</NavLink>
                </div>


                <div className="card">
                    <div className="card-body">
                        <div className="table-responsive">
                            <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th >#</th>
                                        <th class='text-left' >Name</th>
                                        <th class='text-left' >Email</th>
                                        <th class='text-left'>Status</th>
                                        <th class='text-left' >Join Date</th>
                                        <th class='text-left' >Last Activity</th>
                                        <th class='text-center' >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.admins.map((admin, index) => (
                                            <tr>
                                                <td>{index + 1}</td>
                                                <td class='text-left table_content'>{admin.first_name} {admin.last_name}</td>
                                                <td class='text-left'>{admin.email}</td>
                                                <td class='text-left'>
                                                    
                                                    {admin.status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                </td>
                                       
                                                <td className='text-left'>
                                                    <span className='dateFormat'>{admin.created_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(admin.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td>

                                                <td className='text-left'>
                                                    <span className='dateFormat'>{admin.modified_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(admin.modified_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td>

                                                <td class='text-center'>
                                                    {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                    {/* <NavLink to={`/subadmin/detail/${admin.admin_id}`} className="text-info brandColor p-1" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>                                                     */}
                                                    
                                                    <a href='javascript:void(0);' onClick={this.subAdminDelete()} data-id={admin.admin_id} className="brandColor p-1" title="Edit"><i className="fa fa-trash text-danger"></i></a>
                                                </td>
                                            </tr>
                                        ))
                                    }

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </Fragment>
        )
    }
}

export default UserListing
