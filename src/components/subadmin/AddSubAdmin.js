import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import LoadingOverlay from 'react-loading-overlay';
import { NavLink } from 'react-router-dom'
// Variable
const $ = window.$

export class AddUser extends Component {

    state = {
        crm_id: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        linkeinProfile: '',
        address: '',
        city: '',
        state_code: '',
        country_code: 'USA',
        countries: [],
        states: [],
        businessWebsite: '',
        businessDescription: '',
        showAlert: false,
        showAlertMessage: '',
        member: {},
        member_permissions: [],
        valid_permission: [],
        selected_permission: {},
        zip_code: ''
    }

    componentDidMount() {
        $('.dropify').dropify();



        //get country List
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "country/list",
            method: 'get',
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ countries: response.data.countries });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


        this.setState({ showLoader: true });
        //get state List
        axios({
            url: API_URL + "state/list/USA",
            method: 'get',
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;

                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ states: response.data.states });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


        //get user detail from memeber id

    }

    table = (id) => {
        var table = $(`#${id}`).DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    },
                    render: function (datad) {
                        //return '<input type="checkbox" name="selectcompanyid">';
                        return '<label><input type="checkbox" name="test[]"></label>';
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'order': [[1, 'asc']]
        });
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }

    handleCountryChange = (e) => {
        const country_code = e.target.value
        if (!country_code) {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select country" });
            return false;
        }


        this.setState({
            [e.target.name]: country_code
        })

        //get state List
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "state/list/" + country_code,
            method: 'get',
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;

                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ states: response.data.states });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


    }


    validateUrl = (url) => {
        var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        const result = urlregex.test(url);
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    checkUrl = (e) => {
        const url = e.target.value;
        if (this.validateUrl(url)) {
            return true;
        }
        else {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
            return false;
        }
    }


    AddUser = (e) => {

        e.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,

                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const words = ['linkedin.com'];
            const regex = new RegExp(words.join('|'));
            if (!this.validateUrl(this.state.linkeinProfile)) {
                e.preventDefault()
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                return false;
            }
            else if (!regex.test(this.state.linkeinProfile)) {
                e.preventDefault()
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                return false;
            }
            else if (!this.validateUrl(this.state.businessWebsite)) {
                e.preventDefault()
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid Business Website URL." });
                return false;
            }

            axios({
                url: API_URL + "admin/subadmin/register/",
                method: 'post',
                data: {
                    first_name: this.state.firstName,
                    last_name: this.state.lastName,
                    linkedin_profile: this.state.linkeinProfile,
                    business_website: this.state.businessWebsite,
                    business_description: this.state.businessDescription,
                    Address: this.state.address,
                    city: this.state.city,
                    email: this.state.email,
                    state_code: this.state.state_code,
                    country_code: this.state.country_code,

                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success                        
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                        window.location.href = '/subadmin'
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions

    }

    checkValidZipCode = (evt) => {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            evt.preventDefault();
            return false;
        }
        else {
            if (iKeyCode == 46) {
                evt.preventDefault();
                return false;
            }
            else {
                if (evt.target.value.length > 7) {
                    evt.preventDefault();
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }

    render() {
        return (

            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    title="Alert"
                    text={this.state.showAlertMessage}
                    onConfirm={() => this.setState({ showAlert: false })}
                />

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Staff</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/subadmin">Staff</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Add Staff</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className='user-profile white-container'>


                    <div className="row">
                        <div className="col">

                            <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                <form className='form-horizontal' onSubmit={this.AddUser}>

                                    <div className="card-body">

                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">First Name </label>
                                            <div className="col-md-7">
                                                <input type="text" className="form-control" value={this.state.member.first_name} name='firstName' onChange={this.handleChange} required />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Last Name </label>
                                            <div className="col-md-7">
                                                <input type="text" className="form-control" value={this.state.member.last_name} name='lastName' onChange={this.handleChange} required />
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Email address </label>
                                            <div className="col-md-7">
                                                <input type="email" className="form-control" value={this.state.member.email} name='email' onChange={this.handleChange} required />
                                            </div>
                                        </div>
                                        {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Password </label>
                                                <div className="col-md-7">
                                                    <input type="password" className="form-control" name='password' onChange={this.handleChange} />
                                                </div>
                                            </div> */}
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">LinkedIn Profile </label>
                                            <div className="col-md-7">
                                                <input type="text" className="form-control" value={this.state.member.linkedin_profile} onBlur={this.checkUrl} name='linkeinProfile' onChange={this.handleChange} required />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Address </label>
                                            <div className="col-md-7">
                                                <textarea name="address" id="address" className="form-control" value={this.state.member.Address} name='address' onChange={this.handleChange}></textarea>
                                            </div>
                                        </div>

                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">City </label>
                                            <div className="col-md-7">
                                                <input type="text" className="form-control" value={this.state.member.city} name='city' onChange={this.handleChange} required />
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Country </label>
                                            <div className="col-md-7">
                                                <select className="form-control custom-select" name='country_code' onChange={this.handleCountryChange} required >
                                                    <option value="" hidden selected>Please Select Country</option>
                                                    {
                                                        this.state.countries.map((cntry) => (
                                                            cntry.country_code == "USA" ? (<option selected value={cntry.country_code}>{cntry.country_name}</option>) : (<option value={cntry.country_code}>{cntry.country_name}</option>)
                                                        ))
                                                    }
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">State / Province </label>
                                            <div className="col-md-7">
                                                <select className="form-control custom-select" name='state_code' onChange={this.handleChange} required >
                                                    <option value="" hidden selected>Please Select State</option>
                                                    {
                                                        this.state.states.map((state) => (
                                                            <option value={state.state_code}>{state.state_name}</option>
                                                        ))
                                                    }
                                                </select>
                                            </div>
                                        </div>
                                        {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">State / Province </label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='state' onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="state">State</option>
                                                    </select>
                                                </div>
                                            </div> */}
                                        {/* <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Country</label>
                                                <div className="col-md-7">
                                                    <select className="form-control custom-select" name='country' onChange={this.handleChange}>
                                                        <option value="" hidden selected>Please Choose</option>
                                                        <option value="Country">Country</option>
                                                    </select>
                                                </div>
                                            </div> */}

                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Zip Code</label>
                                            <div className="col-md-7">
                                                <input type="text" className="form-control" value={this.state.member.zip_code} name='zip_code' onKeyPress={this.checkValidZipCode} onChange={this.handleChange} required />
                                            </div>
                                        </div>
                                        <div className="form-group required row">
                                            <label className="col-md-3 col-form-label">Business Website</label>
                                            <div className="col-md-7">
                                                <input type="text" className="form-control" value={this.state.member.business_website} onBlur={this.checkUrl} name='businessWebsite' onChange={this.handleChange} required />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label">Business Description </label>
                                            <div className="col-md-7">
                                                <textarea name="address" id="businessDescription" className="form-control" value={this.state.member.business_description} name='businessDescription' onChange={this.handleChange}></textarea>
                                            </div>
                                        </div>



                                    </div>

                                    <div className="card-footer text-right">
                                        <button type="submit" className="btn btn-brand">Add Staff</button>
                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>


                </div>


            </Fragment>





        )
    }
}

export default AddUser
