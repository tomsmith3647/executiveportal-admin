import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

// import DropdownTreeSelect from 'react-dropdown-tree-select';
// import 'react-dropdown-tree-select/dist/styles.css';

import Container from './DropdownContainer';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class NotificationEdit extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        notification: {
            notification_description: '',
            notification_title: '',
            notification_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        role_list:[],
        selectedMember:[],
        selected_members : [],

        
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const notification_id = this.props.match.params.notification_id;
            this.setState({ notification_id: notification_id });
            this.setState({ notification: { notification_id: notification_id } });
            this.setState({ notification: { ...this.state.notification, send_type: 'banner' } });  
            this.getRoleMembers();
            this.getNotificationDetails();


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    getNotificationDetails = () => {

   
         
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        const notification_id = this.props.match.params.notification_id;

        axios({
            url: API_URL + "admin/notification/details/"+notification_id,
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ notification: response.data.notification, notification_members: response.data.notification.members}); 
                if(response.data.notification.members.length == 0)
                {
                    this.setState({ showProgaramTableMessage: 'Member not found'}); 
                }   
                var allreview = $('#tabnotification').DataTable({

                    'columnDefs': [{
                        //'targets': [6], // column index (start from 0)
                        'orderable': false, // set orderable false for selected columns
                    }]
                });

                allreview.columns().iterator('column', function (ctx, idx) {
                    $(allreview.column(idx).header()).append('<span class="sort-icon"/>');
                });                                               
            }
                       
        })
        .catch(err => {
            console.log(err);
        });        
    }

    getRoleMembers = () => {
         
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "admin/rolemember/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ role_list: response.data.role_list });                                                     
            }
            else{
                //success                        
                this.setState({ role_list: [] });                                                       
            }            
        })
        .catch(err => {
            console.log(err);
        });        
    }


    validateUrl = (url) => {
        var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        const result = urlregex.test(url);
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    checkUrl = (e) => {
        const url = e.target.value;
        if (this.validateUrl(url)) {
            return true;
        }
        else {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
            return false;
        }
    }

    handleChangeMembers = (currentNode, selectedNodes) => {
        this.setState
        ({
            selectedMember: selectedNodes
        });                          
    }

    handleChangeMembers55 = (currentNode, selectedNodes) => {
    // currentNode: { label, value, children, expanded, checked, className, ...extraProps }
    // selectedNodes: [{ label, value, children, expanded, checked, className, ...extraProps }]
    console.log('onChange selectedNodes ', selectedNodes);

     


    // const selectedData = [];
    // for(var u=0;u < selectedNodes.length; u++)
    // {
    //     const selectData = selectedNodes[u];
    //     const rmname = selectData.label;
    //     const rmid = selectData.value;
    //     const rmchildren = selectData._children ?  selectData.children : Array();
    //     var selected_member = {};
    //     if(rmchildren.length > 0)
    //     {
    //          selected_member = {name:rmname, id:rmid, type:'role'};
              
    //     }
    //     else
    //     {
    //          selected_member = {name:rmname, id:rmid, type:'member'};
    //     }
    //     selectedData.push(selected_member);
    // }

    // this.setState
    // ({
    //     selectedMember: selectedData
    // });


     
 
    console.log('onChange selectedNodes Final ', this.state.selectedMember);
    //const vm = JSON.stringify(selectedNodes);
    // this.setState({
    //     selectedMember:[...selectedNodes]
    //     },() => {
    //         this.handleChangeMembers();
    //     });
    
    //this.setState({ notification: { ...this.state.notification, notification_title: e.target.value } })

    //console.log('onChange selectedNodes final ', this.state.selectedMember);

    }


    handleChangeMembers4 = (event) => {
    // currentNode: { label, value, children, expanded, checked, className, ...extraProps }
    // selectedNodes: [{ label, value, children, expanded, checked, className, ...extraProps }]
    
    const isChecked = event.checked ? true : false;

    if(isChecked)
    {
        this.state.selected_members.push(event);

        // // Create a new array based on current state:
        // let selected_members = [...this.state.selected_members];

        // // Add item to it
        // selected_members.push(event);

        // // Set state
        // this.setState({ selected_members });

       // this.setState({ selected_members: this.state.selected_members.concat(event) })


        //this.setState({ notification: { ...this.state.notification, notification_title: e.target.value } });
    }
    else
    {
        
        const rmdata  = this.removeSelectedMember(this.state.selected_members,'value',event.value);
        this.setState(
            {
                selected_members:[]
            }
            );
        this.state.selected_members.push(rmdata);
    }

    console.log('onChange selectedNodes ', this.state.selected_members);
      
    }

    
    removeSelectedMember = (myObjects,prop,valu) =>
    {
         return myObjects.filter(function (val) {
          return val[prop] !== valu;
      });

    }

    
 
    handleChange = (e) => {
        this.setState({ notification: { ...this.state.notification, notification_title: e.target.value } });
    }
 

    handleSummerNote = (e) => {
        this.setState({
            notification: {
                ...this.state.notification,
                notification_description: e
            }
        })
    }
 

    //add task
    NotificationEditBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            const selected_member = this.state.selectedMember;

            if(selected_member.length == 0) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select role/member." });
                return false;
            }


            if (!this.state.notification.notification_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter notification title." });
                return false;

            }

            if ((this.state.notification.notification_title).trim().length ==0) {
                this.setState({ notification: { ...this.state.notification, notification_title: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid notification title." });
                return false;
            }
            
             

            const selectedData = [];
            for(var u=0;u < selected_member.length; u++)
            {
                const selectData = selected_member[u];
                const rmname = selectData.label;
                const rmid = selectData.value;
                const rmchildren = selectData._children ?  selectData._children : Array();
                var selmember = {};
                if(rmchildren.length > 0)
                {
                    selmember = {name:rmname, id:rmid, type:'role'};
                    
                }
                else
                {
                    selmember = {name:rmname, id:rmid, type:'member'};
                }
                selectedData.push(selmember);
            }

 

            var postData = {
                notification_title: this.state.notification.notification_title,
                notification_description: this.state.notification.notification_description,
                send_type: this.state.notification.send_type,
                selected_member: selectedData
            }

    
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/notification/create",
                method: 'post',
                data: postData,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success 
                         
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/notifications'});
                         
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleActionNotificationType = (e) => {  
        
        const checkvalue = '';
        


        // const isChecked = e.target.checked;
        
        // if(isChecked)
        // {
        //     checkvalue = e.currentTarget.value;
        // }
         this.setState({ notification: { ...this.state.notification, send_type: e.target.value } });        
    }
    
       


    render() {


        var dropdownroledata = [];
        var roleData = this.state.role_list;
        for(var i =0; i < roleData.length; i++)
        {
            const rolemember = roleData[i];
            const role_id = rolemember.role_id;
            const role_title = rolemember.role_title;
            const members = rolemember.members;
            var dropdownmemberdata = [];
            for(var k =0; k < members.length; k++)
            {
                const member = members[k];
                const member_id = member.member_id;
                const member_first_name = member.first_name;
                const member_last_name = member.last_name;
                const member_full_name = member_first_name+' '+member_last_name;
                const memberdata = {label: member_full_name, value: member_id}
                dropdownmemberdata.push(memberdata);
            }

            const role_data = {label: role_title, value: role_id, children: dropdownmemberdata}

            dropdownroledata.push(role_data);
            
        }

         

           
        //   const onAction = (node, action) => {
        //     console.log('onAction::', action, node)
        //   }
        //   const onNodeToggle = currentNode => {
        //     console.log('onNodeToggle::', currentNode)
        //   }

        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Notification</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li> 
                                    <li className="breadcrumb-item"><NavLink to="/notifications">notifications</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Add notification</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-6 col-md-12">
                        <form className='form-horizontal' onSubmit={this.NotificationEditBtn}>

                        <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Roles/Members</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                        <Container data={dropdownroledata} onChange={this.handleChangeMembers}    />
                                        </div>
                                    </div>                                
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Title</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.notification.notification_title} name='notification_title' onChange={this.handleChange}  />
                                        </div>
                                    </div>                                
                                </div>
                            </div>

                            

                            

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value={this.state.notification.notification_description}

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['notification', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Notification Type</h3>
                                </div>
                                <div className="card-body notification">
                                    <label class="radio-inline"><input type="radio" name="send_type" value="banner" onChange={this.handleActionNotificationType} checked={this.state.notification.send_type == 'banner' ? true : ''} defaultChecked={this.state.notification.send_type == 'banner' ? true : ''} />Banner</label>    
                                    <label class="radio-inline"><input type="radio" name="send_type" value="email" onChange={this.handleActionNotificationType} checked={this.state.notification.send_type == 'email' ? true : ''} defaultChecked={this.state.notification.send_type == 'email' ? true : ''} />Email</label>
                                    <label class="radio-inline"><input type="radio" name="send_type" value="both" onChange={this.handleActionNotificationType} checked={this.state.notification.send_type == 'both' ? true : ''} defaultChecked={this.state.notification.send_type == 'both' ? true : ''} />Both</label>
                                </div>
                            </div>

                            
                             
                             
                             
                            <div className="card-footer text-right">                                
                                <button type="submit" className="btn btn-brand">Add</button>
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default NotificationEdit
