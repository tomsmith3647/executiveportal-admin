import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class NotificationList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        notifications: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_notification_id: '',
        access_permission:false,
        access_permission_r:false,
        access_permission_w:false,
        access_permission_u:false,
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
             
            this.getNotificationList();


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }


    getNotificationList = () => {
         
        
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "admin/notification/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                this.setState({ access_permission_r: response.data.access_permission_r });
                this.setState({ access_permission_w: response.data.access_permission_w });
                this.setState({ access_permission_u: response.data.access_permission_u });
                if (error_code == '309') {
                    this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
                else if (error_code == '200') {
                    //success                        
                    this.setState({ notifications: response.data.notifications });

                    
                        var notificationTable = $('#usersTable').DataTable({
                           
                            'columnDefs': [{
                                'targets': [4], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            //"pageLength": 500,
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                            
                        });

                        notificationTable.columns().iterator('column', function (ctx, idx) {
                            $(notificationTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                         
                   
                }
                else {

                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });       
    }



    getNotificationListAction = () => {
         
        
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "admin/notification/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ notifications: response.data.notifications });                                         
                }
                else {

                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });       
    }


   

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    //notificationDelete    
    notificationDelete = () => (e) => {
        
        const notification_id = e.currentTarget.dataset.id ;
        this.setState({ selected_notification_id: notification_id });
        this.setState({ showConfirmationAlert: true });
    }

    //notificationDeleteProcess
    notificationDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const notification_id = this.state.selected_notification_id;

            axios({
                url: API_URL + "admin/notification/delete",
                method: 'post',
                data: {
                    notification_id: notification_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            notification/category/module/details       
                        
                        // this.getNotificationListAction();

                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/notifications' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }

    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }


    handleCheckWindow =(e)=> {
        const url = e.currentTarget.dataset.id ;
        
        window.open(url, "myWindow", 'width=800,height=600');
        e.preventDefault();
     }


    //handleMemberProgramMapping
    handlenotificationTopFive = (e) => {
         
        const isChecked = e.target.checked ? 1 : 0;
        
         
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });             
            axios({
                url: API_URL + "admin/notification/topfive",
                method: 'post',
                data: {
                    notification_id: e.target.value,   
                    isChecked: isChecked,    
                                        
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            
                        
                        this.setState({ notifications: response.data.notifications });     

                        
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: false, showAlertMessage: error_message });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
    }

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.notificationDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Notifications</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">Notifications</li>
                        </ol>
                    </div>
                </div>
                <br />
                {this.state.access_permission_r && (
                <div className="tab-pane" id="Holiday-all">

{this.state.access_permission_w && (<div className="card-footer text-right">
                        <NavLink to="/notification/add" className="btn btn-brand">Add Notification</NavLink>
                    </div>)}

                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th >#</th>
                                            
                                            <th className='text-left' >Title</th>
                                          
                                            <th className='text-left'>Status</th>     
                                                                                                                                
                                            <th className='text-left' >Created Date</th>
                                            
                                            {this.state.access_permission_u && (<th className='text-center' >Action</th>)}
                                             

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.notifications.map((notification, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>
                                                    
                                                    <td className="text-left text-capitalize cm-td">
                                                        
 
                                                            <NavLink data-id={notification.notification_id} to={`/notification/details/${notification.notification_id}`} className="text-info brandColor" title="View Detail">
                                                            {notification.notification_title}
                                                            </NavLink>
                                                        
                                                    </td>
                                                     
                                                    
                                                    <td className='text-left'>
                                                        {notification.notification_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                    </td>

                                                     
                                                    
                                                    

                                                    <td className='text-left'><span className='dateFormat'>{notification.created_date}</span>{(new Date(this.convertUTCDateToLocalDate(new Date(notification.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}</td>
                                                    
                                                    
                                                    {this.state.access_permission_u && (<td className='text-center'>
                                                    {/* <NavLink to={`/notification/edit/${notification.notification_id}`} className="text-info brandColor p-1" title="Edit">Duplicate</NavLink> */}
                                                    
                                                    <a href='javascript:void(0);' onClick={this.notificationDelete()} data-id={notification.notification_id} className="brandColor p-1" title="Edit"><i className="fa fa-trash text-danger"></i></a>
                                                    </td>)}
                                                     
                                                </tr>
                                            ))
                                        }

                                    </tbody>
                                    {this.state.notifications.length == 0 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'No notification'}</td></tr></tfoot>)}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>)}
            </Fragment>
        )
    }
}

export default NotificationList
