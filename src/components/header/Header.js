import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
// Variable
const $ = window.$

export class Header extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        showAlert: false,
        showAlertMessage: '',
        admin: {}
    }

    componentDidMount() {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
            })

            axios({
                url: API_URL + "admin/detail/" + userSession.user_id,
                method: 'get',
                data: {},
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    if (error_code == '200') {
                        this.setState({ admin: response.data.admin });
                    }
                    else if (error_code == '301') {
                        //auth expired
                        this.handleLogout();

                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        else {
            this.handleLogout();
        }
    }


    handleLogout = () => {

        //this.setState({showAlert:true,showAlertMessage:'Logout Successfully'});

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            const selected_permissions = this.state.valid_permission;
            axios({
                url: API_URL + "admin/logout",
                method: 'post',
                data: {},
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    window.localStorage.removeItem('userSession');
                    window.location.href = '/auth/signin'
                    if (error_code == '200') {
                        //success                        

                    }
                    else {
                        //fail
                        //this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
    }

    


    render() {
        return (
            // Start Page header
            <Fragment>

                <SweetAlert
                    show={this.state.showAlert}
                    title="Alert"
                    text={this.state.showAlertMessage}
                    onConfirm={() => this.setState({ showAlert: false })}
                />

                <div className="page-header">

                    <div className="left">
                        <div className="input-group">
                            
                            <input type="text" className="form-control" placeholder="What you want to find" />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary btn-search" type="button">Search</button>
                            </div>
                            <a href="javascript:void(0)" className="d-sm-block d-md-none nav-link icon menu_toggle" onClick={this.handleToggle}><i className="fe fe-align-center"></i></a>
                        </div>
                    </div>
                    <div className="right">
                        <div className="notification d-flex">
                            <div className="dropdown d-flex">
                                <a href="javascript:void(0)" className="chip ml-3" data-toggle="dropdown">
                                    <span className="avatar" style={{ backgroundImage: 'url(/assets/images/placeholder-2.png)' }}></span>{this.state.full_name}</a>
                                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">

                                    <a className="dropdown-item" onClick={this.handleLogout}><i className="dropdown-icon fe fe-log-out"></i> Sign out</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </Fragment>

        )
    }
}

export default Header
