import React, { Component } from 'react'

export class Breadcrumb extends Component {
    render() {
        return (
            <div className="d-flex justify-content-between align-items-center">
                <div className="header-action">
                    <h1 className="page-title"></h1>
                    <ol className="breadcrumb page-breadcrumb">
                        {/*  <li className="breadcrumb-item"><a href="#">Admin</a></li>
                         <li className="breadcrumb-item"><a href="#">University</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Dashboard</li> */}
                    </ol>
                </div>
            </div>
        )
    }
}

export default Breadcrumb
