import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class LinkList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        links: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_link_id: '',
        access_permission:false,
        access_permission_r:false,
        access_permission_w:false,
        access_permission_u:false,
         
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/link/list",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    this.setState({ access_permission_r: response.data.access_permission_r });
                    this.setState({ access_permission_w: response.data.access_permission_w });
                    this.setState({ access_permission_u: response.data.access_permission_u });
                    if (error_code == '309') {
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                    else if (error_code == '200') {
                        //success                        
                        this.setState({ links: response.data.links });


                        var linkTable = $('#usersTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [5], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            //"pageLength": 500,
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                            
                        });

                        linkTable.columns().iterator('column', function (ctx, idx) {
                            $(linkTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        linkTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = linkTable.row(diff[i].node).data();
                                const link_id = $(rowData[1]).attr("data-id");
                                const catsort = { "link_id": link_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/link/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                                });
                        });
                    }
                    else {

                        this.setState({ showLoader: false, showProgaramTableMessage: error_message });
                        //fail
                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }


   

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    //linkDelete    
    linkDelete = () => (e) => {
        
        const link_id = e.currentTarget.dataset.id ;
        this.setState({ selected_link_id: link_id });
        this.setState({ showConfirmationAlert: true });
    }

    //linkDeleteProcess
    linkDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const link_id = this.state.selected_link_id;

            axios({
                url: API_URL + "admin/link/delete",
                method: 'post',
                data: {
                    link_id: link_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            link/category/module/details       
                        
                        axios({
                            url: API_URL + "admin/link/list",
                            method: 'get',
                            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                        })
                        .then(response => {
                            const error_code = response.data.error_code;
                            const error_message = response.data.error_message;
                            const ApiURL = response.data.api_url;
                            this.setState({ baseApiURL: ApiURL });
                            this.setState({ showLoader: false });
                            if (error_code == '200') {
                                //success                        
                                this.setState({ links: response.data.links });                                                       
                            }
                            else{
                                //success                        
                                this.setState({ links: [] });                                                       
                            }
                            
                        })
                        .catch(err => {
                            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                        });

                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/links' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
        //Get user permissions
    }

    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }


    handleCheckWindow =(e)=> {
        const url = e.currentTarget.dataset.id ;
        
        window.open(url, "myWindow", 'width=800,height=600');
        e.preventDefault();
     }


    //handleMemberProgramMapping
    handleLinkTopFive = (e) => {
         
        const isChecked = e.target.checked ? 1 : 0;
        
         
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });             
            axios({
                url: API_URL + "admin/link/topfive",
                method: 'post',
                data: {
                    link_id: e.target.value,   
                    isChecked: isChecked,    
                                        
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            
                        
                        this.setState({ links: response.data.links });     

                        
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: false, showAlertMessage: error_message });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
    }

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.linkDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Links</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">Links</li>
                        </ol>
                    </div>
                </div>
                <br />
                {this.state.access_permission_r && (
                <div className="tab-pane" id="Holiday-all">

{this.state.access_permission_w && (<div className="card-footer text-right">
                        <NavLink to="/link/add" className="btn btn-brand">Add Link</NavLink>
                    </div>)}

                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th >#</th>
                                            
                                            <th className='text-left' >Title</th>
                                            <th className='text-left' >URL</th>
                                            <th className='text-left'>Status</th>     
                                            <th className='text-left'>Open Type</th>                                                                                        
                                            <th className='text-left' >Created Date</th>
                                            <th class='text-center'>Top</th>
                                            {this.state.access_permission_u && (<th className='text-center' >Action</th>)}
                                             

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.links.map((link, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>
                                                    
                                                    <td className="text-left text-capitalize">
                                                        
 
                                                            <NavLink data-id={link.link_id} to={`/link/details/${link.link_id}`} className="text-info brandColor" title="View Detail">
                                                            {link.link_title}
                                                            </NavLink>
                                                        
                                                    </td>
                                                     
                                                    <td className="text-left table_content">                                                        
                                                        {/* <a href={`${link.link_url}`} target="_blank" className="btn btn-icon btn-sm text-info brandColor" title="View Detail">
                                                        {link.link_url}
                                                        </a> */}

                                                        {link.open_type == 0 ? (                                                       
                                                            <a  href={`${link.link_url}`} target="_blank" className="brandColor m-0 p-0" title="Details">{link.link_url}</a>
                                                        ) : (
                                                            <a  href={`${"/link/open/"+link.link_id}`} className="brandColor m-0 p-0" title="Details">{link.link_url}</a>
                                                        )
                                                        }

                                                        {/* <a href='javascript:void(0);' onClick={this.handleCheckWindow.bind(this)} data-id={link.link_url} className="brandColor" title="Details">{link.link_url}</a> */}
                                                    </td>
                                                    <td className='text-left'>
                                                        {link.link_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                    </td>

                                                    
                                                    <td className='text-left'>
                                                        {link.open_type == 0 ? (<span className="tag">New Tab</span>) : (<span className="tag">iFrame</span>)}
                                                    </td>

                                                    
                                                    

                                                    <td className='text-left'><span className='dateFormat'>{link.created_date}</span>{(new Date(this.convertUTCDateToLocalDate(new Date(link.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}</td>
                                                    
                                                    <td class='text-center'>
                                                    {link.link_status == 1 ? ( <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                <input type="checkbox" class="custom-control-input" name="link_id" value={link.link_id} onChange={this.handleLinkTopFive} checked={link.link_topfive == '1' ? true : ''} defaultChecked={link.link_topfive == '1' ? true : ''}  />               <span class="custom-control-label">&nbsp;</span>
                                                            </label>) : ''}
                                                           
                                                        </td>
                                                        {this.state.access_permission_u && (<td className='text-center'>
                                                    <NavLink to={`/link/edit/${link.link_id}`} className="text-info brandColor p-1" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>
                                                    
                                                    <a href='javascript:void(0);' onClick={this.linkDelete()} data-id={link.link_id} className="brandColor p-1" title="Edit"><i className="fa fa-trash text-danger"></i></a>
                                                    </td>)}
                                                     
                                                </tr>
                                            ))
                                        }

                                    </tbody>
                                    {this.state.links.length == 0 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'No link'}</td></tr></tfoot>)}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>)}
            </Fragment>
        )
    }
}

export default LinkList
