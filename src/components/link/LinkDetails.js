import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class LinkDetails extends Component {

    state = {
        name: '',
        notes: '',
        link_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        link: {
            link_description: '',
            link_title: '',
            link_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        link_permissions:[],
        link_link_permissions:[],
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            const link_id = this.props.match.params.link_id;
            this.setState({ link_id: link_id });
            this.setState({ link: { link_id: link_id } });

            if(link_id)
            {
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "admin/link/details/" + link_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ link: response.data.link, showAlertActionURL: '/link/category/link/details/' + this.state.link_id});
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });

                
            }
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }
 
    

    validateUrl = (url) => {
        var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        const result = urlregex.test(url);
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    checkUrl = (e) => {
        const url = e.target.value;
        if (this.validateUrl(url)) {
            return true;
        }
        else {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
            return false;
        }
    }
 
    handleChange = (e) => {
        this.setState({ link: { ...this.state.link, link_title: e.target.value } });
    }

    handleChangeURL = (e) => {
        this.setState({ link: { ...this.state.link, link_url: e.target.value } });
    }

     handleChangeLinkStatus = (e) => {
        this.setState({ link: { ...this.state.link, link_status: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            link: {
                ...this.state.link,
                link_description: e
            }
        })
    }
 

    //add task
    linkUpdateBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.link.link_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter link title." });
                return false;

            }

            if ((this.state.link.link_title).trim().length ==0) {
                this.setState({ link: { ...this.state.link, link_title: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid link title." });
                return false;
            }

  
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/link/update",
                method: 'post',
                data: this.state.link,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {

                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success 
                        
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message,  showAlertActionURL: '/links'});
                    
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    

    openWindow = (event, url='') => {
        url = this.state.link.link_url;
        window.open(url, "myWindow", 'width=800,height=600');
        event.preventDefault();
    }

 
  



    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Link Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>  
                                    <li className="breadcrumb-item"><NavLink to="/links">Links</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Link Details</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-12 col-md-12">
                   
                        <div className="card">
                        
                        <div className="card-body">
                        <div className="text-right">
                        <NavLink  className="btn btn-brand" to={`/link/edit/${this.state.link.link_id}`}>Edit</NavLink>                                
                        
                        </div>
                            <div className="form-group row">                                    
                                <div className="col-md-3 text-right">
                                    Name :
                                    </div>
                                    <div className="col-md-9 text-left">
                                    {this.state.link.link_title}
                                </div>                                                                   
                            </div> <br/>

                            <div className="form-group row">                                    
                                <div className="col-md-3 text-right">
                                    URL :
                                    </div>
                                    <div className="col-md-9 text-left">
                                    
                                    {/* <a href="javascript:void(0)" onClick={this.openWindow} className="text-info brandColor m-0 p-0" title="View Detail">
                                                        {this.state.link.link_url}
                                                        </a> */}


                                    {this.state.link.open_type == 0 ? (<a  href={`${this.state.link.link_url}`} target="_blank" className="brandColor m-0 p-0" title="Details">{this.state.link.link_url}</a>) : (<a  href={`${"/link/open/"+this.state.link.link_id}`} className="brandColor m-0 p-0" title="Details">{this.state.link.link_url}</a>)}

                                    

                                    
                                </div>                                                                   
                            </div> <br/>

                            <div className="form-group row">                                    
                                <div className="col-md-3 text-right">
                                    Description :
                                    </div>
                                    <div className="col-md-9 text-left">
                                    
                                    <div dangerouslySetInnerHTML={{ __html: this.state.link.link_description }} />
                                </div>                                                                   
                            </div> <br/>

                            <div className="form-group row">                                    
                                <div className="col-md-3 text-right">
                                    Open Type :
                                    </div>
                                    <div className="col-md-9 text-left">
                                    
                                    {this.state.link.open_type == 0 ? ('New Tab') : ('iFrame')}
                                </div>                                                                   
                            </div>


                            <div className="form-group row">                                    
                                <div className="col-md-3 text-right">
                                    Status :
                                    </div>
                                    <div className="col-md-9 text-left">
                                    
                                    {this.state.link.link_status == 1 ? ('Active') : ('Inactive')}
                                </div>                                                                   
                            </div>


                                 </div>
                            </div>
                            
                             
                             
                        
                    </div>


                     

                </div>
            </Fragment>

        )
    }
}

export default LinkDetails
