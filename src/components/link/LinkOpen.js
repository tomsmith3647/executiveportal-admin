import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';

import Iframe from 'react-iframe';

import { MDBContainer, MDBIframe } from "mdbreact";


const $ = window.$
class LinkOpen extends Component {

    state = {
        name: '',
        notes: '',
        link_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        link: {
            link_description: '',
            link_title: '',
            link_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        link_permissions:[],
        link_link_permissions:[],
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            const link_id = this.props.match.params.link_id;
            this.setState({ link_id: link_id });
            this.setState({ link: { link_id: link_id } });

            if(link_id)
            {
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "admin/link/details/" + link_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ link: response.data.link, showAlertActionURL: '/link/category/link/details/' + this.state.link_id});
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });

                
            }
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }
 
    

    validateUrl = (url) => {
        var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        const result = urlregex.test(url);
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    checkUrl = (e) => {
        const url = e.target.value;
        if (this.validateUrl(url)) {
            return true;
        }
        else {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
            return false;
        }
    }
 
    handleChange = (e) => {
        this.setState({ link: { ...this.state.link, link_title: e.target.value } });
    }

    handleChangeURL = (e) => {
        this.setState({ link: { ...this.state.link, link_url: e.target.value } });
    }

     handleChangeLinkStatus = (e) => {
        this.setState({ link: { ...this.state.link, link_status: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            link: {
                ...this.state.link,
                link_description: e
            }
        })
    }
 

    //add task
    linkUpdateBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.link.link_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter link title." });
                return false;

            }

            if ((this.state.link.link_title).trim().length ==0) {
                this.setState({ link: { ...this.state.link, link_title: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid link title." });
                return false;
            }

  
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/link/update",
                method: 'post',
                data: this.state.link,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {

                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success 
                        
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message,  showAlertActionURL: '/links'});
                    
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

  



    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Link Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>  
                                    <li className="breadcrumb-item"><NavLink to="/links">Links</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Details</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-12 col-md-12">       
                    {this.state.link.link_url}                      
                        <div className="card">                        
                            <div className="card-body">
                            <MDBContainer className="text-center">
      <MDBIframe src="https://www.youtube.com/embed/v64KOxKVLVg" />
    </MDBContainer>
                             </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        

 
        )
    }
}

export default LinkOpen
