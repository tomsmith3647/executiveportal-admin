import React, { Component, Fragment } from 'react'
import {Pie} from 'react-chartjs-2';
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import { NavLink } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';
import Select from 'react-select';
import Moment from 'react-moment';
import 'moment-timezone';
const $ = window.$

export class DashBoardMember extends Component {

    constructor(props) {
        super(props);
        this.state = {
            baseApiURL: '',
            user_id: '',
            auth_token: '',
            email: '',
            full_name: '',
            showLoader: false,
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showLoader: false,
            categories: [],
            reviews: [],
            userSession:{},
            apiHeader:{},
            booms:[],
            refers:[],
            members:[],
            member:{},
            member_id:'',            
            showConfirmationAlertBoom:false,
            showConfirmationAlertReferral:false,
            counters:[],
            counter_data:{},
            options:[],
            memberData:{},
            defaultVlueOption:{},
            showAlertActionURL:'',

            access_permission_boom_r:false,
            access_permission_boom_w:false,
            access_permission_boom_u:false,

            access_permission_review_r:false,
            access_permission_review_w:false,
            access_permission_review_u:false,

            access_permission_referral_r:false,
            access_permission_referral_w:false,
            access_permission_referral_u:false,
            permissions: [],
        }
 
    }

    componentDidMount() {  
        
        this.state.options.push({value: '', label: 'All Members' }) 
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({user_id:userSession.user_id});               
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
         
        const member_id = this.props.match.params.member_id;
        this.setState({member_id: member_id});
        this.getDashBoardMember(1);
        this.getBoomSales(1);
        this.getReferralList(1);
        this.getMemberDetails();
        this.getMemberList();
        this.getProgramCounter();
        this.getStaffPermission();
    }

    getStaffPermission = () => {                 
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
         
        axios({
            url: API_URL + "admin/staff/permissions",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
           
           
            if (error_code == '200') {
                //success                        
                this.setState({permissions: response.data.permissions});   
                                      
            }
            else {
                this.setState({ showLoader: false, showProgaramTableMessage: error_message });                    
            }
        })
        .catch(err => {
            console.log(err);
        });                                        
    }


    
    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }

    getProgramCounter =(str)=>{
        //get review List
        str = str ? str : 0;
        const member_id = this.props.match.params.member_id;
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showUsersTableMessage: 'Please wait...' });

       

        axios({
            url: API_URL + "admin/dashboard/program/counts/"+member_id,
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message; //
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });

            if (error_code == '200') {
                //success                        
                this.setState({ counters: response.data.programs, counter_data: response.data });
                
            }
            else {
                //fail
                this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
            }
        })
        .catch(err => {
            console.log(err);
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });

    }

    getMemberList =(str)=>{
        //get review List
        str = str ? str : 0;
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showUsersTableMessage: 'Please wait...' });
        axios({
            url: API_URL + "admin/dashboard/member/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message; //
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });

            if (error_code == '200') {
                //success                        
                this.setState({ members: response.data.members });

                this.state.members.map((member, index) => (            
                    this.state.options.push({value: member.member_id, label: member.first_name + ' ' + member.last_name + ' (' + member.email + ')' })      
                    
                ))

                if(str)
                {
                    var userTable = $('#pusersTable').DataTable({
                        'columnDefs': [{
                            'targets': [4], // column index (start from 0)
                            'orderable': false, // set orderable false for selected columns
                        }]
                    });
                    userTable.columns().iterator('column', function (ctx, idx) {
                        $(userTable.column(idx).header()).append('<span class="sort-icon"/>');
                    });
                }
            }
            else {
                //fail
                this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
            }
        })
        .catch(err => {
            console.log(err);
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });

    }

    getMemberDetails =(str)=>{
        //get review List
        str = str ? str : 0;
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showUsersTableMessage: 'Please wait...' });
        const member_id = this.props.match.params.member_id;
        this.setState({member_id:member_id, showLoader: true, showUsersTableMessage: 'Please wait...' });
        axios({
            url: API_URL + "admin/member/detail/"+member_id,
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message; //
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });

            if (error_code == '200') {
                //success                                     
                this.setState({ member: response.data.member, memberData: response.data.member});   
                const defaultVlueOption ={value: this.state.memberData.member_id, label: this.state.memberData.first_name + ' ' + this.state.memberData.last_name + ' (' + this.state.memberData.email + ')' };            
                this.setState({defaultVlueOption:defaultVlueOption});
            }
            else {
                //fail
                this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
            }
        })
        .catch(err => {
            console.log(err);
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });

    }

    getDashBoardMember =(str)=>{
        //get review List
        str = str ? str : 0;
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        const member_id = this.props.match.params.member_id;

        this.setState({ showLoader: true });
        axios({
            url: API_URL + "admin/review/list/"+member_id,
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                this.setState({ access_permission_review_r: response.data.access_permission_r });
                this.setState({ access_permission_review_w: response.data.access_permission_w });
                this.setState({ access_permission_review_u: response.data.access_permission_u });
                if (error_code == '200') {
                    //success                        
                    this.setState({ reviews: response.data.reviews });
                    if(str)
                    {
                        var allreview = $('#allreview').DataTable({

                            'columnDefs': [{
                                'targets': [6], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }]
                        });

                        allreview.columns().iterator('column', function (ctx, idx) {
                            $(allreview.column(idx).header()).append('<span class="sort-icon"/>');
                        });
                    }
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });
    }

    getBoomSales =(str)=>{
        //get review List
        str = str ? str : 0;
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));        
        //get boom List
        this.setState({ showLoader: true });
        const member_id = this.props.match.params.member_id;
        
        axios({
            url: API_URL + "admin/boom/sales/"+member_id,
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            this.setState({ showLoader: false });
            this.setState({ access_permission_boom_r: response.data.access_permission_r });
            this.setState({ access_permission_boom_w: response.data.access_permission_w });
            this.setState({ access_permission_boom_u: response.data.access_permission_u });
            if (error_code == '200') {
                //success                        
                this.setState({ booms: response.data.booms });
                if(str)
                    {
                        var TblBoomSales = $('#TblBoomSales').DataTable({

                            'columnDefs': [{
                                'targets': [5], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }]
                        });

                        TblBoomSales.columns().iterator('column', function (ctx, idx) {
                            $(TblBoomSales.column(idx).header()).append('<span class="sort-icon"/>');
                        });
                    }
            }
            else {
                //fail
                //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
            }
        })
        .catch(err => {
            console.log(err);
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });
         
    }

    getReferralList =(str)=>{
        //get review List
        str = str ? str : 0;
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));    
        const member_id = this.props.match.params.member_id;
        //get referral List
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "admin/referral/list/"+member_id,
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            this.setState({ showLoader: false });
            this.setState({ access_permission_referral_r: response.data.access_permission_r });
            this.setState({ access_permission_referral_w: response.data.access_permission_w });
            this.setState({ access_permission_referral_u: response.data.access_permission_u });
            if (error_code == '200') {
                //success                        
                this.setState({ refers: response.data.refers });

                if(str)
                {
                    var TblReferralList = $('#TblReferralList').DataTable({

                        'columnDefs': [{
                            'targets': [8], // column index (start from 0)
                            'orderable': false, // set orderable false for selected columns
                        }]
                    });

                    TblReferralList.columns().iterator('column', function (ctx, idx) {
                        $(TblReferralList.column(idx).header()).append('<span class="sort-icon"/>');
                    });
                }
            }
            else {
                //fail
                //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
            }
        })
        .catch(err => {
            console.log(err);
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });

    }

    //handleReviewPublish
    handleReviewPublish = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        const status = isChecked;
            
         
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });             
            axios({
                url: API_URL + "admin/review/publish",
                method: 'post',
                data: {                     
                    status: isChecked,    
                    review_id: e.target.value,                     
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            
                        
                         
                        this.getDashBoardMember();
                        
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
    }

    //roleDelete    
    boomDelete = () => (e) => {
        //const boom_id = e.currentTarget.value;

        const boom_id = e.currentTarget.dataset.id ;


        this.setState({ selected_boom_id: boom_id });
        this.setState({ showConfirmationAlertBoom: true });
    }

    //roleDeleteProcess
    boomDeleteProcess = () => {
        this.setState({ showConfirmationAlertBoom: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlertBoom: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const boom_id = this.state.selected_boom_id;

            axios({
                url: API_URL + "admin/boom/delete",
                method: 'post',
                data: {
                    boom_id: boom_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            role/category/module/details         
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message});
                        this.getBoomSales();
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
        //Get user permissions
    }


    ////////////////////////////////////////////////



    //roleDelete    
    referralDelete = () => (e) => {
        
        const referral_code = e.currentTarget.dataset.id ;
        this.setState({ selected_referral_code: referral_code });
        this.setState({ showConfirmationAlertReferral: true });
    }

    //roleDeleteProcess
    referralDeleteProcess = () => {
        this.setState({ showConfirmationAlertReferral: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlertReferral: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const referral_code = this.state.selected_referral_code;

            axios({
                url: API_URL + "admin/referral/delete",
                method: 'post',
                data: {
                    referral_code: referral_code,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                this.setState({ showAlert: false });
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                if (error_code == '200') {
                    //success            role/category/module/details         
                    this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    this.getReferralList();
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });
        }
        //Get user permissions
    }



    sweetalertok = () => {
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL == 'refresh') 
        {
            window.location.reload();  
        } 
        else if(this.state.showAlertActionURL) 
        {
            window.location.href = this.state.showAlertActionURL;  
        }
    }

    handleChangeMember = (e) => {

        const member_id = e.target.value ;
        if (member_id) {
            window.location.href = '/dashboard/'+member_id; // '/dashboard';
        }
        else
        {
            window.location.href = '/dashboard'; // '/dashboard';
        }
    }

    
    handleChangeSearch = selectedOption => {
        
        const member_id = selectedOption.value;
        if (member_id) {
            window.location.href = '/dashboard/' + member_id; // '/dashboard';
        }
        else
        {
            window.location.href = '/dashboard'; // '/dashboard';
        }

    };


    render() {

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        //Current Date
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; //January is 0!
        let yyyy = today.getFullYear();

        if (dd < 10) { dd = '0' + dd }
        if (mm < 10) { mm = '0' + mm }

        let current = yyyy + '-' + mm + '-';

        
        
        

        

       


        const data = {
            labels: [
                'Boom Sales',
                'Refer a Friend',
                // 'Others'
            ],
            datasets: [{
                data: [this.state.counter_data.total_boom_amount_chart, this.state.counter_data.total_referral_amount_chart],
                backgroundColor: [
                    '#e8be28',
                    '#000000',
                    // '#FFCE56'
                ],
                hoverBackgroundColor: [
                    '#e8be28',
                    '#000000',
                    // '#FFCE56'
                ]
            }]
        };

        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2
          })


          const getPermissionOject = (value) => {
            const permissiondata = this.state.permissions.filter(function (e) {
                return e.permission_id == value;
            });

            if (permissiondata) {
                return permissiondata[0];
            }
        }



        //boom - 10588967
        const boomdata = getPermissionOject('10588967');
        var boom_r = 0;
        var boom_w = 0;
        var boom_u = 0;
        if (boomdata) {
            boom_r = boomdata.r;
            boom_w = boomdata.w;
            boom_u = boomdata.u;

        }

         

        //Notification - 42936533
        const notificationData = getPermissionOject('42936533');
        var notification_r = 0;
        var notification_w = 0;
        var notification_u = 0;
        if (notificationData) {
            notification_r = notificationData.r;
            notification_w = notificationData.w;
            notification_u = notificationData.u;
        }

        //User Dashboard // 55640434
        // const userDashboardData = getPermissionOject('55640434');
        var userdashboard_r = 1;
        var userdashboard_w = 1;
        var userdashboard_u = 1;
        // if (referAFriendData) {
        //     userdashboard_r = userDashboardData.r;
        //     userdashboard_w = userDashboardData.w;
        //     userdashboard_u = userDashboardData.u;
        // }

 


        //Program // 94012374
        //const programData = "";
        const programData = getPermissionOject('94012374');
        var program_r = 0;
        var program_w = 0;
        var program_u = 0;
        if (programData) {
            program_r = programData.r;
            program_w = programData.w;
            program_u = programData.u;
        }

        //Link // 91607899
        const linkData = getPermissionOject('91607899');
        var link_r = 0;
        var link_w = 0;
        var link_u = 0;
        if (linkData) {
            link_r = linkData.r;
            link_w = linkData.w;
            link_u = linkData.u;
        }

        //Support // 56519713
        const supportData = getPermissionOject('56519713');
        var support_r = 0;
        var support_w = 0;
        var support_u = 0;
        if (supportData) {
            support_r = supportData.r;
            support_w = supportData.w;
            support_u = supportData.u;
        }

        //FAQ // 16000381
        const faqData = getPermissionOject('16000381');
        var faq_r = 0;
        var faq_w = 0;
        var faq_u = 0;
        if (faqData) {
            faq_r = faqData.r;
            faq_w = faqData.w;
            faq_u = faqData.u;
        }

        //Event // 83407795
        const eventData = getPermissionOject('83407795');
        var eventData_r = 0;
        var eventData_w = 0;
        var eventData_u = 0;
        if (eventData) {
            eventData_r = eventData.r;
            eventData_w = eventData.w;
            eventData_u = eventData.u;
        }
 

         //permission // 69836958
         const permissionData = getPermissionOject('69836958');
         var permissionData_r = 0;
         var permissionData_w = 0;
         var permissionData_u = 0;
         if (permissionData) {
            permissionData_r = permissionData.r;
            permissionData_w = permissionData.w;
            permissionData_u = permissionData.u;
         }

          //permission // 54147970
          const referralsData = getPermissionOject('54147970');
          var referralsData_r = 0;
          var referralsData_w = 0;
          var referralsData_u = 0;
          if (referralsData) {
             referralsData_r = referralsData.r;
             referralsData_w = referralsData.w;
             referralsData_u = referralsData.u;
          }

          //users // 7573878
          const userData = getPermissionOject('7573878');
          var user_r = 0;
          var user_w = 0;
          var user_u = 0;
          if (userData) {
            user_r = userData.r;
            user_w = userData.w;
            user_u = userData.u;
          }

           


          if(this.state.user_id == '10001')
          {
            referralsData_r = 1;
            referralsData_w = 1;
            referralsData_u = 1;

            permissionData_r = 1;
            permissionData_w = 1;
            permissionData_u = 1;

            boom_r = 1;
            boom_w = 1;
            boom_u = 1;

            notification_r = 1;
            notification_w = 1;
            notification_u = 1;

            program_r = 1;
            program_w = 1;
            program_u = 1;

            link_r = 1;
            link_w = 1;
            link_u = 1;

            support_r = 1;
            support_w = 1;
            support_u = 1;  

            faq_r = 1;
            faq_w = 1;
            faq_u = 1;

            eventData_r = 1;
            eventData_w = 1;
            eventData_u = 1;

            user_r = 1;
            user_w = 1;
            user_u = 1;
          }

        return (
            <Fragment>
            <LoadingOverlay
                active={this.state.showLoader}
                //active='true'
                spinner
                text='Please wait...'>
            </LoadingOverlay>
            <SweetAlert
                type={this.state.showAlertIcon}
                title={this.state.showAlertMessage}
                onConfirm={this.sweetalertok}
                onCancel={() => this.setState({ showAlert: false })}
                show={this.state.showAlert}
            />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.boomDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlertBoom: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlertBoom}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.referralDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlertReferral: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlertReferral}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                {/* <Loader/> */}
                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Dashboard</h1>                                                   
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div className="col-md-12">                             
                            <Select
                                
                                value={this.state.selectedOption}
                                onChange={this.handleChangeSearch}
                                options={ this.state.options}
                                placeholder={this.state.defaultVlueOption.label}
                               
                                
                            />
                        </div>
                    </div>
                </div>
                <br/>



                <div className="program-progress">

                <div className="row">

                <div className="col-sm-8">
                         {program_r ? (<h5 className='text-center mb-3'>Program Progress</h5>) : (<h5 className='text-center mb-3'>You have no access to this module</h5>)}
                            <div className={program_r ? "" : "forblur"}>
                     
                        
                        <div class="accordion dashboardCounterDiv" id="accordionExample">

                        {
                            this.state.counters.map((pg, pgindex) => (

                                pg.program_title ?

                                (<div className="card">
                                <div className="card-header" id={'heading-'+pgindex} >
                                    <a className="btn btn-link d-flex program" data-toggle="collapse" data-target={'#collapse-'+pgindex} aria-expanded="false">
                            <div className='Title'>{pg.program_title} ({pg.category_exist})</div>
                                        <div className='Status'>

                            <span>{pg.completeProgramItems}/{pg.totalProgramItems} Tasks</span>
                                        </div>
                                        {/* <div className='Percentage'>{ pg.program_completed_percentage && pg.category_exist ? (pg.program_completed_percentage/pg.category_exist)*100 : 0}%</div> */}
                                        <div className='Percentage'>{ pg.completeProgramItemsPercent}%</div>
                                        {pg.category_exist > 0 ? <span className="arrow"></span> : ''}
                                    </a>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style={{backgroundColor: pg.program_color ? pg.program_color : '#e8be28', width: pg.completeProgramItemsPercent+'%'}}></div>
                                    </div>
                                </div>
                                


                                {/* Category */}
                                { pg.category_exist > 0 ?

                                    (
                                        <div id={'collapse-'+pgindex} className="collapse" aria-labelledby={'heading-'+pgindex} data-parent="#accordionExample" style={{border: '1px solid'}}>
                                    <Fragment>
                                    
                                    {pg.categories.map((ct, ctindex) => (
                                    <div className="card-body">

                                        <div class="accordion" id="levelOneaccordionExample">

                                            <div className="card">
                                                <div className="card-header" id={'levelOneheading-'+ctindex} >
                                                    <a className="btn btn-link d-flex program" data-toggle="collapse" data-target={'#levelOnecollapse-'+ctindex} aria-expanded="false">
                                                        <div className='Title cm-title' title={ct.category_title+' ('+ct.module_exist+')'}>{ct.category_title} ({ct.module_exist})</div>
                                                        <div className='Percentage text-center'>
                                                            <div class="progress progress-xs mediumHeight">
                                                                            
                                    {/* <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style={{ width: ct.module_complete_count && ct.module_exist ? (ct.module_complete_count/ct.module_exist)*100+'%' : 0+'%' }}> { ct.module_complete_count && ct.module_exist ? (ct.module_complete_count/ct.module_exist)*100 : 0}%</div> */}
                                    <div class="progress-bar" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style={{backgroundColor: pg.program_color ? pg.program_color : '#e8be28', width: ct.completeCategoryItemsPercent+'%' }}></div>
                                        <span style={{
                                            position: 'absolute',
                                            right: '0',
                                            left: '0',
                                            color: '#fff'
                                        }}>
                                       { ct.completeCategoryItemsPercent}%
                                        </span>
                                                            </div>
                                                        </div>
                                                        <div className='Status text-right'>
                                    
                                    <span>{ct.completeCategoryItems}/{ct.totalCategoryItems} Tasks</span>
                                                        </div>


                                                        {ct.module_exist > 0 ? <span className="arrow"></span> : ''}
                                                    </a>

                                                </div>
                                                { ct.module_exist > 0 ?
                                                (
                                                <div id={'levelOnecollapse-'+ctindex} className="collapse" aria-labelledby={'levelOneheading-'+ctindex} data-parent="#levelOneaccordionExample" >
                                                    <div className="card-body row" style={{ paddingTop: '6px' }}>
                                                        <div className="col">

                                                        
                                                            <div className="card">
                                                                <div className="card-header">
                                                                    <a className="btn btn-link d-flex Module">
                                                                        <div className="Title text-center">
                                                                            Modules
                                                               </div>

                                                                    </a>
                                                                </div>
                                                                <div className="card-body ModuleBody">
                                                                    <div class="accordion" id="levelSecondaccordionExample">

                                                                    {ct.modules.map((md, mdindex) => (
                                                                        <div className="card">
                                                                            <div className="card-header" id={'levelSecondheading-'+mdindex} >
                                                                                <a className="btn btn-link d-flex" data-toggle="collapse" data-target={'#levelSecondcollapse-'+mdindex} aria-expanded="false">
                                                                                {md.complete_status == 1 ? (<i class="fa fa-check-circle text-success"></i>) : (<i class="fa fa-check-circle"></i>)}{md.module_title} {md.topic_exist > 0 ? <span className="arrow"></span> : ''}
                                                                                </a>
                                                                            </div>
                                                                            {md.topic_exist > 0 ?
                                                                            (<div id={'levelSecondcollapse-'+mdindex} className="collapse" aria-labelledby={'levelSecondheading-'+mdindex} data-parent="#levelSecondaccordionExample">
                                                                                <div className="card-body">
                                                                                    <ul className='listing'>
                                                                                        {md.topics.map((tp, tpindex) => (
                                                                                            
                                                                                            <li>{tp.complete_status == 1 ? (<i class="fa fa-check-circle text-success"></i>) : (<i class="fa fa-check-circle"></i>)} {tp.topic_title} </li>
                                                                                            
                                                                                        ))}
                                                                                    </ul>
                                                                                </div>
                                                                            </div>) : ''}
                                                                        </div>
                                                                    ))}

                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             
                                                        </div>
                                                        <div className="col">
                                                            <div className="card">
                                                                <div className="card-header" >
                                                                    <a className="btn btn-link d-flex Module">
                                                                        <div className="Title text-center">Action Items </div>
                                                                    </a>
                                                                </div>
                                                                <div className="card-body ModuleBody">
                                                                   { ct.item_exist > 0 ?                          
                                                                    (<ul className='listing'>
                                                                    {ct.items.map((it, mdindex) => (
                                                                        <li>{it.complete_status ? <i class="fa fa-check-circle text-success"></i> : <i class="fa fa-check-circle"></i>} {it.item_title} </li>
                                                                    ))}
                                                                    </ul>
                                                                    ) :''}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                ):''}
                                            </div>

 


                                        </div>

                                    </div>
                                    ))}

                                    </Fragment> </div>) : '' }


                                
                            </div>) : ''
                            ))
                        }
                            


 

                        </div>
                        </div>

                    </div>




                    <div className="col-sm-4">
                        <h5 className='text-center mb-3'>Earnings</h5>
                        <div className="card">

                            <div className='dashboardChart'>

                                <Pie data={data} />

                                <div className='earning '>
                        <div className='text-center earning-amount'>${this.state.counter_data.total_income}</div>
                                    <div className='earning-list-wrapper '>
                                    <div className='earning-list'>
                                                <div>Boom Sales ({this.state.counter_data.boom_count})</div>
                                                {/* <div></div> */}
                                                <div>${this.state.counter_data.total_boom_amount}</div>
                                            </div>
                                            <div className='earning-list'>
                                                <div>Refer a Friend ({this.state.counter_data.referral_friend_count})</div>
                                                {/* <div></div> */}
                                                <div>${this.state.counter_data.total_referral_amount} </div>
                                            </div>
                                         
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>


                </div>
            </div>
             


            <div class="row clearfix row-deck">

            

            {this.state.member.member_id ? 
            (
            <div class="col-xl-12 col-lg-12 col-md-12">
            <div className="card">
                 
                <div className="card-body">
                    <div className="table-responsive">                                        
                        <form className='form-horizontal' onSubmit={this.RoleAddBtn}>
                                <div className="card">
                            <div className="card-header pb-0">
                                <h3 className="card-title">Member Details</h3>
                            </div>
                            <div className="card-body">
                                <div className="form-group row">                                    
                                        <div className="col-md-3 text-right">
                                            Name :
                                         </div>
                                         <div className="col-md-9 text-left">
                                            {this.state.member.first_name+" "+this.state.member.last_name}
                                        </div>                                                                 
                                </div>

                                <div className="form-group row">
                                    
                                        <div className="col-md-3 text-right">
                                            Email :
                                         </div>
                                         <div className="col-md-9 text-left">
                                         {this.state.member.email}
                                        </div>
                                                                       
                                </div>
                                <div className="form-group row">
                                    
                                        <div className="col-md-3 text-right">
                                            Status :
                                         </div>
                                         <div className="col-md-9 text-left">
                                         {this.state.member.status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                        </div>
                                                                
                                </div>

                                <div className="form-group row">                                    
                                        <div className="col-md-3 text-right">
                                            Date of Creation :
                                         </div>
                                         <div className="col-md-9 text-left">
                                         {this.state.member.created_date  ? ((new Date(this.convertUTCDateToLocalDate(new Date(this.state.member.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)) : 'Not Available'}
                                        </div>                                                                
                                </div>

                                <div className="form-group row">                                    
                                        <div className="col-md-3 text-right">
                                            Last Login :
                                         </div>
                                         <div className="col-md-9 text-left">
                                         {this.state.member.last_login  ? ((new Date(this.convertUTCDateToLocalDate(new Date(this.state.member.last_login)))).toLocaleDateString('en-US', DATE_OPTIONS)) : 'Not Available'}
                                        </div>                                                                
                                </div>

                                 
                            </div>
                        </div>
                            
                            
                             
                        </form>
                    </div>
                </div>
            </div>
            
            </div>) : ''}
            {/* Review */}
            {this.state.access_permission_review_r && ( <div class="col-xl-12 col-lg-12 col-md-12">

            

                <div className="card">
                    <div className="row ">
                        <div className="col">
                            <div className="card-header pb-0">
                                <h3 className="card-title">Reviews</h3>
                            </div>
                        </div>                                         
                    </div>
                    <div className="card-body">
                        <div className="table-responsive">
                                        
                            <table id='allreview' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        
                                        <th>Title</th>                                                                                  
                                        <th class='text-left'>Rating</th>
                                        <th class='text-left'>Date</th>
                                        <th class='text-left'>Member</th>
                                        <th class='text-left'>Status</th>
                                        <th class='text-center'>Publish</th>
                                    </tr>
                                </thead>                                        
                                <tbody>                                                
                                    {
                                        this.state.reviews.map((review, rindex) => (
                                            <tr key={rindex}>
                                                <td>{rindex + 1}</td>
                                               
                                                <td className="text-capitalize">

                                                    <a href={'/dashboard/review/details/'+review.review_id} >{review.title}</a>
                                                    {/* {review.program_title} */}
                                                </td>
                                                {/* <td className="text-capitalize">
                                                    {review.member_name}
                                                </td> */}
                                                {/* <td className="text-capitalize">
                                                <div dangerouslySetInnerHTML={{ __html: review.description }} />
                                                </td>                                                                 */}
                                                <td class='text-left brandColor'>
                                                <span className='dateFormat'>{review.rating}</span>
                                                    <span className={review.rating == '1.00' ? '' : 'hidden'}>
                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;                                                                                         
                                                    </span>

                                                    <span className={review.rating == '2.00' ? '' : 'hidden'}>
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                    </span>

                                                    <span className={review.rating == '3.00' ? '' : 'hidden'}>
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                    </span>

                                                    <span className={review.rating == '4.00' ? '' : 'hidden'}>
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                        <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                        <br/>
                                                        
                                                    </span>

                                                    <span className={review.rating == '5.00' ? '' : 'hidden'}>
                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                    <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                    </span>
                                                </td>  
                                                 
                                                
                                                <td className='text-left'>
                                                    <span className='dateFormat'>{review.created_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(review.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td> 

                                                <td class='text-left'>
                                                    {review.member_name}
                                                </td>

                                                <td class='text-left'>
                                                {review.status == 1 ? (<span className="tag tag-success">Published</span>) : (<span className="tag tag-danger">Hidden</span>)}                                                         
                                                </td>  
                                                
                                                <td class='text-center'>
                                                    <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                        <input type="checkbox" class="custom-control-input" name="review_id" value={review.review_id} onChange={this.handleReviewPublish} checked={review.status == 1 ? true : ''} defaultChecked={review.status == 1 ? true : ''}  />               <span class="custom-control-label">&nbsp;</span>
                                                    </label>
                                                </td>   

                                                
                                            </tr>
                                        ))
                                    }
                                </tbody>
                                {this.state.reviews.length == 0 && (<tfoot><tr><td colspan="8"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                            </table>
                        </div>
                    </div>
                </div>
            </div>    )}

                {/* sales */} 
                {this.state.access_permission_boom_r && ( <div class="col-xl-12 col-lg-12 col-md-12">
                    <div className="card">
                        <div class="card-header">
                                <h3 class="card-title">Sales</h3>
                        </div>
                        <div className="card-body">
                        <div className="table-responsive">                                                    
                        <table id='TblBoomSales' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th class='text-right'>Amount($)</th>
                                        <th class='text-left'>Date</th>
                                        {this.state.access_permission_boom_u && ( <th class='text-center'>Action</th>  )}                                                   
                                    </tr>
                                </thead>                                        
                                <tbody>
                                    {
                                        this.state.booms.map((boom, bindex) => (
                                            <tr>
                                                <td>{bindex + 1}</td>
                                                <td className="">
                                                    {boom.member_name}
                                                </td>
                                                
                                                <td className="">
                                                    {boom.member_email}
                                                </td>
                                                <td className="text-right">
                                                    {/* ${boom.amount} */}
                                                    {formatter.format(boom.amount)}
                                                </td>

                                                 
                                                <td className='text-left'>
                                                    <span className='dateFormat'>{boom.created_date}</span>
                                                    {/* {(new Date(this.convertUTCDateToLocalDate(new Date(boom.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)} */}
                                                    <Moment format="MMM D, YYYY" local>
                                                        {boom.created_date}
                                                    </Moment>
                                                </td>

                                                {this.state.access_permission_boom_u && (<td class='text-center'>
                                                    <a href={`/member/boom/sales/update/${this.state.member_id}/${boom.id}`} className="brandColor p-1" title="Edit"><i className="fa fa-pencil-square-o"></i></a>

                                                    <a href='javascript:void(0);' onClick={this.boomDelete()} data-id={boom.id} className="brandColor p-1" title="Edit"><i className="fa fa-trash text-danger"></i></a>                                                                                                        
                                                </td> )}                                                             
                                            </tr>
                                        ))
                                    }
                                </tbody>
                                {this.state.booms.length == 0 && (<tfoot><tr><td colspan="6"  className="text-center text-capitalize">{this.state.showCategoriesTableMessage}</td></tr></tfoot>)}
                            </table>
                        </div>
                        </div>
                    </div>
                </div>   )} 


                {/* sales */} 
                {this.state.access_permission_referral_r && ( <div class="col-xl-12 col-lg-12 col-md-12">
                    <div className="card">
                        <div class="card-header">
                                <h3 class="card-title">Referrals</h3>
                        </div>
                        <div className="card-body">
                        <div className="table-responsive">                                                    
                        <table id='TblReferralList' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th>#</th>

                                        <th className="text-left">Referral Name</th>
                                        <th className="text-left">Referral Email</th>
                                        <th className='text-right'>Earned($)</th> 
                                        <th className='text-left'>Date</th>
                                        <th className="text-left">Status</th>
                                        <th className="text-left">Referred Name</th>
                                        <th className="text-left">Referred Email</th>
                                                                                
                                        

                                        

                                        {this.state.access_permission_referral_u && (<th className='text-center'>Action</th>)}
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.refers.map((refer, refindex) => (
                                            <tr>
                                                <td>{refindex + 1}</td>

                                                <td className="text-left table_content">
                                                    {refer.referral_name}
                                                </td>
                                                <td className="text-left">
                                                    {refer.referral_email}
                                                </td>

                                                <td className="text-right">
                                                    {/* ${refer.amount} */}
                                                    {formatter.format(refer.amount)}
                                                </td>

                                                 

                                                

                                                <td className='text-left'>
                                                    <span className='dateFormat'>{refer.created_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(refer.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td>

                                                <td className='text-left'>
                                                    {refer.referred_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Pending</span>)}
                                                </td>

                                                <td className="text-left">
                                                    {refer.referred_name}
                                                </td>
                                                <td className="text-left">
                                                    {refer.referred_email}
                                                </td>
                                                
                                                
                                                

                                                
                                                
                                                {this.state.access_permission_referral_u && (<td className='text-center'>
                                                    {refer.referred_status == 1 ? (<a href={`/member/referral/update/${this.state.member_id}/${refer.referral_code}`} className="brandColor p-1" title="Edit"><i className="fa fa-pencil-square-o"></i></a>) : ''}
                                                    
                                                    {/* <button value={refer.referral_code}  onClick={this.programCategoryDelete()} className="btn btn-icon btn-sm " title="delete"><i className="fa fa-trash text-danger"></i></button> */}
                                                    <button value={refer.referral_code} onClick={this.referralDelete()} className="btn btn-icon btn-sm " title="delete"><i className="fa fa-trash text-danger"></i></button>
                                                </td>)}
                                            </tr>
                                        ))
                                    }
                                </tbody>
                                {this.state.refers.length == 0 && (<tfoot><tr><td colspan="9" className="text-center text-capitalize">{this.state.showCategoriesTableMessage ? this.state.showCategoriesTableMessage : 'No Referral'}</td></tr></tfoot>)}
                            </table>
                        </div>
                        </div>
                    </div>
                </div>)}

            </div>
            </Fragment>
        )
    }
}

export default DashBoardMember
