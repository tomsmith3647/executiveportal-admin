import React, { Component } from 'react'

import {Pie} from 'react-chartjs-2';

class DashboardNew extends Component {
    render() {

        const data = {
            labels: [
                'Boom Sales',
                'Refer a friend',
                'Others'
            ],
            datasets: [{
                data: [300, 50, 100],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ]
            }]
        };
        return (
            <div className="program-progress">

                <div className="row">

                    <div className="col-sm-8">
                        <h5 className='text-center mb-3'>Program Progress</h5>
                        <div class="accordion" id="accordionExample">
                            <div className="card">
                                <div className="card-header" id='heading-1' >
                                    <a className="btn btn-link d-flex program" data-toggle="collapse" data-target='#collapse-1' aria-expanded="false">
                                        <div className='Title'>Program 1</div>
                                        <div className='Status'>

                                            <span>10/10 Action Items</span>
                                        </div>
                                        <div className='Percentage'>100%</div>
                                        <span className="arrow"></span>
                                    </a>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style={{ width: '81%' }}></div>
                                    </div>
                                </div>
                                <div id='collapse-1' className="collapse" aria-labelledby='heading-1' data-parent="#accordionExample">



                                    <div className="card-body">

                                        <div class="accordion" id="levelOneaccordionExample">

                                            <div className="card">
                                                <div className="card-header" id='levelOneheading-1' >
                                                    <a className="btn btn-link d-flex program" data-toggle="collapse" data-target='#levelOnecollapse-1' aria-expanded="false">
                                                        <div className='Title'>Category 1</div>
                                                        <div className='Percentage text-center'>
                                                            <div class="progress progress-xs mediumHeight">
                                                                <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style={{ width: '81%' }}> 81%</div>
                                                            </div>
                                                        </div>
                                                        <div className='Status text-right'>
                                                            <span>10/10 Action Items</span>
                                                        </div>

                                                        <span className="arrow"></span>
                                                    </a>

                                                </div>
                                                <div id='levelOnecollapse-1' className="collapse" aria-labelledby='levelOneheading-1' data-parent="#levelOneaccordionExample" >
                                                    <div className="card-body row" style={{ paddingTop: '6px' }}>
                                                        <div className="col">
                                                            <div className="card">
                                                                <div className="card-header">
                                                                    <a className="btn btn-link d-flex Module">
                                                                        <div className="Title text-center">
                                                                            Modules
                                                               </div>

                                                                    </a>
                                                                </div>
                                                                <div className="card-body ModuleBody">
                                                                    <div class="accordion" id="levelSecondaccordionExample">
                                                                        <div className="card">
                                                                            <div className="card-header" id='levelSecondheading-1' >
                                                                                <a className="btn btn-link d-flex" data-toggle="collapse" data-target='#levelSecondcollapse-1' aria-expanded="false">
                                                                                    Modules - 1
                                                                        <span className="arrow"></span>
                                                                                </a>
                                                                            </div>
                                                                            <div id='levelSecondcollapse-1' className="collapse" aria-labelledby='levelSecondheading-1' data-parent="#levelSecondaccordionExample">
                                                                                <div className="card-body">
                                                                                    <ul className='listing'>
                                                                                        <li>Topic 1</li>
                                                                                        <li>Topic 2</li>
                                                                                        <li>Topic 3</li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="card">
                                                                            <div className="card-header" id='levelSecondheading-2' >
                                                                                <a className="btn btn-link d-flex" data-toggle="collapse" data-target='#levelSecondcollapse-2' aria-expanded="false">
                                                                                    Modules - 2
                                                                        <span className="arrow"></span>
                                                                                </a>
                                                                            </div>
                                                                            <div id='levelSecondcollapse-2' className="collapse" aria-labelledby='levelSecondheading-2' data-parent="#levelSecondaccordionExample">
                                                                                <div className="card-body">
                                                                                    <ul className='listing'>
                                                                                        <li>Topic 1</li>
                                                                                        <li>Topic 2</li>
                                                                                        <li>Topic 3</li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col">
                                                            <div className="card">
                                                                <div className="card-header" >
                                                                    <a className="btn btn-link d-flex Module">
                                                                        <div className="Title text-center">  Actions List </div>
                                                                    </a>
                                                                </div>
                                                                <div className="card-body ModuleBody">
                                                                    <ul className='listing'>
                                                                        <li>Topic 1</li>
                                                                        <li>Topic 2</li>
                                                                        <li>Topic 3</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="card">
                                                <div className="card-header" id='levelOneheading-2' >
                                                    <a className="btn btn-link d-flex" data-toggle="collapse" data-target='#levelOnecollapse-2' aria-expanded="false">
                                                        1111111111111
                                                 <span className="arrow"></span>
                                                    </a>
                                                </div>
                                                <div id='levelOnecollapse-2' className="collapse" aria-labelledby='levelOneheading-2' data-parent="#levelOneaccordionExample">
                                                    <div className="card-body">
                                                        Testing
                                            </div>
                                                </div>
                                            </div>



                                        </div>

                                    </div>
                                </div>
                            </div>




                            <div className="card">
                                <div className="card-header" id='heading-2' >
                                    <a className="btn btn-link d-flex program" data-toggle="collapse" data-target='#collapse-2' aria-expanded="false">
                                        <div className='Title'>Program 2</div>
                                        <div className='Status'>

                                            <span>8/10 Action Items</span>
                                        </div>
                                        <div className='Percentage'>100%</div>
                                        <span className="arrow"></span>
                                    </a>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style={{ width: '71%' }}></div>
                                    </div>
                                </div>
                                <div id='collapse-2' className="collapse" aria-labelledby='heading-2' data-parent="#accordionExample">
                                    <div className="card-body row">
                                        <div className='col'>
                                            asdasd
                                </div>
                                        <div className='col'>
                                            sadasd
                                </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>




                    <div className="col-sm-4">
                        <h5 className='text-center mb-3'>Your Earnings</h5>
                        <div className="card">

                            <div className='dashboardChart'>

                                <Pie data={data} />

                                <div className='earning '>
                                    <div className='text-center earning-amount'>$117,00</div>
                                    <div className='earning-list-wrapper '>
                                        <div className='earning-list'>
                                            <div>Boom earning</div>
                                            <div>33</div>
                                            <div>88%</div>
                                        </div>
                                        <div className='earning-list'>
                                            <div>Refer a friend</div>
                                            <div>33</div>
                                            <div>88%</div>
                                        </div>
                                        <div className='earning-list'>
                                            <div>Other</div>
                                            <div>22</div>
                                            <div>75%</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>


                </div>
            </div>
        )
    }
}

export default DashboardNew
