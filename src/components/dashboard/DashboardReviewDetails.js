import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
 
// Variable
const $ = window.$

export class DashboardReviewDetails extends Component {


    constructor(props) {
        super(props);



        this.state = {

            baseApiURL: '',
            showLoader: false,
            description: '',
            program_id: '',
            rating: '',
             
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showLoader: false,
            
            member: {},
            star_rating: [1, 2, 3, 4, 5],
            reviewrating: 0,
            title: '',
            note_counter: '500',
            curTime: '',
            rating: 0,

            offset: '',
            timeZone: '',
            memberPermission:[],
            review: {},
        }



    }


 

    componentDidMount() {

 
        //get user detail from memeber id
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
                memberPermission: userSession.permissions,
            })

            const review_id = this.props.match.params.review_id;
            const member_id = this.props.match.params.member_id;
            

            //get program List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/review/details/"+review_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ review: response.data.review_details, member: response.data.member });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


            

        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

 
 

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }
 
 
    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }


    render() {

         

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };


        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">

                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Review Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Review Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>






                </div>
                
                

                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-body">
                                {
                                    
                                        

                                              
                                                <div className="card-body">
                                                    <div className="form-group row">                                    
                                                            <div className="col-md-3 text-right">
                                                                Title :
                                                            </div>
                                                            <div className="col-md-9 text-left">
                                                                {this.state.review.title}
                                                            </div>                                                                 
                                                    </div>

                                                    <div className="form-group row">                                                        
                                                        <div className="col-md-3 text-right">
                                                            Rating :
                                                        </div>
                                                        <div className="col-md-9 text-left brandColor">
                                                            <span className={this.state.review.rating == '1.00' ? '' : 'hidden'}>
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;                                                                                         
                                                            </span>

                                                            <span className={this.state.review.rating == '2.00' ? '' : 'hidden'}>
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                            </span>

                                                            <span className={this.state.review.rating == '3.00' ? '' : 'hidden'}>
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                            </span>

                                                            <span className={this.state.review.rating == '4.00' ? '' : 'hidden'}>
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <br/>
                                                                
                                                            </span>

                                                            <span className={this.state.review.rating == '5.00' ? '' : 'hidden'}>
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                <i class="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                            </span>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div className="form-group row">                                                        
                                                        <div className="col-md-3 text-right">
                                                            Program :
                                                        </div>
                                                        <div className="col-md-9 text-left">
                                                        {this.state.review.program_title}
                                                        </div>                                                                                        
                                                    </div>

                                                    <div className="form-group row">                                                        
                                                        <div className="col-md-3 text-right">
                                                            Description :
                                                        </div>
                                                        <div className="col-md-9 text-left">
                                                        <div dangerouslySetInnerHTML={{ __html: this.state.review.description }} />
                                                        </div>                                                                                        
                                                    </div>

                                                    <div className="form-group row">                                                        
                                                        <div className="col-md-3 text-right">
                                                        Status :
                                                        </div>
                                                        <div className="col-md-9 text-left">
                                                            {this.state.review.status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </div>                                                                                        
                                                    </div>


                                                     

                                                    <div className="form-group row">                                    
                                                            <div className="col-md-3 text-right">
                                                                Author Name :
                                                            </div>
                                                            <div className="col-md-9 text-left">
                                                            {this.state.member.first_name+' '+this.state.member.last_name}
                                                            </div>                                                                
                                                    </div>

                                                    <div className="form-group row">                                    
                                                            <div className="col-md-3 text-right">
                                                                Author Email :
                                                            </div>
                                                            <div className="col-md-9 text-left">
                                                            {this.state.member.email}
                                                            </div>                                                                
                                                    </div>


                                                    <div className="form-group row">                                    
                                                            <div className="col-md-3 text-right">
                                                                Date :
                                                            </div>
                                                            <div className="col-md-9 text-left">
                                                            {(new Date(this.convertUTCDateToLocalDate(new Date(this.state.review.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                            </div>                                                                
                                                    </div>


                                                    
                                                </div>
                                           

                                             




  
                                     


                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>

        )
    }
}

export default DashboardReviewDetails
