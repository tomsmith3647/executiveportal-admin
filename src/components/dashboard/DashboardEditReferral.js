import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import LoadingOverlay from 'react-loading-overlay';
import { NavLink } from 'react-router-dom'
// Variable
const $ = window.$

export class DashboardEditReferral extends Component {

    state = {

        showAlert: false,
        showAlertMessage: '',
        member: {},
        amount: '0.00',
        member_referral: {},
        member_id: '',
        member_referral: { amount: '0.00' },

    }



    componentDidMount() {
        //get user detail from memeber id
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const referral_code = this.props.match.params.referral_code;
            
            if (referral_code) {
                //Get user profile details   
                this.setState({referral_code: referral_code, showLoader: true });
                axios({
                    url: API_URL + "admin/referral/details/" + referral_code,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        const ApiURL = response.data.api_url;
                        this.setState({ showLoader: false });
                        this.setState({ baseApiURL: ApiURL });
                        if (error_code == '200') {
                            //success                      
                            this.setState({ member_referral: response.data.member_referral, amount: response.data.member_referral.amount });

                        }
                        else {
                            //fail
                            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
            else {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "referral_code empty!" });
            }
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }

    }



    handleChange = (e) => {
        this.setState({
            ...this.state.member_referral,
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault();

    }





    DashboardEditReferral = (e) => {
        e.preventDefault();

        console.log(this.state.valid_permission);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,

                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            var ex = /^[0-9]+\.?[0-9]*$/;
            if (ex.test(this.state.amount) == false) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
                return false;
            }


            const member_id = this.props.match.params.member_id;

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/referral/update",
                method: 'post',
                data: {
                    
                    referral_code: this.state.referral_code,
                    amount: this.state.amount
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success        
                        if(member_id)
                        {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/dashboard/'+member_id});
                        }
                        else{
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/dashboard'});
                        }                       
                        

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '' });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions

    }



    checkValidAmount = (evt) => {

        var ex = /^[0-9]+\.?[0-9]*$/;
        if(this.state.amount)
        {
            if (ex.test(this.state.amount) == false) {
                evt.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
                return false;
            }
        }

        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            evt.preventDefault();
            return false;
        }
        else {
            return true;
        }
    }

    checkDec = (evt) => {
        var ex = /^[0-9]+\.?[0-9]*$/;
        if (ex.test(evt.target.value) == false) {
            evt.preventDefault();
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
            return false;
        }
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }
    render() {
        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Referral Update</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Referral Update</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className='user-profile'>
                    <div className="row">
                        <div className="col">
                            <div className="tab-content mt-3">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.DashboardEditReferral}>

                                        <div className="card">
                                            <div className="card-body">

                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Referral Amount</label>
                                                    <div className="col-md-7">
                                                        {
                                                            this.state.amount == '0.00' ?
                                                                (<input type="text" className="form-control" placeholder={this.state.amount} name='amount' onChange={this.handleChange} onKeyPress={this.checkValidAmount} onBlur={this.checkDec} />) :
                                                                (<input type="text" className="form-control" value={this.state.amount} name='amount' onChange={this.handleChange} onKeyPress={this.checkValidAmount} onBlur={this.checkDec} />)
                                                        }

                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-footer text-right">
                                                <button type="submit" className="btn btn-brand">Update</button>
                                            </div>
                                        </div>

                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>


            </Fragment>





        )
    }
}

export default DashboardEditReferral
