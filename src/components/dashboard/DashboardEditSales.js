import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import LoadingOverlay from 'react-loading-overlay';
import { NavLink } from 'react-router-dom'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// Variable
const $ = window.$

export class DashboardEditSales extends Component {

    state = {

        showAlert: false,
        showAlertMessage: '',
        member: {},
        amount: '0.00',
        member_referral: {},
        member_id: '',
        member_referral: { amount: '0.00' },
        boom_details:{},
        amount: '0.00',
        startDate: new Date(),
        currentDate: new Date(),
        sDate:'',
    }



    componentDidMount() {

         
        //get user detail from memeber id
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const boom_id = this.props.match.params.boom_id;
            
            if (boom_id) {
                //Get user profile details   
                this.setState({boom_id: boom_id, showLoader: true });
                axios({
                    url: API_URL + "admin/boom/details/" + boom_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        const ApiURL = response.data.api_url;
                        this.setState({ showLoader: false });
                        this.setState({ baseApiURL: ApiURL });
                        if (error_code == '200') {
                            //success       
                            const createDate = response.data.boom_details.created_timestamp;    
                             
                            //startDate: createDate
                            this.setState({ boom_details: response.data.boom_details, amount: response.data.boom_details.amount, startDate: createDate, sDate: response.data.boom_details.created_date});

                        }
                        else {
                            //fail
                            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
            else {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "boom_id empty!" });
            }
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }

    }



    handleChange = (e) => {
        this.setState({
            ...this.state.member_referral,
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault();

    }





    DashboardEditSales = (e) => {
        e.preventDefault();

        console.log(this.state.valid_permission);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,

                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            var ex = /^[0-9]+\.?[0-9]*$/;
            if (ex.test(this.state.amount) == false) {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
                return false;
            }


            const member_id = this.props.match.params.member_id;
            const boom_id = this.props.match.params.boom_id;

            if(!this.state.startDate)
            {
                e.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please select / enter valid date', showAlertActionURL: '' });
                return false;
            }


            
               
 
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/dashboard/boom/update",
                method: 'post',
                data: {
                    
                    boom_id: this.state.boom_id,
                    amount: this.state.amount,
                    createDate: this.state.startDate
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success        
                        if(member_id)
                        {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/dashboard/'+member_id});
                        }
                        else{
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/dashboard'});
                        }                       
                        

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '' });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions

    }



    checkValidAmount = (evt) => {

        var ex = /^[0-9]+\.?[0-9]*$/;
        if(this.state.amount)
        {
            if (ex.test(this.state.amount) == false) {
                evt.preventDefault();
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
                return false;
            }
        }

        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            evt.preventDefault();
            return false;
        }
        else {
            return true;
        }
    }

    checkDec = (evt) => {
        var ex = /^[0-9]+\.?[0-9]*$/;
        if (ex.test(evt.target.value) == false) {
            evt.preventDefault();
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please enter valid amount', showAlertActionURL: '' });
            return false;
        }
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }

    handleChangeDate = (date) => {

        //const selectDate = this.convertUTCDateToLocalDate(date);        
        const timestamp = Date.parse(date);         
        if(timestamp < 1)
        {            
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please select / enter valid date', showAlertActionURL: '' });
            return false;
        }

        this.setState({
          startDate: timestamp
        });
      };


    render() {

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        //Current Date
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; //January is 0!
        let yyyy = today.getFullYear();

        if (dd < 10) { dd = '0' + dd }
        if (mm < 10) { mm = '0' + mm }

        let current = yyyy + '-' + mm + '-';

        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Sales Update</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Sale Update</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className='user-profile'>
                    <div className="row">
                        <div className="col">
                            <div className="tab-content mt-3">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.DashboardEditSales}>

                                        <div className="card">
                                            <div className="card-body">
                                                                                            <div className="form-group row">
        <label className="col-md-3 col-form-label">Sale Amount </label>
                                                    <div className="col-md-7">
                                                        {
                                                            this.state.amount == '0.00' ?
                                                                (<input type="text" className="form-control" placeholder={this.state.amount} name='amount' onChange={this.handleChange} onKeyPress={this.checkValidAmount} onBlur={this.checkDec} />) :
                                                                (<input type="text" className="form-control" value={this.state.amount} name='amount' onChange={this.handleChange} onKeyPress={this.checkValidAmount} onBlur={this.checkDec} />)
                                                        }

                                                    </div>
                                                </div>
                                             
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Date</label>
                                                    <div className="col-md-7">
                                                    <DatePicker
                                                            
                                                            
                                                            className="form-control"
                                                            selected={this.state.startDate}
                                                            onChange={this.handleChangeDate}
                                                            maxDate={this.state.currentDate}
                                                            //dateFormat="yyyy/MM/dd h:mm aa"
                                                        />

 
                                                    </div>
                                                </div>
                                                </div>
                                            <div className="card-footer text-right">
                                                <button type="submit" className="btn btn-brand">Update</button>
                                            </div>
                                        </div>

                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>


            </Fragment>





        )
    }
}

export default DashboardEditSales
