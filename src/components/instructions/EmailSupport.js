import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class EmailSupport extends Component {

    state = {
        
        
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        instruction: {},

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        instruction_id:'10101010',
        title:'',
        description:'',
        status:'',
         
    }

    componentDidMount() {

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


             
            this.setState({ instruction: { instruction_id: this.state.instruction_id } });
            
            this.getInstructionDetails();
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }


    getInstructionDetails = () => {

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));                                   
        if(this.state.instruction_id)
        {
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/instruction/details/" + this.state.instruction_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                           
                    this.setState({ instruction: response.data.instruction, showAlertActionURL: ''});
                }
                else {
                    //fail
                    this.setState({ showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });            
        }         
    }
 
    
 
    handleChange = (e) => {
        this.setState({ instruction: { ...this.state.instruction, title: e.target.value } });
    }

 

     handleChangeLinkStatus = (e) => {
        this.setState({ instruction: { ...this.state.instruction, status: e.target.value } });
    }
 
    

    handleSummerNote = (e) => {
        this.setState({

            instruction: {
                ...this.state.instruction,
                description: e
            }
        })
    }
 

    //add task
    instructionUpdateBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            this.setState({ instruction: { ...this.state.instruction, instruction_id: this.state.instruction_id } });
             
            this.setState({ instruction: { ...this.state.instruction, status: 1 } });
  
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/instruction/email/support/update",
                method: 'post',
                data: this.state.instruction,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {

                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success 
                        
                    this.getInstructionDetails();
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message,  showAlertActionURL: ''});
                    
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL == 'refresh') {
            window.location.reload(); // '/dashboard';
        }
        else if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

  



    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Email Support</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                                                                          
                                    <li className="breadcrumb-item active" aria-current="page">Instructions / Email Support</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-6 col-md-12">
                        <form className='form-horizontal' onSubmit={this.instructionUpdateBtn}>
                        <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" readOnly="true" className="form-control" value={this.state.instruction.title} name='title' onChange={this.handleChange} required />
                                        </div>
                                    </div>                                
                                </div>
                            </div>

                         
                            

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                         value={this.state.instruction.description}

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>

                                



                            </div>



                            {/* <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='status' onChange={this.handleChangeLinkStatus}>
                                                <option selected={this.state.instruction.status == 1} value="1">Active</option>
                                                <option selected={this.state.instruction.status == 0} value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                             
                            <div className="card-footer text-right">                                
                            <button type="submit" className="btn btn-brand">Update</button>
                            </div>
                        </form>


                    </div>


                     

                </div>
            </Fragment>

        )
    }
}

export default EmailSupport
