import React from 'react';

const SectionBodyWrapper = (props) => {
    return (
        <div className={`section-body ${props.classes ? props.classes : ''}`} id={`${props.id ? props.id : ''}`}>
            <div className="container-fluid">
                {props.children}
            </div>
        </div>
    );
}

export default SectionBodyWrapper;
