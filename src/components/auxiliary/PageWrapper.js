import React from 'react';
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';

const PageWrapper = (props) => {
    return (
        <div className={`page ${props.classes ? props.classes : ''}`}>
            {props.children}
        </div>
    );
}

export default PageWrapper;
