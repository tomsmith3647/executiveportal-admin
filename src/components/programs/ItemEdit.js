import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
class ItemUpdate extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        item: {},
        item: {
            item_description: '',
            item_title: ''
        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        access_permission:false,
    }

    componentDidMount() {



        $('#activeitem').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const item_id = this.props.match.params.item_id;
            this.setState({ item_id: item_id });
            this.setState({ item: { item_id: item_id } });

            axios({
                url: API_URL + "admin/program/category/item/detail/" + item_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ access_permission: response.data.access_permission });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success       
                        //this.setState({ item:{...this.state.item, program_id: response.data.item.program_id, category_id: response.data.item.category_id} });    

                        this.setState({ item: response.data.item });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    // handleChangemodule = (e) => {
    //     const moduleData = this.state.module;
    //     moduleData[e.target.name] = e.target.value;
    //     this.setState({ module: moduleData });
    // }
    handleChangeitem = (e) => {
        this.setState({ item: { ...this.state.item, item_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            item: {
                ...this.state.item,
                item_description: e
            }
        })
    }
    //add module
    ItemUpdateBtn = () => {

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/item/update",
                method: 'post',
                data: this.state.item,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.item.category_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeItemStatus = (e) => {
        this.setState({ item: { ...this.state.item, item_status: e.target.value } });
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Edit Item</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.item.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.item.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Edit Item</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <form className='form-horizontal' onSubmit={this.handleSubmit}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name {this.state.item.item_title}</h3>
                                </div>
                                <div className="card-body">


                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.item.item_title} name='item_title' onChange={this.handleChangeitem} />
                                        </div>
                                    </div>

                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}


                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value={this.state.item.item_description}

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='item_status' onChange={this.handleChangeItemStatus}>
                                                <option selected={this.state.item.item_status == 1} value="1">Active</option>
                                                <option selected={this.state.item.item_status == 0} value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div className="card-footer text-right">

                            <button type="submit" onClick={this.ItemUpdateBtn} className="btn btn-brand">Update</button>
                        </div>
                    </div>

                </div>
            </Fragment>

        )
    }
}

export default ItemUpdate
