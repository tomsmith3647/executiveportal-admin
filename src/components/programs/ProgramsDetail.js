import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import reactCSS from 'reactcss'
import {SketchPicker, PhotoshopPicker, ChromePicker} from 'react-color';

const $ = window.$
class ProgramsDetail extends Component {

    
    


    state = {
        member: [],
        name: '',
        program_id: '',
        notes: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        program: {},
        categories: [],
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showUsersTableMessage: '',
        selected_profile_icon:'',
        members: [],
        category_sort: [],
        pgcolor:'#F1CA29',
        pgcolorrgb:{},
        displayColorPicker:false,
        access_permission:false,

    }

    componentDidMount() {

        this.setState({ showLoader: false });

        

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const program_id = this.props.match.params.program_id;
            this.setState({ program_id: program_id });
            this.setState({ program: { program_id: program_id } });
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/detail/" + program_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ access_permission: response.data.access_permission });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ program: response.data.program });
                        if(response.data.program.program_color)
                        {
                            const rgbcolor = this.convertHex(response.data.program.program_color);
                            this.setState({pgcolorrgb: rgbcolor});
                        }

                        $('.dropify').dropify();
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            //get Category

            this.setState({ showLoader: true, showCategoriesTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/program/category/list/" + program_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ categories: response.data.categories });

                        var categoryTable = $('#categoriestbl').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [3], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        

                        categoryTable.columns().iterator('column', function (ctx, idx) {
                            $(categoryTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        categoryTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var category_sort = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = categoryTable.row(diff[i].node).data();
                                const cat_id = $(rowData[1]).attr("data-id");
                                const cat_sort = { "category_id": cat_id, "position": diff[i].newData };
                                category_sort.push(cat_sort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/program/category/position/update",
                                method: 'post',
                                data: { sorting_order: category_sort },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showCategoriesTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });




            this.setState({ showLoader: true, showUsersTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/program/member/list/" + program_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message; //
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });

                    if (error_code == '200') {
                        //success                        
                        this.setState({ members: response.data.members });

                        var userTable = $('#pusersTable').DataTable({

                            'columnDefs': [{
                                'targets': [4], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        userTable.columns().iterator('column', function (ctx, idx) {
                            $(userTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


               
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }


    convertHex = (hex,opacity=100) => {
        hex = hex.replace('#','');
        const r = parseInt(hex.substring(0,2), 16);
        const g = parseInt(hex.substring(2,4), 16);
        const b = parseInt(hex.substring(4,6), 16);
    
        //const result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
        const a = opacity/100;
        const result = {r,g,b,a}
        return result;
    }


    //categoryPositionOrderUpdate
    categoryPositionOrderUpdate = (orderData = []) => {

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });
            if (orderData.length > 0) {
                axios({
                    url: API_URL + "admin/program/category/position/update",
                    method: 'post',
                    data: orderData,
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success     

                        }
                        else {
                            //fail
                            //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
        }
    }




    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    handleChangeProgram = (e) => {
        this.setState({ program: { ...this.state.program, program_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            program: {
                ...this.state.program,
                program_description: e
            }
        })
    }

    handleChangeStatus = (e) => {
        this.setState({ program: { ...this.state.program, program_status: e.target.value } });
    }

    //handleImageIcon

    handleImageIcon = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
            }
            else {
                this.setState({
                    selected_profile_icon: event.target.files[0],
                })
            }
        }
    }



    handleImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
            }
            else {
                this.setState({
                    selected_profile_image: event.target.files[0],
                })
            }
        }
    }


    //Update program
    programUpdateBtn = () => {

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/update",
                method: 'post',
                data: this.state.program,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success     
                        if (this.state.selected_profile_image || this.state.selected_profile_icon) {
                            const imagedata = new FormData()
                            imagedata.append('icon', this.state.selected_profile_icon)
                            imagedata.append('image', this.state.selected_profile_image)
                            imagedata.append('program_id', response.data.program.program_id)
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "admin/program/image/upload",
                                method: 'post',
                                data: imagedata,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/programs' });
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/programs' });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
    }


    // handleImage = (event) => {

    //     const userSession = JSON.parse(window.localStorage.getItem('userSession'));
    //     console.log(event.target.files[0])
    //     if (!event.target.files[0]) {
    //         this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
    //     }
    //     else {
    //         this.setState({
    //             showLoader: true,
    //         })
    //         const imagedata = new FormData()
    //         imagedata.append('image', event.target.files[0])
    //         imagedata.append('program_id', this.state.program_id)
    //         axios({
    //             url: API_URL + "admin/program/image/upload",
    //             method: 'post',
    //             data: imagedata,
    //             headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
    //         })
    //             .then(response => {

    //                 const error_code = response.data.error_code;
    //                 const error_message = response.data.error_message;
    //                 if (error_code == '200') {
    //                     //success                        
    //                     this.setState({showAlertIcon : 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
    //                     //window.location.href = '/dashboard'
    //                 }
    //                 else {
    //                     //fail
    //                     this.setState({showAlertIcon : 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
    //                 }
    //             })
    //             .catch(err => {
    //                 //error
    //                 console.log(err);
    //             });

    //     }
    // }

    //programCategoryDelete    
    programCategoryDelete = () => (e) => {
        const category_id = e.currentTarget.value;
        this.setState({ selected_category_id: category_id });
        this.setState({ showConfirmationAlert: true });
    }

    //subAdminDelete
    programCategoryDeleteProcess = () => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const category_id = this.state.selected_category_id;

            axios({
                url: API_URL + "admin/program/category/delete",
                method: 'post',
                data: {
                    category_id: category_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            program/category/module/details         
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/details/' + this.state.program_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }

    //handleMemberProgramMapping
    handleMemberProgramMapping = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        const status = isChecked;

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });
            //const admin_id = e.currentTarget.value;
            const category_id = this.state.selected_category_id;

            axios({
                url: API_URL + "admin/program/member/mapping/add",
                method: 'post',
                data: {
                    program_id: this.state.program_id,
                    member_id: e.target.value,
                    status: status,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            program/category/module/details         
                        //this.setState({showAlertIcon : 'success', showLoader:false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/details/'+this.state.program_id});                                              
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
    }

    handleChangeComplete = (color, event) => {
        this.setState({ pgcolor: color.hex, pgcolorrgb: color.rgb });  
        this.setState({
            program: {
                ...this.state.program,
                program_color: color.hex
            }
        })      
      };
          
      handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
      };
    
      handleClose = () => {
        this.setState({ displayColorPicker: false })
      };

    render() {


        const styles = reactCSS({
            'default': {
              color: {
                width: '36px',
                height: '14px',
                borderRadius: '2px',
                background: `rgba(${ this.state.pgcolorrgb.r }, ${ this.state.pgcolorrgb.g }, ${ this.state.pgcolorrgb.b }, ${ this.state.pgcolorrgb.a })`,
                //background:`${this.state.pgcolorrgb }`, 
              },
              swatch: {
                padding: '5px',
                background: '#fff',
                borderRadius: '1px',
                boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                display: 'inline-block',
                cursor: 'pointer',
              },
              popover: {
                position: 'absolute',
                zIndex: '2',
              },
              cover: {
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
              },
            },
          });

        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.programCategoryDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>



                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Program Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Program </li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                {this.state.access_permission &&
                (<div className="row">
                    <div className="col-xl-5 col-md-12">
                        <form className='form-horizontal' onSubmit={this.handleSubmit}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Title</h3>
                                </div>
                                <div className="card-body">


                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.program.program_title} name='program_title' onChange={this.handleChangeProgram} />
                                        </div>
                                    </div>

                                    {/* <div className="card-footer text-right">
                                            <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                        </div> */}


                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value={this.state.program.program_description}
                                        popover={{
                                            image: [],
                                            link: [],
                                            air: []
                                        }}
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}
                                    />
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Program Icon   </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="file" onChange={this.handleImageIcon} data-height="200" className="dropify" data-default-file={this.state.program.program_icon ? this.state.baseApiURL + this.state.program.program_icon : '/assets/images/gallery/1.jpg'} />
                                    <span><b>Note:</b> Please upload grayscale (color: #7D8490) icon with transparent background for better user experience.</span>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Program Image   </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file={this.state.program.program_image ? this.state.baseApiURL + this.state.program.program_image : '/assets/images/gallery/1.jpg'} />
                                    
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Program Color </h3>
                                </div>
                                <div className="card-body text-center">
                                    <div style={ styles.swatch } onClick={ this.handleClick }>
                                    <div style={ styles.color } />
                                    </div>
                                    { this.state.displayColorPicker ? <div style={ styles.popover }>
                                    <div style={ styles.cover } onClick={ this.handleClose }/>
                                    <SketchPicker color={ this.state.pgcolorrgb } onChange={ this.handleChangeComplete } />
                                    </div> : null }                                      
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='program_status' onChange={this.handleChangeStatus}>
                                                <option selected={this.state.program.program_status == 1} value="1">Active</option>
                                                <option selected={this.state.program.program_status == 0} value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer text-right">
                                <button type="submit" onClick={this.programUpdateBtn} className="btn btn-brand">Update</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-xl-7 col-md-12">
                        <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Categories</h3>
                                    </div>
                                </div>

                                {this.state.access_permission &&
                                    (<div className="col">
                                        <div className="card-header pb-0 justify-content-end">
                                            <NavLink to={`/program/category/add/${this.state.program_id}`} className='btn btn-brand' title="add">Add Category</NavLink>
                                        </div>
                                    </div>)}
                            </div>
                            <div className="card-body">
                                <div id="result"></div>
 
                                
                                

                                <div className="table-responsive">                                                                
                                    <table id='categoriestbl' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Category</th>
                                                <th class='text-left'>Status</th>
                                                <th class='text-center'>Modules</th>
                                                <th class='text-center'>Resources</th>
                                                <th class='text-center'>Topics</th>
                                                <th class='text-center'>Action Item</th>
                                                
                                                <th class='text-center'>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.categories.map((category, cindex) => (
                                                    <tr>
                                                        <td className='reorder text-left'>{cindex + 1}</td>
                                                        <td className="text-capitalize pl-1 table_content">
                                                            <NavLink data-id={category.category_id} to={`/program/category/details/${category.category_id}`} className="text-info brandColor text-left" title="View">{category.category_title}</NavLink>

                                                        </td>
                                                        <td class='text-left'>
                                                            {category.category_status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>

                                                        <td class='text-center'>
                                                            {category.module_count == 0 ? '--' : category.module_count}
                                                        </td>

                                                        <td class='text-center'>
                                                            {category.resource_count == 0 ? '--' : category.resource_count}
                                                        </td>

                                                        <td class='text-center'>
                                                            {category.topic_count == 0 ? '--' : category.topic_count}
                                                        </td>

                                                        <td class='text-center'>
                                                            {category.item_count == 0 ? '--' : category.item_count}
                                                        </td>

                                                        <td class='text-center'>
                                                            {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                            <NavLink to={`/program/category/details/${category.category_id}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>


                                                            <button value={category.category_id} onClick={this.programCategoryDelete()} className="btn btn-icon btn-sm " title="delete"><i class="fa fa-trash text-danger"></i></button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.categories.length == 0 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">{this.state.showCategoriesTableMessage ? this.state.showCategoriesTableMessage : 'No Category'}</td></tr></tfoot>)}
                                    </table>
                                    
                                </div>
                            </div>
                        </div>




                        {/* <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Members</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table id='pusersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th class='text-center'>Action</th>
                                                <th class='text-center'>Name</th>
                                                <th class='text-center'>Email</th>
                                                <th class='text-center'>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.members.map((member, index) => (
                                                    <tr>
                                                        <td>{index + 1}</td>
                                                        <td class='text-center'>
                                                            <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                <input type="checkbox" class="custom-control-input" name="member_id" value={member.member_id} onChange={this.handleMemberProgramMapping} defaultChecked={member.member_program_status == 1 ? true : ''} />               <span class="custom-control-label">&nbsp;</span>
                                                            </label>
                                                        </td>
                                                        <td class='text-center'><NavLink to={`/user/detail/${member.member_id}`} className="btn btn-icon btn-sm text-info brandColor text-left" title="Member">{member.first_name} {member.last_name}</NavLink></td>
                                                        <td class='text-center'>{member.email}</td>
                                                        <td class='text-center'>
                                                            {member.status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.members.length == 0 && (<tfoot><tr><td colspan="5" className="text-center text-capitalize">{this.state.showUsersTableMessage}</td></tr></tfoot>)}
                                    </table>
                                </div>
                            </div>
                        </div> */}
                    </div>
                </div>)}
            </Fragment>

        )
    }
}

export default ProgramsDetail
