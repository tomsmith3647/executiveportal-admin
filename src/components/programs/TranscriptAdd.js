import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
class TranscriptAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        task: {
            task_description: '',
            task_title: ''
        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
    }

    componentDidMount() {



        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const module_id = this.props.match.params.module_id;
            this.setState({ module_id: module_id });
            this.setState({ task: { module_id: module_id } });

            axios({
                url: API_URL + "admin/program/category/module/detail/" + module_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ task: { ...this.state.task, program_id: response.data.module.program_id, category_id: response.data.module.category_id } });
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    // handleChangetask = (e) => {
    //     const taskData = this.state.task;
    //     taskData[e.target.name] = e.target.value;
    //     this.setState({ task: taskData });
    // }
    handleChangetask = (e) => {
        this.setState({ task: { ...this.state.task, task_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            task: {
                ...this.state.task,
                task_description: e
            }
        })
    }
    //add task
    TaskAddBtn = () => {
        console.log(this.state.member);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/module/task/add",
                method: 'post',
                data: this.state.task,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Task Add</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.task.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.task.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/module/details/${this.state.module_id}`}>Module</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Task Add</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <form className='form-horizontal' onSubmit={this.handleSubmit}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">


                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.task.task_title} name='task_title' onChange={this.handleChangetask} />
                                        </div>
                                    </div>

                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}


                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value=""

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>
                        </form>

                        <div className="card-footer text-right">

                            <button type="submit" onClick={this.TaskAddBtn} className="btn btn-brand">Task Add</button>
                        </div>
                    </div>

                </div>
            </Fragment>

        )
    }
}

export default TranscriptAdd
