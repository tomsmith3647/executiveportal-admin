import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import reactCSS from 'reactcss'
import {SketchPicker, PhotoshopPicker, ChromePicker} from 'react-color';

const $ = window.$
class ProgramAdd extends Component {

    state = {
        name: '',
        notes: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        program: {
            program_description: '',
            program_title: ''
        },
        categories: [],
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_profile_image: '',
        selected_profile_icon:'',
        members: [],
        src: null,
        crop: {
            unit: '%',
            width: 30,
            aspect: 16 / 9,
        },
        pgcolor:'#F1CA29',
        pgcolorrgb:{},
        displayColorPicker:false,
        access_permission:false,
    }

    componentDidMount() {


        const rgbcolor = this.convertHex(this.state.pgcolor);

        this.setState({pgcolorrgb: rgbcolor});


        // const pixepixelarityCss = document.createElement("link");
        // pixepixelarityCss.rel = "stylesheet";      
        // pixepixelarityCss.href = "/assets/plugins/croptool/pixelarity.css";         
        // document.head.appendChild(pixepixelarityCss);

        // const pixelarityFace = document.createElement("script");
        // pixelarityFace.src = "/assets/plugins/croptool/pixelarity-face.js";         
        // document.body.appendChild(pixelarityFace);



        // const scriptFace = document.createElement("script");
        // scriptFace.src = "/assets/plugins/croptool/script-face.js";         
        // document.body.appendChild(scriptFace);



        $('.dropify').dropify();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const program_id = this.props.match.params.program_id;

            axios({
                url: API_URL + "admin/program/member/list/" + program_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message; //
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });

                    if (error_code == '200') {
                        //success                        
                        this.setState({ members: response.data.members });
                        console.log(this.state.members);
                        $('#pusersTable').DataTable({
                            'columnDefs': [{
                                'targets': [4], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }]
                        });

                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });

        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    
        convertHex = (hex,opacity=100) => {
        hex = hex.replace('#','');
        const r = parseInt(hex.substring(0,2), 16);
        const g = parseInt(hex.substring(2,4), 16);
        const b = parseInt(hex.substring(4,6), 16);
    
        //const result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
        const a = opacity/100;
        const result = {r,g,b,a}
        return result;
    }

    
 


    // handleChangeProgram = (e) => {
    //     const programData = this.state.program;
    //     programData[e.target.name] = e.target.value;
    //     this.setState({ program: programData });
    // }
    handleChangeProgram = (e) => {
        this.setState({ program: { ...this.state.program, program_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            program: {
                ...this.state.program,
                program_description: e
            }
        })
    }


    handleImageIcon = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
            }
            else {
                this.setState({
                    selected_profile_icon: event.target.files[0],
                })
            }
        }
    }


    handleImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
            }
            else {
                this.setState({
                    selected_profile_image: event.target.files[0],
                })
            }
        }
    }


    //add program
    programAdd = (e) => {
        e.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,

                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const thumImageData = $("#thumbresult").attr("src");
            //alert(thumImageData);


            if (!this.state.program.program_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter title." });
                return false;
            }



            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/add",
                method: 'post',
                data: this.state.program,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success
                        if (this.state.selected_profile_image || this.state.selected_profile_icon) {
                            const imagedata = new FormData()
                            imagedata.append('icon', this.state.selected_profile_icon)
                            imagedata.append('image', this.state.selected_profile_image)
                            imagedata.append('program_id', response.data.program.program_id)
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "admin/program/image/upload",
                                method: 'post',
                                data: imagedata,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/programs' });
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/programs' });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }




    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );


            this.setState({ croppedImageUrl });
        }


    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }


    handleChangeComplete = (color, event) => {
        this.setState({ pgcolor: color.hex, pgcolorrgb: color.rgb });
        this.setState({
            program: {
                ...this.state.program,
                program_color: color.hex
            }
        })
        
      };


      
    
      handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
      };
    
      handleClose = () => {
        this.setState({ displayColorPicker: false })
      };

    

       

    render() {
        const { crop, croppedImageUrl, src } = this.state;

        
    const styles = reactCSS({
        'default': {
          color: {
            width: '36px',
            height: '14px',
            borderRadius: '2px',
            background: `rgba(${ this.state.pgcolorrgb.r }, ${ this.state.pgcolorrgb.g }, ${ this.state.pgcolorrgb.b }, ${ this.state.pgcolorrgb.a })`,
            //background:`${this.state.pgcolorrgb }`, 
          },
          swatch: {
            padding: '5px',
            background: '#fff',
            borderRadius: '1px',
            boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
            display: 'inline-block',
            cursor: 'pointer',
          },
          popover: {
            position: 'absolute',
            zIndex: '2',
          },
          cover: {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px',
          },
        },
      });

        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Program</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Add Program</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-5 col-md-12">
                        <form className='form-horizontal' onSubmit={this.programAdd}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Title</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.program.program_title} name='program_title' onChange={this.handleChangeProgram} required />
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">





                                    <ReactSummernote
                                        value=""
                                        required
                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}
                                    />
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Program Icon   </h3>
                                </div>
                                <div className="card-body">
                                    {/* <input id="thumbfile" type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file={this.state.program.program_icon ? this.state.baseApiURL + this.state.program.program_icon : '/assets/images/gallery/1.jpg'} />                                      */}
                                    <input type="file" name="file" onChange={this.handleImageIcon} data-height="200" className="dropify" data-default-file={this.state.program.program_icon ? this.state.baseApiURL + this.state.program.program_icon : '/assets/images/gallery/1.jpg'} />
                                    <div class="col-md-6 mx-auto">
                                        <img id="thumbresult" />
                                    </div>
                                    <span><b>Note:</b> Please upload grayscale (color: #7D8490) icon with transparent background for better user experience.</span>
                                </div>
                            </div>

                            
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Program Image   </h3>
                                </div>
                                <div className="card-body">
                                    {/* <input id="thumbfile" type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file={this.state.program.program_icon ? this.state.baseApiURL + this.state.program.program_icon : '/assets/images/gallery/1.jpg'} />                                      */}
                                    <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file={this.state.program.program_image ? this.state.baseApiURL + this.state.program.program_image : '/assets/images/gallery/1.jpg'} />
                                    <div class="col-md-6 mx-auto">
                                        <img id="thumbresult" />
                                    </div>
                                    
                                </div>
                            </div>

                            
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Program Color </h3>
                                </div>
                                <div className="card-body text-center">
                                    <div style={ styles.swatch } onClick={ this.handleClick }>
                                    <div style={ styles.color } />
                                    </div>
                                    { this.state.displayColorPicker ? <div style={ styles.popover }>
                                    <div style={ styles.cover } onClick={ this.handleClose }/>
                                    <SketchPicker color={ this.state.pgcolor } onChange={ this.handleChangeComplete } />
                                    </div> : null }                                      
                                </div>
                            </div>
                             
                            <div className="card-footer text-right">
                                <button type="submit" className="btn btn-brand">Submit</button>
                            </div>
                        </form>


                    </div>



                    <div className="col-xl-7 col-md-12">
                        <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Categories</h3>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="card-header pb-0 justify-content-end">

                                        <button className='btn btn-brand' disabled={true}>Add Category</button>

                                    </div>

                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">

                                    <table id='categoriestbl' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Category</th>
                                                <th class='text-center'>Status</th>
                                                <th class='text-center'>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot><tr><td colspan="4" className="text-center text-capitalize">'No Category'</td></tr></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>


                        {/* <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Users</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table id='pusersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th class='text-center'>#</th>
                                                <th class='text-center'>Name</th>
                                                <th class='text-center'>Email</th>
                                                <th class='text-center'>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.members.map((member, index) => (
                                                    <tr>
                                                        <td>{index + 1}</td>
                                                        <td class='text-center'>
                                                            <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                <input type="checkbox" disabled={true} class="custom-control-input" name="member_id" value={member.member_id} onChange={this.handleMemberProgramMapping} defaultChecked={member.member_program_status == 1 ? true : ''} />               <span class="custom-control-label">&nbsp;</span>
                                                            </label>
                                                        </td>
                                                        <td class='text-center'><NavLink to={`/user/detail/${member.member_id}`} className="btn btn-icon btn-sm text-info brandColor" title="Member">{member.first_name} {member.last_name}</NavLink></td>
                                                        <td class='text-center'>{member.email}</td>
                                                        <td class='text-center'>
                                                            {member.status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.members.length == 0 && (<tfoot><tr><td colspan="5" className="text-center text-capitalize">{this.state.showUsersTableMessage}</td></tr></tfoot>)}
                                    </table>
                                </div>
                            </div>
                        </div> */}


                    </div>
                </div>
            </Fragment>

        )
    }
}

export default ProgramAdd
