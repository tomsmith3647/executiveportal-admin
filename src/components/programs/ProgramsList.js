import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class ProgramsList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        programs: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_program_id: '',
        access_permission:false,
        
        access_permission_r:false,
        access_permission_w:false,
        access_permission_u:false,
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/program/list",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ access_permission: response.data.access_permission });
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    this.setState({ access_permission_r: response.data.access_permission_r });
                    this.setState({ access_permission_w: response.data.access_permission_w });
                    this.setState({ access_permission_u: response.data.access_permission_u });
                    if (error_code == '309') {
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                    else if (error_code == '200') {
                        //success                        
                        this.setState({ programs: response.data.programs });


                        var programTable = $('#usersTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [7], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        programTable.columns().iterator('column', function (ctx, idx) {
                            $(programTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        programTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = programTable.row(diff[i].node).data();
                                const program_id = $(rowData[1]).attr("data-id");
                                const catsort = { "program_id": program_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/program/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                                });
                        });
                    }
                    else {
                        //fail
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    convertUTCDateToLocalDate =(date)=> {        
        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL == 'refresh') 
        {
            window.location.reload();  
        } 
        else if(this.state.showAlertActionURL) 
        {
            window.location.href = this.state.showAlertActionURL;  
        }
    }


    //programDelete    
    programDelete = () => (e) => {        
        const program_id = e.currentTarget.dataset.id ;
        this.setState({ selected_program_id: program_id });
        this.setState({ showConfirmationAlert: true });
    }

    //programDeleteProcess
    programDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const program_id = this.state.selected_program_id;

            axios({
                url: API_URL + "admin/program/delete",
                method: 'post',
                data: {
                    program_id: program_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            program/category/module/details         
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/programs' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
        //Get user permissions
    }

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.programDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Programs</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">Program List</li>
                        </ol>
                    </div>
                </div>
                <br />
                {this.state.access_permission && 
                    (
                <div className="tab-pane" id="Holiday-all">
                    {this.state.access_permission_w && 
                    (<div className="card-footer text-right">
                        <NavLink to="/program/add" className="btn btn-brand">Add Program</NavLink>
                    </div>)}

                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th >#</th>
                                            <th className='text-left' >Program Code</th>
                                            <th className='text-left' >Program Title</th>
                                            <th className='text-left'>Status</th>
                                            <th className='text-center' >Members Count</th>
                                            <th className='text-left' >Created Date</th>
                                            <th className='text-left' >Last Activity</th>
                                            {this.state.access_permission_u && 
                                            (<th className='text-center' >Action</th>)}

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.programs.map((program, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>
                                                    <td className="text-left text-capitalize">
                                                        <NavLink data-id={program.program_id} to={`/program/details/${program.program_id}`} className="text-info brandColor" title="View Detail">
                                                            {program.program_code}
                                                        </NavLink>
                                                    </td>
                                                    <td className="text-left text-capitalize">
                                                        {program.program_title}
                                                    </td>
                                                    <td className='text-left'>
                                                        {program.program_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                    </td>
                                                    <td className="text-center">
                                                        {program.member_counter}
                                                    </td>

                                                    
                                                
                                                    <td className='text-left'>
                                                        <span className='dateFormat'>{program.created_date}</span>
                                                        {(new Date(this.convertUTCDateToLocalDate(new Date(program.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                    </td> 

                                                    
                                                    
                                                    <td className='text-left'>
                                                        <span className='dateFormat'>{program.modified_date}</span>
                                                        {(new Date(this.convertUTCDateToLocalDate(new Date(program.modified_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                    </td> 


                                                     
                                                    {this.state.access_permission_u && 
                                                    (<td className='text-center'>
                                                        {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                        <NavLink to={`/program/details/${program.program_id}`} className="text-info brandColor p-1" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>
                                                        
                                                        <a href='javascript:void(0);' onClick={this.programDelete()} data-id={program.program_id} className="brandColor p-1" title="Edit"><i className="fa fa-trash text-danger"></i></a>
                                                        {/* <NavLink to='' className="btn btn-icon btn-sm " title="Edit"><i className="fa fa-trash text-danger"></i></NavLink> */}
                                                    </td>)}
                                                </tr>
                                            ))
                                        }

                                    </tbody>
                                    {this.state.programs.length == 0 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'No Program'}</td></tr></tfoot>)}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>)}
            </Fragment>
        )
    }
}

export default ProgramsList
