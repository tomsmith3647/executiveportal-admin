import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class TaskAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        topic: {
            topic_description: '',
            topic_title: '',
            topic_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        access_permission:false,
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const module_id = this.props.match.params.module_id;
            this.setState({ module_id: module_id });
            this.setState({ topic: { module_id: module_id } });

            axios({
                url: API_URL + "admin/program/category/module/detail/" + module_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ topic: { ...this.state.topic, program_id: response.data.module.program_id, category_id: response.data.module.category_id } });
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    // handleChangetask = (e) => {
    //     const taskData = this.state.task;
    //     taskData[e.target.name] = e.target.value;
    //     this.setState({ task: taskData });
    // }
    handleChangetask = (e) => {
        this.setState({ topic: { ...this.state.topic, topic_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            topic: {
                ...this.state.topic,
                topic_description: e
            }
        })
    }


    handleThumnailImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose Thumnail image." });
            }
            else {
                const file = event.target.files[0];
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }                 
                else {
                    this.setState({
                        selected_thumnail: file,
                    })
                }
            }
        }
    }


    handleUloadVideo = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ fileName: 'Choose file' });
                this.setState({ showAlert: true, showAlertMessage: "Please choose video." });
            }
            else {
                const file = event.target.files[0];
                this.setState({ fileName: event.target.files[0].name });
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "mp4") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }                
                else {



                    this.setState({
                        selected_video: file,
                    })

                    const formData = new FormData()

                    formData.append('module_video', file)
                    var A = this;

                    this.setState({fileUploading:true});
                    axios({
                        url: API_URL + "admin/program/category/module/topic/add/imagevideo/upload",
                        method: 'post',
                        data: formData,
                        headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                        onUploadProgress: function (progressEvent) {
                            var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                            A.setState({ processBarPercent: percentCompleted })
                            console.log(percentCompleted)
                        }
                    })
                        .then(response => {

                            const error_code = response.data.error_code;
                            const error_message = response.data.error_message;
                            this.setState({fileUploading:false});
                            if (error_code == '200') {
                                //success   
                                if (response.data.video_path) {
                                    this.setState({ topic: { ...this.state.topic, topic_video: response.data.video_path } });
                                }
                                this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message, video_path: response.data.video_path });
                            }
                            else {
                                //fail
                                this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                            }
                        })
                        .catch(err => {
                            //error
                            console.log(err);
                        });

                }
            }
        }
    }

    //add task
    TaskAddBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.topic.topic_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter topic title." });
                return false;

            }




            const imagefile = this.state.selected_thumnail;
            // if(!imagefile)
            // {
            //     this.setState({ showAlertIcon : 'warning', showAlert: true, showAlertMessage: "Please choose thumnail image." });                                
            //     return false;
            // }

            const videofile = this.state.selected_video;
            // if(!videofile)
            // {
            //     this.setState({ showAlertIcon : 'warning', showAlert: true, showAlertMessage: "Please choose video file." });                                
            //     return false;
            // }

            if (imagefile) {
                const image_extension = imagefile.type.split('/').pop().toLowerCase();
                if (image_extension != "jpeg" && image_extension != "jpg" && image_extension != "png" && image_extension != "bmp" && image_extension != "gif") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }                
            }


            if (videofile) {
                const video_extension = videofile.type.split('/').pop().toLowerCase();
                if (video_extension != "mp4") {
                    event.target.value = '';
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }                 
            }

            // if(!this.state.topic.topic_description)
            // {
            //     this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: "Please enter topic description." }); 
            //     return false;
            // }
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/module/topic/add",
                method: 'post',
                data: this.state.topic,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success          
                        if (this.state.selected_thumnail || this.state.selected_video) {
                            this.setState({ showLoader: true });
                            const formData = new FormData()
                            formData.append('module_image', this.state.selected_thumnail)
                            formData.append('topic_id', response.data.topic.topic_id)

                            axios({
                                url: API_URL + "admin/program/category/module/topic/imagevideo/upload",
                                method: 'post',
                                data: formData,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                            })
                                .then(response => {
                                    this.setState({ showLoader: false });
                                    const error_code = response.data.error_code;
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                        }

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Topic</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.topic.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.topic.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/module/details/${this.state.module_id}`}>Module</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Add Topic</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-5 col-md-12">
                        <form className='form-horizontal' onSubmit={this.TaskAddBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">


                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.topic.topic_title} name='topic_title' onChange={this.handleChangetask} />
                                        </div>
                                    </div>

                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}


                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Thumbnail  </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="thumnail" onChange={this.handleThumnailImage} data-height="200" className="dropify" data-default-file={this.state.data_default_file} />
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Video   </h3>
                                </div>
                                <div className="card-body">

                                    <div class="input-group">

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="video"
                                                aria-describedby="inputGroupFileAddon01" onChange={this.handleUloadVideo} />
                                            <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                        </div>

                                    </div>

                                    {this.state.processBarPercent > 0 ?
                                        (
                                            <div className="my-3">
                                                <div className="text-center">{this.state.processBarPercent}%</div>
                                                <Progress value={this.state.processBarPercent} />
                                            </div>
                                        ) : ''}

                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value=""

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>


                            <div className="card-footer text-right">

                                
                                {this.state.fileUploading ? (<button type="button" disabled   className="btn btn-brand">Submit</button>) : (<button type="submit" className="btn btn-brand">Submit</button>)}
                                
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default TaskAdd
