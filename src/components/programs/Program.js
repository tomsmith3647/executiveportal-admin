import React, { Component } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
const $ = window.$
class Program extends Component {

    componentDidMount() {
        $('#activeCategory').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });
    }

    state = {
        name: '',
        notes: ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    render() {
        return (
            <div className="row">
                <div className="col-xl-5 col-md-12">

                    <div className="card">
                        <div className="card-header pb-0">
                            <h3 className="card-title">Name</h3>
                        </div>
                        <div className="card-body">
                            <form className='form-horizontal' onSubmit={this.handleSubmit}>

                                <div className="form-group row">
                                    <div className="col-md-12">
                                        <input type="text" className="form-control" value={this.state.name} name='name' onChange={this.handleChange} />
                                    </div>
                                </div>

                                {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}

                            </form>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header pb-0">
                            <h3 className="card-title">Timeline Activity</h3>
                        </div>
                        <div className="card-body">
                            <ReactSummernote
                                value="Default value"
                                popover={{
                                    image: [],
                                    link: [],
                                    air: []
                                }}
                                options={{
                                    height: 150,
                                    toolbar: [
                                        ['style', ['style']],
                                        ['font', ['bold', 'underline', 'clear']],
                                        ['fontname', ['fontname']],
                                        ['para', ['ul', 'ol', 'paragraph']],
                                        ['table', ['table']],
                                        ['insert', ['link', 'picture', 'video']],
                                        ['view', ['fullscreen', 'codeview']]
                                    ]
                                }}
                            // onChange={this.handleChange}
                            />

                        </div>
                    </div>
                </div>
                <div className="col-xl-7 col-md-12">
                    <div className="card">
                        <div className="row ">
                            <div className="col">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Categories</h3>
                                </div>
                            </div>
                            <div className="col ">
                                <div className="card-header pb-0 justify-content-end">
                                    <button className='btn btn-brand'>Add Category</button>
                                </div>

                            </div>
                        </div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th>Active</th>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>test</td>
                                            <td>Test</td>
                                            <td>Test</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='activeCategory' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th>Active</th>
                                            <th>User Name</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>test</td>
                                            <td>Test</td>
                                            <td>Test</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Program
