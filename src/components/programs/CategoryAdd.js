import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
class CategoryAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        category: {
            category_description: '',
            category_title: ''
        },
        categories: [],
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_profile_image: '',
        access_permission:false,
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activeCategory').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const program_id = this.props.match.params.program_id;
            this.setState({ program_id: program_id });
            this.setState({ category: { program_id: program_id } });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    // handleChangecategory = (e) => {
    //     const categoryData = this.state.category;
    //     categoryData[e.target.name] = e.target.value;
    //     this.setState({ category: categoryData });
    // }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    handleChangecategory = (e) => {
        this.setState({ category: { ...this.state.category, category_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            category: {
                ...this.state.category,
                category_description: e
            }
        })
    }

    handleImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
            }
            else {
                this.setState({
                    selected_profile_image: event.target.files[0],
                })
            }
        }
    }



    //add category
    CategoryAddBtn = () => {
        console.log(this.state.member);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,

                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/add",
                method: 'post',
                data: this.state.category,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success    
                        if (this.state.selected_profile_image) {
                            const imagedata = new FormData()
                            imagedata.append('image', this.state.selected_profile_image)
                            imagedata.append('category_id', response.data.category.category_id)
                            axios({
                                url: API_URL + "admin/program/category/image/upload",
                                method: 'post',
                                data: imagedata,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/details/' + this.state.program_id });
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/details/' + this.state.program_id });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }






    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Category</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.program_id}`}>Program </NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Add Category</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />

                <div className="user-profile">
                    <div className="row">

                        <div className="col-xl-5 col-md-12">
                            <form className='form-horizontal' onSubmit={this.handleSubmit}>
                                <div className="card">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Name</h3>
                                    </div>
                                    <div className="card-body">
                                        <div className="form-group row">
                                            <div className="col-md-12">
                                                <input type="text" className="form-control" value={this.state.category.category_title} name='category_title' onChange={this.handleChangecategory} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Description</h3>
                                    </div>
                                    <div className="card-body">
                                        <div className="form-group row">

                                            <div className="col-md-12">
                                                <ReactSummernote
                                                    value=""

                                                    popover='false'
                                                    options={{
                                                        height: 150,
                                                        toolbar: [
                                                            ['style', ['style']],
                                                            ['font', ['bold', 'underline', 'clear']],
                                                            ['fontname', ['fontname']],
                                                            ['para', ['ul', 'ol', 'paragraph']],
                                                            ['table', ['table']],
                                                            ['insert', ['link', 'picture', 'video']],
                                                            ['view', ['fullscreen', 'codeview']]
                                                        ]
                                                    }}
                                                    onChange={this.handleSummerNote}

                                                />
                                            </div>
                                        </div>

                                        {/* <div className="card-footer text-right">
    <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
</div> */}


                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Image/Icon   </h3>
                                    </div>
                                    <div className="card-body">
                                        <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file={this.state.category.category_icon ? this.state.baseApiURL + this.state.category.category_icon : '/assets/images/gallery/1.jpg'} />
                                    </div>
                                </div>

                                <div className="card-footer text-right">
                                    <button type="submit" onClick={this.CategoryAddBtn} className="btn btn-brand">Submit</button>
                                </div>

                            </form>


                        </div>
                        <div className="col-xl-7 col-md-12">
                            {/* Modules */}
                            <div className="card">
                                <div className="row ">
                                    <div className="col">
                                        <div className="card-header pb-0">
                                            <h3 className="card-title">Modules</h3>
                                        </div>
                                    </div>
                                    <div className="col ">
                                        <div className="card-header pb-0 justify-content-end">

                                            <button className='btn btn-brand' disabled={true} >Add Module</button>

                                        </div>

                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table id='moduleListTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                            <thead>
                                                <tr>
                                                    <th>Sno.</th>
                                                    <th>Module</th>
                                                    <th class='text-center'>Status</th>
                                                    <th class='text-center'>Action</th>
                                                </tr>
                                            </thead>


                                            <tbody></tbody>
                                            <tfoot><tr><td colspan="4" className="text-center text-capitalize">Modules Not Found.</td></tr></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            {/* Action Items */}
                            <div className="card">
                                <div className="row ">
                                    <div className="col">
                                        <div className="card-header pb-0">
                                            <h3 className="card-title">Action Items</h3>
                                        </div>
                                    </div>
                                    <div className="col ">
                                        <div className="card-header pb-0 justify-content-end">

                                            <button className='btn btn-brand' disabled={true}>Add Item</button>

                                        </div>

                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table id='moduleItemTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                            <thead>
                                                <tr>
                                                    <th>Sno.</th>
                                                    <th>Item</th>
                                                    <th class='text-center'>Status</th>
                                                    <th class='text-center'>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot><tr><td colspan="4" className="text-center text-capitalize">Items Not Found.</td></tr></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>


            </Fragment>

        )
    }
}

export default CategoryAdd
