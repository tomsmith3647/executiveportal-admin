import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';
const $ = window.$
class ResourceUpdate extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        resource_id:'',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        resource: {},
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_file: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        file_size: 1000, //20MB
        image_size: 2, //2MB
        fileUpload: false,
        link: true,
        fileName:'Choose file',
        processBarPercent:0,
        resource_url:'',
        fileUploading:false,
        access_permission:false,
        
    }

    componentDidMount() {

        $('.dropify').dropify();

        


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const resource_id = this.props.match.params.resource_id;
            this.setState({ resource_id: resource_id });
            this.setState({ resource: { resource_id: resource_id, resource_type:'link' } });
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/module/resource/detail/" + resource_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                this.setState({ access_permission: response.data.access_permission });
                if (error_code == '200') {
                    //success   
                    
                    if(response.data.resource.resource_type == 'link')
                    {                                                       
                        this.setState({fileUpload: false, link: true,});
                    }
                    else if(response.data.resource.resource_type == 'file')
                    {
                        this.setState({fileUpload: true, link: false,});
                    }
                    
                    this.setState({resource: response.data.resource});
                    this.setState({...this.state.resource, resource_url:ApiURL+response.data.resource.resource_url});
                }
                else {
                    //fail
                    this.setState({ showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }


    }

  
    handleChangeResourceTitle = (e) => {
        this.setState({ resource: { ...this.state.resource, resource_title: e.target.value } });
    }
    handleChangeResourceLink = (e) => {
        this.setState({ resource: { ...this.state.resource, resource_url: e.target.value } });
    }


   


    handleUloadFile = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ fileName: 'Choose file' });
                this.setState({ selected_file: ''})
                //this.setState({ showAlert: true, showAlertMessage: "Please choose." });
            }
            else {
                const file = event.target.files[0];

                this.setState({ fileName: event.target.files[0].name });
                    
                const fileNameExt = file.type.split('/').pop().toLowerCase();


                var validExtensions = ['pdf', 'doc', 'xls', 'txt','jpg', 'jpeg', 'gif','png']; //array of valid extensions


                
                // if ($.inArray(fileNameExt, validExtensions) == -1)
                // {
                //     event.target.value = '';
                //     event.target.value = null;
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid file." });
                //     return false;
                // }
                // else if (file.size > (1024000 * this.state.file_size)) {
                //     event.target.value = '';
                //     event.target.value = null;
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.file_size + "MB only" });
                //     return false;
                // }
                // else {
                //     this.setState({
                //         selected_file: file,
                //     })
                // }

                this.setState({
                    selected_file: file,
                })

                const formData = new FormData()
                   
                    formData.append('resource_file',file)
                    var A =this;
                    this.setState({fileUploading:true});
                    axios({
                        url: API_URL + "admin/program/category/module/resource/add/imagevideo/upload",
                        method: 'post',
                        data: formData,
                        headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                        onUploadProgress: function(progressEvent) {
                            var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                            A.setState({processBarPercent:percentCompleted})                            
                        }
                    })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        const api_url = response.data.api_url;
                        this.setState({fileUploading:false});
                        if (error_code == '200') {
                            //success  
                            if(response.data.resource_path)
                            {
                                this.setState({  resource: {...this.state.resource, resource_url:api_url+response.data.resource_path} });                                
                            }                                                               
                            this.setState({showAlertIcon : 'success', showAlert: false, showAlertMessage: error_message, resource_url:api_url+response.data.resource_path});       
                            
                        }
                        else {
                            //fail
                            this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
        }
    }


    validateUrl = (str) => {
       const regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (regexp.test(str))
        {
          return true;
        }
        else
        {
          return false;
        }
}

    

    //add task
    ResourceUpdateBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.resource.resource_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter resource title." });
                return false;
            }

            var resource_url = '';
            var resource_url = '';
            
            const fileUpload = this.state.selected_file;
            if(this.state.resource.resource_type=='file' && this.state.selected_file !='')
            {
                if (!fileUpload) {
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please choose file." });
                    return false;
                }        

                // var validExtensions = ['pdf', 'doc', 'xls', 'txt','jpg', 'jpeg', 'gif','png']; //array of
                // const fileNameExt = fileUpload.type.split('/').pop().toLowerCase();
                // if ($.inArray(fileNameExt, validExtensions) == -1)
                // {
                //     event.target.value = '';
                //     event.target.value = null;    
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid file." });
                //     return false;
                // }
                // else if (fileUpload.size > (1024000 * this.state.file_size)) {
                //     event.target.value = '';
                //     event.target.value = null;
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.file_size + "MB only" });
                //     return false;
                // }

                //resource_url = this.state.selected_file;
                resource_url = this.state.resource_url;
            }
            else if(this.state.resource.resource_type=='link')
            {
                if(!this.state.resource.resource_url)
                {                    
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter link / URL" });
                    return false;  
                }

                if (!this.validateUrl(this.state.resource.resource_url)) {
                    
                    event.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
                    return false;
                                
                }
                else
                {
                    resource_url = this.state.resource.resource_url;             
                }

                
                           
            }
            

            

            this.setState({ showLoader: true });
             
            //success          
             
            const formData = new FormData();                
            formData.append('resource_title', this.state.resource.resource_title);
            formData.append('resource_url', resource_url);                 
            formData.append('resource_type', this.state.resource.resource_type);
            formData.append('program_id', this.state.resource.program_id);
            formData.append('category_id', this.state.resource.category_id);
            formData.append('module_id', this.state.resource.module_id);
            formData.append('resource_id', this.state.resource.resource_id);
            
            
            axios({
                url: API_URL + "admin/program/category/module/resource/update",
                method: 'post',
                data: formData,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.resource.module_id });
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });                                 
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    chooseFile = (e) => {
        console.log(e.target.value)
         

        if (e.target.value === 'file') {
            this.setState({
                fileUpload: true,
                link: false,
                resource:{resource_type:'file'}
            })
             
        } else {
            this.setState({
                fileUpload: false,
                link: true,
                resource:{resource_type:'link'}
            })
        }

        this.setState({

            resource: {
                ...this.state.resource,
                resource_type: e.target.value
            }
        })
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Resource Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.resource.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.resource.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/module/details/${this.state.resource.module_id}`}>Module</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Update Resource</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">  
                    <div className="col-md-12">
                        <form className='form-horizontal' onSubmit={this.ResourceUpdateBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">


                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.resource.resource_title} name='resource_title' onChange={this.handleChangeResourceTitle} />
                                        </div>
                                    </div>

                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}


                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Choose upload Type </h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-7 mb-3">
                                            <div className="custom-controls-stacked">  
                                                <label className="custom-control custom-radio custom-control-inline"> 
                                                    <input type="radio" className="custom-control-input" name="resource_type" defaultValue="link" checked={this.state.link}  onChange={this.chooseFile} />
                                                    <span className="custom-control-label">Link</span>
                                                </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="resource_type" defaultValue="file"  checked={this.state.fileUpload} onChange={this.chooseFile} />
                                                    <span className="custom-control-label">File Upload</span>
                                                </label>
                                            </div>
                                        </div>
                                         
                                        {this.state.link && <div className="col-md-12">
                                            <input type="text" className="form-control" name="resource_url" value={this.state.resource.resource_url} onChange={this.handleChangeResourceLink} />
                                        </div>}


                                        {this.state.fileUpload && <div className="col-md-12">

                                        <div class="input-group">
                                         
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01"
                                            aria-describedby="inputGroupFileAddon01" onChange={this.handleUloadFile} />
                                            <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                        </div>                                                                                                                        
                                        </div>
                                        {this.state.processBarPercent > 0 ? 
                                        (
                                            <div  className="my-3">
                                            <div className="text-center">{this.state.processBarPercent}%</div>
                                            <Progress value={this.state.processBarPercent} />
                                            </div>
                                        ) : '' }
                                        <br/>
                                         
                                        {this.state.fileUpload && this.state.resource.resource_url && (<a href={this.state.resource.resource_url} target='_blank'>Click here to view uploaded file</a>)}
                                        


                                            {/* <div class="input-group input-file" name="chooseLink">
                                                <input type="text" class="form-control" placeholder='Choose a file...' name='fileUpload'  />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-choose" type="button">Choose</button>
                                                </span>

                                            </div> */}
                                        </div>}

                                    </div>
                                </div>
                            </div>
                             


                            <div className="card-footer text-right">

                                
                                {this.state.fileUploading ? (<button type="button" disabled   className="btn btn-brand">Update</button>) : (<button type="submit" className="btn btn-brand">Update</button>)}
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default ResourceUpdate
