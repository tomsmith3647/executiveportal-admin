import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';
 

 

const $ = window.$
class ResourceAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        resource: {
            resource_description: '',
            resource_title: '',
            resource_type:'link'
        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_file: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        file_size: 1000, //20MB
        image_size: 2, //2MB
        fileUpload: false,
        link: true,
        fileName:'Choose file',
        processBarPercent:0,
        resource_url:'',
        fileUploading:false,
        access_permission:false,
        
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const module_id = this.props.match.params.module_id;
            this.setState({ module_id: module_id });
            this.setState({ resource: { module_id: module_id, resource_type:'link' } });

            axios({
                url: API_URL + "admin/program/category/module/detail/" + module_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ resource: { ...this.state.resource, program_id: response.data.module.program_id, category_id: response.data.module.category_id } });
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }


    }


    bs_input_file = () => {
        $(".input-file").before(
            function () {
                if (!$(this).prev().hasClass('input-ghost')) {
                    var element = $("<input id='fileInput' onChange='handleUloadFile(e)' type='file' class='input-ghost' style='visibility:hidden; height:0; display: none'>");
                    element.attr("name", $(this).attr("name"));
                    element.change(function () {
                        element.next(element).find('input').val((element.val()).split('\\').pop());
                         
                    });
                    $(this).find("button.btn-choose").click(function () {
                        element.click();
                    });
                    $(this).find("button.btn-reset").click(function () {
                        element.val(null);
                        $(this).parents(".input-file").find('input').val('');
                    });
                    $(this).find('input').css("cursor", "pointer");
                    $(this).find('input').mousedown(function () {
                        $(this).parents('.input-file').prev().click();
                        return false;
                    });
                    return element;
                }
            }
        );
    }



    // handleChangeResource = (e) => {
    //     const taskData = this.state.task;
    //     taskData[e.target.name] = e.target.value;
    //     this.setState({ task: taskData });
    // }
    handleChangeResourceTitle = (e) => {
        this.setState({ resource: { ...this.state.resource, resource_title: e.target.value } });
    }
    handleChangeResourceLink = (e) => {
        this.setState({ resource: { ...this.state.resource, resource_url: e.target.value } });
    }


   


    handleUloadFile = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ fileName: 'Choose file' });
                this.setState({ selected_file: ''})
                //this.setState({ showAlert: true, showAlertMessage: "Please choose." });
            }
            else {
                const file = event.target.files[0];

                this.setState({ fileName: event.target.files[0].name });
                    
                // const fileNameExt = file.type.split('/').pop().toLowerCase();


                // var validExtensions = ['pdf', 'doc', 'xls', 'txt','jpg', 'jpeg', 'gif','png']; //array of valid extensions


                // //if (t != "pdf" || t != "doc" || t != "xls" || t != "txt" || t != "jpg" || t != "gif" || t != "png" || t != "jpeg") {
                // if ($.inArray(fileNameExt, validExtensions) == -1)
                // {
                //     event.target.value = '';
                //     event.target.value = null;
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid file." });
                //     return false;
                // }
                // else if (file.size > (1024000 * this.state.file_size)) {
                //     event.target.value = '';
                //     event.target.value = null;
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.file_size + "MB only" });
                //     return false;
                // }
                // else {
                //     this.setState({
                //         selected_file: file,
                //     })
                // }

                this.setState({
                    selected_file: file,
                })
                this.setState({fileUploading:true});
                const formData = new FormData()
                   
                    formData.append('resource_file',file)
                    var A =this;
                     
                    axios({
                        url: API_URL + "admin/program/category/module/resource/add/imagevideo/upload",
                        method: 'post',
                        data: formData,
                        headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                        onUploadProgress: function(progressEvent) {
                            var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                            A.setState({processBarPercent:percentCompleted})                            
                        }
                    })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        const api_url = response.data.api_url;
                        this.setState({fileUploading:false});
                        if (error_code == '200') {
                            //success                 
                            if(response.data.resource_path)
                            {
                                this.setState({  resource: {...this.state.resource, resource_url:response.data.resource_path} });                                
                            }                     
                            this.setState({showAlertIcon : 'success', showAlert: false, showAlertMessage: error_message, resource_url:api_url+response.data.resource_path});       
                            
                        }
                        else {
                            //fail
                            this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
        }
    }


    
    
    validateUrl = (str) => {
        const regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (regexp.test(str))
        {
          return true;
        }
        else
        {
          return false;
        }
}

    //add task
    ResourceAddBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.resource.resource_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter resource title." });
                return false;
            }

            var resource_url = '';
            
            const fileUpload = this.state.selected_file;
            if(this.state.resource.resource_type=='file')
            {
                if (!fileUpload) {
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please choose file." });
                    return false;
                }        

                // var validExtensions = ['pdf', 'doc', 'xls', 'txt','jpg', 'jpeg', 'gif','png']; //array of
                // const fileNameExt = fileUpload.type.split('/').pop().toLowerCase();
                // if ($.inArray(fileNameExt, validExtensions) == -1)
                // {
                //     event.target.value = '';
                //     event.target.value = null;    
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid file." });
                //     return false;
                // }
                // else if (fileUpload.size > (1024000 * this.state.file_size)) {
                //     event.target.value = '';
                //     event.target.value = null;
                //     this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.file_size + "MB only" });
                //     return false;
                // }

                resource_url = this.state.selected_file;
                resource_url = this.state.resource_url;
            }
            else
            {
                if(this.state.resource.resource_url)
                {
                     
                    
                    if (!this.validateUrl(this.state.resource.resource_url)) {
                    
                        event.preventDefault()
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
                        return false;                                    
                    }
                    else
                    {
                        resource_url = this.state.resource.resource_url;             
                    }
                }
                else
                {
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter URL." });
                    return false;
                }
                
                                
            }
            

            

            

            this.setState({ showLoader: true });
             
            //success          
             
            const formData = new FormData();                
            formData.append('resource_title', this.state.resource.resource_title);
            formData.append('resource_url', resource_url);                 
            formData.append('resource_type', this.state.resource.resource_type);
            formData.append('program_id', this.state.resource.program_id);
            formData.append('category_id', this.state.resource.category_id);
            formData.append('module_id', this.state.resource.module_id);
             
            
            axios({
                url: API_URL + "admin/program/category/module/resource/add",
                method: 'post',
                data: formData,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                 
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });                                 
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    chooseFile = (e) => {
        console.log(e.target.value)
        

        if (e.target.value === 'file') {
            this.setState({
                fileUpload: true,
                link: false
            })
            setTimeout(() => {
                this.bs_input_file()
            }, 500);
        } else {
            this.setState({
                fileUpload: false,
                link: true
            })
        }

        this.setState({

            resource: {
                ...this.state.resource,
                resource_type: e.target.value
            }
        })
    }

  
 


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Resource</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.resource.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.resource.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/module/details/${this.state.module_id}`}>Module</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Add Resource</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <form className='form-horizontal' onSubmit={this.ResourceAddBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">


                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" name='resource_title' onChange={this.handleChangeResourceTitle} />
                                        </div>
                                    </div>

                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}


                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Choose upload Type </h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-7 mb-3">
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="resource_type" defaultValue="link" defaultChecked onChange={this.chooseFile} />
                                                    <span className="custom-control-label">Link</span>
                                                </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="resource_type" defaultValue="file" onChange={this.chooseFile} />
                                                    <span className="custom-control-label">File Upload</span>
                                                </label>
                                            </div>
                                        </div>

                                        {this.state.link && <div className="col-md-12">
                                            <input type="text" className="form-control" name="resource_url" onChange={this.handleChangeResourceLink} />
                                        </div>}


                                        {this.state.fileUpload && <div className="col-md-12">

                                        <div class="input-group">
                                         
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01"
                                            aria-describedby="inputGroupFileAddon01" onChange={this.handleUloadFile} />
                                            <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                        </div>
                                         
                                        </div>

                                        {this.state.processBarPercent > 0 ? 
                                        (
                                            <div  className="my-3">
                                            <div className="text-center">{this.state.processBarPercent}%</div>
                                            <Progress value={this.state.processBarPercent} />
                                            </div>
                                        ) : '' }

                                            {/* <div class="input-group input-file" name="chooseLink">
                                                <input type="text" class="form-control" placeholder='Choose a file...' name='fileUpload'  />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-choose" type="button">Choose</button>
                                                </span>

                                            </div> */}
                                        </div>}

                                    </div>
                                </div>
                            </div>
                            {/* <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Thumbnail  </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="thumnail" onChange={this.handleThumnailImage} data-height="200" className="dropify" data-default-file={this.state.data_default_file} />
                                </div>
                            </div> */}

                            {/* <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Video   </h3>
                                </div>
                                <div className="card-body">
                                    <div class="input-group">

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01"
                                                aria-describedby="inputGroupFileAddon01" />
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div> */}




                            {/* <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Video   </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="video" onChange={this.handleUloadFile} data-height="200" className="dropify" data-default-file={this.state.data_default_file} />
                                </div>
                            </div> */}
                            {/* <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value=""

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div> */}


                            <div className="card-footer text-right">

                                
                                {this.state.fileUploading ? (<button type="button" disabled   className="btn btn-brand">Submit</button>) : (<button type="submit" className="btn btn-brand">Submit</button>)}
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default ResourceAdd
