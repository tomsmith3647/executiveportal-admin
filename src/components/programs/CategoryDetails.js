import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

const $ = window.$
class CategoryDetails extends Component {

    state = {
        items: [],
        name: '',
        program_id: '',
        notes: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        program: {},
        modules: [],
        category: {},
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showConfirmationAlertItem: false,
        showModuleTableMessage: '',
        showItemTableMessage: '',
        selected_file: '',
        access_permission:false,
    }

    componentDidMount() {




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const category_id = this.props.match.params.category_id;
            this.setState({ category_id: category_id });
            const program_id = this.props.match.params.program_id;
            this.setState({ program_id: program_id });

            this.setState({ showLoader: true, showModuleTableMessage: "Please wait...", showItemTableMessage: 'Please wait...' });

            //get category details
            axios({
                url: API_URL + "admin/program/category/detail/" + category_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ access_permission: response.data.access_permission });
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ category: response.data.category });
                        $('.dropify').dropify();
                    }
                    else {
                        //fail
                        this.setState({ howAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            //get Category
            axios({
                url: API_URL + "admin/program/category/module/list/" + category_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });

                    if (error_code == '200') {
                        //success                        
                        this.setState({ modules: response.data.modules });

                        var moduleTable = $('#moduleListTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [3], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        moduleTable.columns().iterator('column', function (ctx, idx) {
                            $(moduleTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        moduleTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = moduleTable.row(diff[i].node).data();
                                const module_id = $(rowData[1]).attr("data-id");
                                const catsort = { "module_id": module_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/program/category/module/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });

                    }
                    else {
                        //fail
                        this.setState({ howAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showModuleTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });



            //get Items
            axios({
                url: API_URL + "admin/program/category/item/list/" + category_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });

                    if (error_code == '200') {
                        //success                        
                        this.setState({ items: response.data.items });
                        var itemTable = $('#moduleItemTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [3], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        itemTable.columns().iterator('column', function (ctx, idx) {
                            $(itemTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        itemTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = itemTable.row(diff[i].node).data();
                                const item_id = $(rowData[1]).attr("data-id");
                                const catsort = { "item_id": item_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/program/category/item/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showItemTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }

    handleChangecategory = (e) => {
        this.setState({ category: { ...this.state.category, category_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            category: {
                ...this.state.category,
                category_description: e
            }
        })
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeStatus = (e) => {
        this.setState({ category: { ...this.state.category, category_status: e.target.value } });
    }

    handleImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {

                this.setState({ selected_file: '' })
                //this.setState({ showAlert: true, showAlertMessage: "Please choose." });
            }
            else {
                const file = event.target.files[0];



                const fileNameExt = file.type.split('/').pop().toLowerCase();


                var validExtensions = ['jpg', 'jpeg', 'gif', 'png']; //array of valid extensions


                //if (t != "pdf" || t != "doc" || t != "xls" || t != "txt" || t != "jpg" || t != "gif" || t != "png" || t != "jpeg") {
                if ($.inArray(fileNameExt, validExtensions) == -1) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image." });
                    return false;
                }
                else if (file.size > (1024000 * this.state.file_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.file_size + "MB only" });
                    return false;
                }
                else {
                    this.setState({
                        selected_file: file,
                    })
                }
            }
        }
    }

    //CategoryUpdateBtn
    CategoryUpdateBtn = (event) => {
        event.preventDefault()
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (this.state.selected_file) {
                const file = this.state.selected_file;
                const fileNameExt = file.type.split('/').pop().toLowerCase();
                var validExtensions = ['jpg', 'jpeg', 'gif', 'png']; //array of valid extensions 
                if ($.inArray(fileNameExt, validExtensions) == -1) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image." });
                    return false;
                }
                 
            }

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/update",
                method: 'post',
                data: this.state.category,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success     

                        if (this.state.selected_file && this.state.category_id) {
                            this.setState({ showLoader: true });
                            const imagedata = new FormData()
                            imagedata.append('image', this.state.selected_file)
                            imagedata.append('category_id', this.state.category_id)
                            axios({
                                url: API_URL + "admin/program/category/image/upload",
                                method: 'post',
                                data: imagedata,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/details/' + this.state.category.program_id });
                                        //window.location.href = '/dashboard'
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/details/' + this.state.category.program_id });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
    }

    //moduleDelete
    moduleDelete = () => (e) => {
        const module_id = e.currentTarget.value;
        this.setState({ selected_module_id: module_id });
        this.setState({ showConfirmationAlert: true });
    }


    //moduleDeleteProcess
    moduleDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false, showLoader: true });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            //const admin_id = e.currentTarget.value;
            const module_id = this.state.selected_module_id;

            axios({
                url: API_URL + "admin/program/category/module/delete",
                method: 'post',
                data: {
                    module_id: module_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showConfirmationAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            program/category/module/details                                
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.category_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions
    }



    //Items delete
    //moduleDelete
    moduleItemDelete = () => (e) => {
        const item_id = e.currentTarget.value;
        this.setState({ selected_item_id: item_id });
        this.setState({ showConfirmationAlertItem: true });
    }


    //moduleDeleteProcess
    moduleItemDeleteProcess = () => {
        this.setState({ showConfirmationAlertItem: false, showLoader: true });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const item_id = this.state.selected_item_id;

            axios({
                url: API_URL + "admin/program/category/item/delete",
                method: 'post',
                data: {
                    item_id: item_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showConfirmationAlertItem: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            program/category/module/details                                
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.category_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }








    render() {
        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />


                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.moduleDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.moduleItemDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlertItem: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlertItem}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Category Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.category.program_id}`}>Program </NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Category Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-5 col-md-12">
                        <form className='form-horizontal' onSubmit={this.CategoryUpdateBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.category.category_title} name='category_title' onChange={this.handleChangecategory} />
                                        </div>
                                    </div>
                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value={this.state.category.category_description}

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Image/Icon   </h3>
                                </div>
                                <div className="card-body">

                                    <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify" data-default-file={this.state.category.category_icon ? this.state.baseApiURL + this.state.category.category_icon : '/assets/images/gallery/1.jpg'} />
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='category_status' onChange={this.handleChangeStatus}>
                                                <option selected={this.state.category.category_status == 1} value="1">Active</option>
                                                <option selected={this.state.category.category_status == 0} value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer text-right">
                                <button type="submit" className="btn btn-brand">Update</button>
                            </div>
                        </form>

                    </div>
                    <div className="col-xl-7 col-md-12">
                        <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Modules</h3>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="card-header pb-0 justify-content-end">

                                        <NavLink to={`/program/category/module/add/${this.state.category_id}`} className='btn btn-brand' title="add">Add Module</NavLink>

                                    </div>

                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table id='moduleListTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Module</th>
                                                <th class='text-left'>Status</th>
                                                <th class='text-center'>Resource count</th>
                                                <th class='text-center'>Topic count</th>                                                
                                                <th class='text-center'>Action</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            {
                                                this.state.modules.map((module, cindex) => (
                                                    <tr>
                                                        <td className='reorder text-left'>{cindex + 1}</td>
                                                        <td className="text-capitalize pl-1 table_content">
                                                            <NavLink data-id={module.module_id} to={`/program/category/module/details/${module.module_id}`} className="btn btn-icon btn-sm text-info brandColor text-left" title="View">{module.module_title}</NavLink>

                                                        </td>
                                                        <td class='text-left'>
                                                            {module.module_status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>

                                                        <td class='text-center'>
                                                            {module.resource_count ? module.resource_count : '--'}
                                                        </td>
                                                        <td class='text-center'>
                                                            {module.topic_count ? module.topic_count : '--'}
                                                        </td>

                                                        <td class='text-center'>
                                                            {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                            <NavLink to={`/program/category/module/details/${module.module_id}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>


                                                            <button value={module.module_id} onClick={this.moduleDelete()} className="btn btn-icon btn-sm " title="delete"><i class="fa fa-trash text-danger"></i></button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.modules.length == 0 && (<tfoot><tr><td colspan="6" className="text-center text-capitalize">{this.state.showModuleTableMessage}</td></tr></tfoot>)}
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Action Items</h3>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="card-header pb-0 justify-content-end">

                                        <NavLink to={`/program/category/item/add/${this.state.category_id}`} className='btn btn-brand' title="add">Add Item</NavLink>

                                    </div>

                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table id='moduleItemTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Item</th>
                                                <th class='text-center'>Status</th>
                                                <th class='text-center'>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.items.map((item, cindex) => (
                                                    <tr>
                                                        <td className='text-left reorder'>{cindex + 1}</td>
                                                        <td className="text-capitalize pl-1 table_content">
                                                            <NavLink data-id={item.item_id} to={`/program/category/item/update/${item.item_id}`} className="btn btn-icon btn-sm text-info brandColor text-left" title="View">{item.item_title}</NavLink>

                                                        </td>
                                                        <td class='text-center'>
                                                            {item.item_status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>
                                                        <td class='text-center'>
                                                            {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                            <NavLink to={`/program/category/item/update/${item.item_id}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>

                                                            <button value={item.item_id} onClick={this.moduleItemDelete()} className="btn btn-icon btn-sm " title="delete"><i class="fa fa-trash text-danger"></i></button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.items.length == 0 && (<tfoot><tr><td colspan="4" className="text-center text-capitalize">{this.state.showItemTableMessage}</td></tr></tfoot>)}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>

        )
    }
}

export default CategoryDetails
