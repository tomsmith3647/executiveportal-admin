import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import { loadProgressBar } from 'axios-progress-bar'
import 'axios-progress-bar/dist/nprogress.css'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
//import ReactPlayer from 'react-player'

import { PrismCode } from 'react-prism'
import { Player, ControlBar, BigPlayButton, PlaybackRateMenuButton } from 'video-react';
import { Button, Form, FormGroup, Label, Input, Progress, Modal, ModalHeader, ModalBody, ModalFooter, } from 'reactstrap';


const $ = window.$


class ModuleDetails extends Component {



    constructor(props, context) {
        super(props, context);

        this.state = {

            name: '',
            program_id: '',
            notes: '',
            tasks: [],
            resources: [],
            program: {},
            items: [],
            module: {},
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showAlertActionURL: '',
            showLoader: false,
            data_default_file: '/assets/images/gallery/1.jpg',
            video_size: 6000, //20MB
            image_size: 2, //2MB
            showResourceTableMessage: '',
            showResourceConfirmationAlert: false,
            playerSource: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            inputVideoUrl: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            source: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            fileName: 'Choose file',
            processBarPercent: 0,
            modal: false,
            fileUploading:false,
            access_permission:false,
        };


    }



    componentDidMount() {



        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const module_id = this.props.match.params.module_id;
            this.setState({ module_id: module_id });


            this.setState({ showLoader: true, showTaskTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/program/category/module/detail/" + module_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    this.setState({ access_permission: response.data.access_permission });
                    if (error_code == '200') {
                        //success      

                        this.setState({ module: response.data.module });
                        $('.dropify').dropify();
                        this.load();

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });




            //get Task
            axios({
                url: API_URL + "admin/program/category/module/topic/list/" + module_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;


                    if (error_code == '200') {
                        //success                        
                        this.setState({ tasks: response.data.topics });

                        var topicTable = $('#moduleTaskTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [3], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        topicTable.columns().iterator('column', function (ctx, idx) {
                            $(topicTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        topicTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = topicTable.row(diff[i].node).data();
                                const topic_id = $(rowData[1]).attr("data-id");
                                const catsort = { "topic_id": topic_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/program/category/module/topic/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showTaskTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


            //get Task
            this.setState({ showLoader: true, showResourceTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/program/category/module/resource/list/" + module_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;


                    if (error_code == '200') {
                        //success                        
                        this.setState({ resources: response.data.resources });

                        var resourceTable = $('#resourceTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [3], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });
                        resourceTable.columns().iterator('column', function (ctx, idx) {
                            $(resourceTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });
                        resourceTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = resourceTable.row(diff[i].node).data();
                                const resource_id = $(rowData[1]).attr("data-id");
                                const catsort = { "resource_id": resource_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/program/category/module/resource/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: false, showAlertMessage: error_message, showResourceTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }

        this.bs_input_file()
    }

    bs_input_file = () => {
        $(".input-file").before(
            function () {
                if (!$(this).prev().hasClass('input-ghost')) {
                    var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                    element.attr("name", $(this).attr("name"));
                    element.change(function () {
                        element.next(element).find('input').val((element.val()).split('\\').pop());
                    });
                    $(this).find("button.btn-choose").click(function () {
                        element.click();
                    });
                    $(this).find("button.btn-reset").click(function () {
                        element.val(null);
                        $(this).parents(".input-file").find('input').val('');
                    });
                    $(this).find('input').css("cursor", "pointer");
                    $(this).find('input').mousedown(function () {
                        $(this).parents('.input-file').prev().click();
                        return false;
                    });
                    return element;
                }
            }
        );
    }

    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }

    handleChangemodule = (e) => {
        this.setState({ module: { ...this.state.module, module_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            module: {
                ...this.state.module,
                module_description: e
            }
        })
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeModuleStatus = (e) => {
        this.setState({ module: { ...this.state.module, module_status: e.target.value } });
    }


    handlethumbnailImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose thumbnail image." });
            }
            else {
                const file = event.target.files[0];
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }                 
                else {
                    this.setState({
                        selected_thumbnail: file,
                    })

                }
            }
        }
    }


    handleUloadVideo = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            //            loadProgressBar();

            if (!event.target.files[0]) {
                this.setState({ fileName: 'Choose file' });
                this.setState({ showAlert: true, showAlertMessage: "Please choose video." });
            }
            else {
                const file = event.target.files[0];
                this.setState({ fileName: event.target.files[0].name });
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "mp4") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }                 
                else {
                    this.setState({
                        selected_video: file,
                    })

                    const formData = new FormData()



                    if (file) {
                        formData.append('module_video', file)
                    }

                    formData.append('module_id', this.state.module_id)

                    //loadProgressBar();

                    var A = this;
                    this.setState({fileUploading:true});
                    axios({
                        url: API_URL + "admin/program/category/module/imagevideo/upload",
                        method: 'post',
                        data: formData,
                        headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                        onUploadProgress: function (progressEvent) {
                            var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                            A.setState({ processBarPercent: percentCompleted })
                            
                        }
                    })
                        .then(response => {
                            this.setState({ showLoader: false });
                            const error_code = response.data.error_code;
                            const error_message = response.data.error_message;
                            this.setState({fileUploading:false});
                            if (error_code == '200') {
                                //success           

                                if (response.data.video_path) {
                                    this.setState({ module: { ...this.state.module, module_video: response.data.video_path } });
                                }

                                this.load();

                                //this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.module.category_id });
                            }
                            else {
                                //fail

                                this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                            }
                        })
                        .catch(err => {
                            //error
                            console.log(err);

                        });
                }
            }
        }
    }


    //ModuleUpdateBtn
    ModuleUpdateBtn = (event) => {

        //loadProgressBar()
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            if (!this.state.module.module_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter title." });
                return false;
            }



            const imagefile = this.state.selected_thumbnail;
            // if (!imagefile && !this.state.module.module_image) {
            //     this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please choose thumbnail image." });
            //     return false;
            // }

            const videofile = this.state.selected_video;
            // if (!videofile && this.state.module.module_video) {
            //     this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please choose video file." });
            //     return false;
            // }


            if (imagefile) {
                const image_extension = imagefile.type.split('/').pop().toLowerCase();
                if (image_extension != "jpeg" && image_extension != "jpg" && image_extension != "png" && image_extension != "bmp" && image_extension != "gif") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }                 
                // this.setState({ module: { ...this.state.module, module_image: imagefile } });

            }

            if (videofile) {

                const video_extension = videofile.type.split('/').pop().toLowerCase();
                if (video_extension != "mp4") {
                    event.target.value = '';
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }                 
            }


            // if (!this.state.module.module_description) {
            //     this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter description." });
            //     return false;
            // }




            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/module/update",
                method: 'post',
                data: this.state.module,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success   

                        if (this.state.selected_thumbnail || this.state.selected_video) {


                            const formData = new FormData()

                            if (this.state.selected_thumbnail) {
                                formData.append('module_image', this.state.selected_thumbnail)
                            }

                            // if (this.state.selected_video) {
                            //     formData.append('module_video', this.state.selected_video)
                            // }

                            formData.append('module_id', this.state.module_id)
                            axios({
                                url: API_URL + "admin/program/category/module/imagevideo/upload",
                                method: 'post',
                                data: formData,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    this.setState({ showLoader: false });
                                    const error_code = response.data.error_code;
                                    if (error_code == '200') {
                                        //success           

                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.module.category_id });
                                    }
                                    else {
                                        //fail

                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {

                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.module.category_id });
                        }

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }


    }

    //moduleResourceDelete
    //moduleResourceDelete
    moduleResourceDelete = () => (e) => {
        const resource_id = e.currentTarget.value;
        this.setState({ selected_resource_id: resource_id });
        this.setState({ showResourceConfirmationAlert: true });
    }


    //moduleResourceDeleteProcess
    moduleResourceDeleteProcess = () => {
        this.setState({ showResourceConfirmationAlert: false, showLoader: true });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            //const admin_id = e.currentTarget.value;
            const resource_id = this.state.selected_resource_id;

            axios({
                url: API_URL + "admin/program/category/module/resource/delete",
                method: 'post',
                data: {
                    resource_id: resource_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            program/category/module/details                                
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }






    //moduleTaskDelete
    moduleTaskDelete = () => (e) => {
        const topic_id = e.currentTarget.value;
        this.setState({ selected_topic_id: topic_id });
        this.setState({ showConfirmationAlert: true });
    }


    //subAdminDelete
    moduleTaskDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false, showLoader: true });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            //const admin_id = e.currentTarget.value;
            const topic_id = this.state.selected_topic_id;

            axios({
                url: API_URL + "admin/program/category/module/topic/delete",
                method: 'post',
                data: {
                    topic_id: topic_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            program/category/module/details                                
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.module_id });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }



    //player
    handleValueChange(e) {
        const { value } = e.target;
        this.setState({
            inputVideoUrl: value
        });
    }

    load() {
        this.player.load();
    }

    toggleOpen = () => {
        this.setState({
            modal: true
        })
    }

    toggleClose = () => {
        this.setState({
            modal: false
        })
    }



    render() {


        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.moduleResourceDeleteProcess}
                    onCancel={() => this.setState({ showResourceConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showResourceConfirmationAlert}

                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>


                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure? "
                    onConfirm={this.moduleTaskDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>




                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Module Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.module.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.module.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Module Details</li>



                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                {this.state.access_permission &&
                (<div className="row">
                    <div className="col-xl-5 col-md-12">
                        <form className='form-horizontal' onSubmit={this.ModuleUpdateBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.module.module_title} name='module_title' onChange={this.handleChangemodule} />
                                        </div>
                                    </div>
                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.AddUser} className="btn btn-brand">Add User</button>
                                    </div> */}
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Thumbnail  </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="thumbnail" onChange={this.handlethumbnailImage} data-height="200" className="dropify" data-default-file={this.state.module.module_image ? this.state.baseApiURL + this.state.module.module_image : this.state.data_default_file} />
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Video   </h3>
                                </div>
                                <div className="card-body">


                                    <div class="input-group">

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01"
                                                aria-describedby="inputGroupFileAddon01" onChange={this.handleUloadVideo} />
                                            <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                        </div>

                                    </div>

                                    {this.state.processBarPercent > 0 ?
                                        (
                                            <div className="my-3">
                                                <div className="text-center">{this.state.processBarPercent}%</div>
                                                <Progress value={this.state.processBarPercent} />
                                            </div>
                                        ) : ''}

                                    {/* <Button variant="primary" onClick={this.toggleOpen}>
                                        Launch demo modal
                                </Button> */}



                                    <Modal isOpen={this.state.modal} >
                                        <ModalHeader >Modal title</ModalHeader>
                                        <ModalBody>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </ModalBody>
                                        <ModalFooter>
                                            <Button color="secondary" onClick={this.toggleClose}>Cancel</Button>
                                        </ModalFooter>
                                    </Modal>
                                </div>
                            </div>

                            <div className="card" style={this.state.module.module_video ? {} : { display: 'none' }}>
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Video Preview </h3>

                                </div>
                                <div className="card-body">
                                    <Player
                                        poster={this.state.module.module_image ? this.state.baseApiURL + this.state.module.module_image : '/assets/poster.png'}
                                        ref={player => {
                                            this.player = player;
                                        }}
                                    //autoPlay
                                    >
                                        <BigPlayButton position="center" />
                                        <source src={this.state.baseApiURL + this.state.module.module_video} />
                                        <ControlBar autoHide={true} />
                                        <ControlBar>
                                            <PlaybackRateMenuButton rates={[5, 2, 1, 0.5, 0.1]} />
                                        </ControlBar>
                                    </Player>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value={this.state.module.module_description}
                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='module_status' onChange={this.handleChangeModuleStatus}>
                                                <option selected={this.state.module.module_status == 1} value="1">Active</option>
                                                <option selected={this.state.module.module_status == 0} value="0">Deactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer text-right">
                                
                                {this.state.fileUploading ? (<button type="button" disabled   className="btn btn-brand">Update</button>) : (<button type="submit" className="btn btn-brand">Update</button>)}
                                &nbsp;
                                <NavLink to={`/program/category/module/transcript/details/${this.state.module_id}`} className='btn btn-brand filled' title="add">Transcript Details</NavLink>
                            </div>
                        </form>

                    </div>
                    <div className="col-xl-7 col-md-12">

                        <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Resources</h3>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="card-header pb-0 justify-content-end">
                                        <NavLink to={`/program/category/module/resource/add/${this.state.module_id}`} className='btn btn-brand' title="add">Add Resource</NavLink>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">

                                    <table id='resourceTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Resource</th>
                                                <th class='text-left'>Status</th>
                                                <th class='text-center'>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.resources.map((res, rindex) => (
                                                    <tr>
                                                        <td className='text-left reorder'>{rindex + 1}</td>
                                                        <td className="text-capitalize pl-1 table_content">
                                                            <NavLink data-id={res.resource_id} to={`/program/category/module/resource/update/${res.resource_id}`} className="btn btn-icon btn-sm text-info brandColor text-left" title="View">{res.resource_title}</NavLink>

                                                        </td>
                                                        <td class='text-left'>
                                                            {res.resource_status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>
                                                        <td class='text-center'>
                                                            {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                            <NavLink to={`/program/category/module/resource/update/${res.resource_id}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>


                                                            <button value={res.resource_id} onClick={this.moduleResourceDelete()} className="btn btn-icon btn-sm " title="delete"><i class="fa fa-trash text-danger"></i></button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.resources.length == 0 && (<tfoot><tr><td colspan="4" className="text-center text-capitalize">{this.state.showResourceTableMessage}</td></tr></tfoot>)}
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Topics</h3>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="card-header pb-0 justify-content-end">
                                        <NavLink to={`/program/category/module/topic/add/${this.state.module_id}`} className='btn btn-brand' title="add">Add Topic</NavLink>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">

                                    <table id='moduleTaskTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Topic</th>
                                                <th class='text-left'>Status</th>
                                                <th class='text-center'>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.tasks.map((task, cindex) => (
                                                    <tr>
                                                        <td className='text-left reorder'>{cindex + 1}</td>
                                                        <td className="text-capitalize pl-1 table_content">
                                                            <NavLink data-id={task.topic_id} to={`/program/category/module/topic/update/${task.topic_id}`} className="btn btn-icon btn-sm text-info brandColor text-left" title="View">{task.topic_title}</NavLink>

                                                        </td>
                                                        <td class='text-left'>
                                                            {task.topic_status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>
                                                        <td class='text-center'>
                                                            {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                            <NavLink to={`/program/category/module/topic/update/${task.topic_id}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>


                                                            <button value={task.topic_id} onClick={this.moduleTaskDelete()} className="btn btn-icon btn-sm " title="delete"><i class="fa fa-trash text-danger"></i></button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.tasks.length == 0 && (<tfoot><tr><td colspan="4" className="text-center text-capitalize">{this.state.showTaskTableMessage}</td></tr></tfoot>)}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>)}


                {/* Transcript */}

            </Fragment>

        )
    }
}



export default ModuleDetails
