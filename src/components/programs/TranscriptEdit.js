import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
// import ReactSummernote from 'react-summernote';
// import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$

class TranscriptUpdate extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        transcript: {},
        transcript: {
            transcript_description: '',
            transcript_title: ''
        },
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_file: '',
        fileName: 'Choose file',
        file_size: 1000, //20MB
    }

    componentDidMount() {


        let THIS = this


        $('#activetranscript').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const module_id = this.props.match.params.module_id;
            this.setState({ module_id: module_id });
            this.setState({ transcript: { module_id: module_id } });

            axios({
                url: API_URL + "admin/program/category/module/transcript/detail/" + module_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ transcript: response.data.transcript });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });

        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    // handleChangetranscript = (e) => {
    //     const transcriptData = this.state.transcript;
    //     transcriptData[e.target.name] = e.target.value;
    //     this.setState({ transcript: transcriptData });
    // }
    handleChangetranscript = (e) => {
        this.setState({ transcript: { ...this.state.transcript, transcript_title: e.target.value } });
    }

    handleChangetranscriptStatus = (e) => {
        this.setState({ transcript: { ...this.state.transcript, transcript_status: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            transcript: {
                ...this.state.transcript,
                transcript_description: e.target.value
            }
        })

        console.log(e.target.value)

    }
    //add transcript
    TranscriptUpdateBtn = (e) => {
        e.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (this.state.selected_file) {
                const fileUpload = this.state.selected_file;

                const fileNameExt = fileUpload.type.split('/').pop().toLowerCase();
                var validExtensions = ['mp3','mpeg']; //array of valid extensions
                //if (t != "pdf" || t != "doc" || t != "xls" || t != "txt" || t != "jpg" || t != "gif" || t != "png" || t != "jpeg") {
                if ($.inArray(fileNameExt, validExtensions) == -1) {
                    e.target.value = '';
                    e.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid file." });
                    return false;
                }
                else if (fileUpload.size > (1024000 * this.state.file_size)) {
                    e.target.value = '';
                    e.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.file_size + "MB only" });
                    return false;
                }
            }

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/module/transcript/update",
                method: 'post',
                data: this.state.transcript,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success     


                        if (this.state.selected_file) {
                            const imagedata = new FormData()
                            imagedata.append('transcript_url', this.state.selected_file)
                            imagedata.append('transcript_id', response.data.transcript.transcript_id)
                            axios({
                                url: API_URL + "admin/program/category/module/transcript/file/upload",
                                method: 'post',
                                data: imagedata,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.transcript.module_id });
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.transcript.module_id });
                        }

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleUloadFile = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ fileName: 'Choose file' });
                this.setState({ selected_file: '' })
                //this.setState({ showAlert: true, showAlertMessage: "Please choose." });
            }
            else {
                const file = event.target.files[0];

                this.setState({ fileName: event.target.files[0].name });

                const fileNameExt = file.type.split('/').pop().toLowerCase();


                var validExtensions = ['mp3','mpeg']; //array of valid extensions

                if ($.inArray(fileNameExt, validExtensions) == -1) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ selected_file: file, data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid file." });
                    return false;
                }
                else if (file.size > (1024000 * this.state.file_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ selected_file: file, data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.file_size + "MB only" });
                    return false;
                }
                else {
                    this.setState({
                        selected_file: file,
                    })
                }
            }
        }
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Transcript Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.transcript.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.transcript.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/module/details/${this.state.transcript.module_id}`}>Module</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Transcript Details</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <form className='form-horizontal' onSubmit={this.TranscriptUpdateBtn}>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <textarea className='form-control' rows="10" onChange={this.handleSummerNote} value={this.state.transcript.transcript_description} />

                                    {/* <ReactSummernote
                                        value={this.state.transcript.transcript_description}

                                        po
                                        pover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview']]
          ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    /> */}

                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload (mp3 file)</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="inputGroupFile01"
                                                        aria-describedby="inputGroupFileAddon01" accept="application/mp3" onChange={this.handleUloadFile} />
                                                    <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                                </div>
                                            </div>
                                            <br />
                                            {this.state.transcript.transcript_url && (<a href={this.state.transcript.transcript_url} target='_blank'>Click here to view uploaded file</a>)}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='transcript_status' onChange={this.handleChangetranscriptStatus}>
                                                <option selected={this.state.transcript.transcript_status == 1} value="1">Active</option>
                                                <option selected={this.state.transcript.transcript_status == 0} value="0">Deactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer text-right">
                                <button type="submit" className="btn btn-brand">Transcript Update</button>
                            </div>
                        </form>

                    </div>
                </div>
            </Fragment>
        )
    }
}

export default TranscriptUpdate
