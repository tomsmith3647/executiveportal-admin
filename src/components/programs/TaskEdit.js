import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import { PrismCode } from 'react-prism'
import { Player, ControlBar, BigPlayButton, PlaybackRateMenuButton } from 'video-react';
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';
const $ = window.$
class TaskUpdate extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {

            name: '',
            notes: '',
            program_id: '',
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            topic: {
                topic_description: '',
                topic_title: ''
            },

            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showAlertActionURL: '',
            showLoader: false,
            selected_thumnail: '',
            selected_video: '',
            data_default_file: '/assets/images/gallery/1.jpg',
            video_size: 6000, //20MB
            image_size: 2, //2MB,
            card_url_video: 0,
            card_upload_video: 0,
            playerSource: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            inputVideoUrl: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            source: 'http://dev5.logicnext.com:7550/storage/module-video-SP9y1tdkkRpHX1fk8RLj1FfFymkdOFfWC7BUu.mp4',
            fileName: 'Choose file',
            processBarPercent: 0,
            fileUploading:false,
            access_permission:false,
        };

        this.handleValueChange = this.handleValueChange.bind(this);
        this.updatePlayerInfo = this.updatePlayerInfo.bind(this);



        this.play = this.play.bind(this);
        this.pause = this.pause.bind(this);
        this.load = this.load.bind(this);
        this.changeCurrentTime = this.changeCurrentTime.bind(this);
        this.seek = this.seek.bind(this);
        this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
        this.changeVolume = this.changeVolume.bind(this);
        this.setMuted = this.setMuted.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.playerSource !== prevState.playerSource) {
            this.player.load();
        }
    }

    componentDidMount() {

        this.player.subscribeToStateChange(this.handleStateChange.bind(this));

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const topic_id = this.props.match.params.task_id;
            this.setState({ topic_id: topic_id });
            this.setState({ topic: { topic_id: topic_id } });

            axios({
                url: API_URL + "admin/program/category/module/topic/detail/" + topic_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    this.setState({ access_permission: response.data.access_permission });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ topic: response.data.topic, playerSource: ApiURL + this.state.topic.topic_video });
                        $('.dropify').dropify();
                        this.load();
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    // handleChangetask = (e) => {
    //     const taskData = this.state.topic;
    //     taskData[e.target.name] = e.target.value;
    //     this.setState({ topic: taskData });
    // }
    handleChangetask = (e) => {
        this.setState({ topic: { ...this.state.topic, topic_title: e.target.value } });
    }

    handleChangeTaskStatus = (e) => {
        this.setState({ topic: { ...this.state.topic, topic_status: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            topic: {
                ...this.state.topic,
                topic_description: e
            }
        })
    }


    handleThumnailImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose Thumnail image." });
            }
            else {
                const file = event.target.files[0];
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }
                else if (file.size > (1024000 * this.state.image_size)) {
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.image_size + "MB only" });
                    return false;
                }
                else {
                    this.setState({
                        selected_thumnail: file,
                    })

                }
            }
        }
    }


    handleUloadVideo = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ fileName: 'Choose file' });
                this.setState({ showAlert: true, showAlertMessage: "Please choose video." });
            }
            else {
                const file = event.target.files[0];
                this.setState({ fileName: event.target.files[0].name });
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "mp4") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }
                else if (file.size > (1024000 * this.state.video_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.video_size + "MB only" });
                    return false;
                }
                else {
                    this.setState({
                        selected_video: file,
                    })


                    const formData = new FormData()


                    if (file) {
                        formData.append('module_video', file)
                    }
                    this.setState({fileUploading:true});
                    formData.append('topic_id', this.state.topic_id)
                    var A = this;
                    axios({
                        url: API_URL + "admin/program/category/module/topic/imagevideo/upload",
                        method: 'post',
                        data: formData,
                        headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                        onUploadProgress: function (progressEvent) {
                            var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                            A.setState({ processBarPercent: percentCompleted })
                            console.log(percentCompleted)
                        }
                    })
                        .then(response => {
                            this.setState({ showLoader: false });
                            const error_code = response.data.error_code;
                            this.setState({fileUploading:false});
                            if (error_code == '200') {
                                //success 

                                if (response.data.video_path) {
                                    this.setState({ topic: { ...this.state.topic, topic_video: response.data.video_path } });
                                }

                                this.load();

                            }
                            else {
                                //fail
                                //this.setState({showAlertIcon : 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                            }
                        })
                        .catch(err => {
                            //error
                            console.log(err);
                        });

                }
            }
        }
    }

    //add task
    topicUpdateBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            if (!this.state.topic.topic_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter topic title." });
                return false;

            }




            const imagefile = this.state.selected_thumnail;
            // if(!imagefile && !this.state.topic.topic_image)
            // {
            //     this.setState({ showAlertIcon : 'warning', showAlert: true, showAlertMessage: "Please choose thumnail image." });                                
            //     return false;
            // }

            const videofile = this.state.selected_video;
            // if(!videofile && this.state.topic.topic_video)
            // {
            //     this.setState({ showAlertIcon : 'warning', showAlert: true, showAlertMessage: "Please choose video file." });                                
            //     return false;
            // }

            if (imagefile) {
                const image_extension = imagefile.type.split('/').pop().toLowerCase();
                if (image_extension != "jpeg" && image_extension != "jpg" && image_extension != "png" && image_extension != "bmp" && image_extension != "gif") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }
                else if (imagefile.size > (1024000 * this.state.image_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.image_size + "MB only" });
                    return false;
                }
            }


            if (videofile) {
                const video_extension = videofile.type.split('/').pop().toLowerCase();
                if (video_extension != "mp4") {
                    event.target.value = '';
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }
                else if (videofile.size > (1024000 * this.state.video_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.video_size + "MB only" });
                    return false;
                }
            }

            // if(!this.state.topic.topic_description)
            // {
            //     this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: "Please enter topic description." }); 
            //     return false;
            // }

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/module/topic/update",
                method: 'post',
                data: this.state.topic,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success   
                        //success          
                        if (this.state.selected_thumnail || this.state.selected_video) {
                            const formData = new FormData()
                            if (this.state.selected_thumnail) {
                                formData.append('module_image', this.state.selected_thumnail)
                            }


                            this.setState({ showLoader: true });
                            formData.append('topic_id', this.state.topic_id)
                            axios({
                                url: API_URL + "admin/program/category/module/topic/imagevideo/upload",
                                method: 'post',
                                data: formData,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },

                            })
                                .then(response => {
                                    this.setState({ showLoader: false });
                                    const error_code = response.data.error_code;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.topic.module_id });
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showLoader: false });
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/module/details/' + this.state.topic.module_id });
                        }

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    //handleUploadVideoType
    handleUploadVideoType = (e) => {

        if (e.currentTarget.value = 'video') {
            this.setState({ card_url_video: 0 });
            this.setState({ card_upload_video: 1 });
        }

    }

    //handleUploadURLType    
    handleUploadURLType = (e) => {

        if (e.currentTarget.value = 'url') {
            this.setState({ card_url_video: 1 });
            this.setState({ card_upload_video: 0 });
        }


    }


    //player
    handleValueChange(e) {
        const { value } = e.target;
        this.setState({
            inputVideoUrl: value
        });
    }

    updatePlayerInfo() {
        const { inputVideoUrl } = this.state;
        this.setState({
            playerSource: inputVideoUrl
        });
    }


    setMuted(muted) {
        return () => {
            this.player.muted = muted;
        };
    }

    handleStateChange(state) {
        // copy player state to this component's state
        this.setState({
            player: state
        });
    }

    play() {
        this.player.play();
    }

    pause() {
        this.player.pause();
    }

    load() {
        this.player.load();
    }

    changeCurrentTime(seconds) {
        return () => {
            const { player } = this.player.getState();
            this.player.seek(player.currentTime + seconds);
        };
    }

    seek(seconds) {
        return () => {
            this.player.seek(seconds);
        };
    }

    changePlaybackRateRate(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.playbackRate = player.playbackRate + steps;
        };
    }

    changeVolume(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.volume = player.volume + steps;
        };
    }

    changeSource(name) {
        return () => {

            this.player.load();
        };
    }

    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Topic Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/details/${this.state.topic.program_id}`}>Program</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.topic.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/module/details/${this.state.topic.module_id}`}>Module</NavLink></li>

                                    <li className="breadcrumb-item active" aria-current="page">Update Topic</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-md-6">
                        <form className='form-horizontal' onSubmit={this.topicUpdateBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.topic.topic_title} name='topic_title' onChange={this.handleChangetask} />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Thumbnail  </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="thumnail" onChange={this.handleThumnailImage} data-height="200" className="dropify" data-default-file={this.state.topic.topic_image ? this.state.baseApiURL + this.state.topic.topic_image : this.state.data_default_file} />
                                </div>
                            </div>


                            {/* <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Thumbnail  {this.state.card_url_video}  {this.state.card_upload_video} {this.state.playerSource} </h3>
                                </div>
                                <div className="card-body">                                                                 
                                       
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="video_type" value="video" onChange={this.handleUploadVideoType} /> Upload Video
                                        </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="video_type" value="url" onChange={this.handleUploadURLType} /> Enter URL
                                            </label>
                                        </div>

                                   
                                </div>
                            </div> */}


                            {/* <div className="card" style={this.state.card_url_video ? {display: 'block'} : { display: 'none' }}>
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Enter URL   </h3>
                                </div>
                                <div className="card-body">                                                                
                                    <input type="text" className="form-control" value={this.state.topic.video_url} name='video_url' onChange={this.handleChangetask} />
                                    <div style={this.state.topic.topic_video ? {display: 'block'} : { display: 'none' }}>
                                        <Player
                                            ref={player => {
                                                this.player = player;
                                            }}
                                            //autoPlay
                                        >
                                            <BigPlayButton position="center" />
                                            <source src={this.state.playerSource} />
                                            <ControlBar autoHide={true} />
                                        </Player>
                                        <div className="py-3" >
                                            <Button onClick={this.play} className="mr-3">
                                                Play
                                            </Button>
                                            <Button onClick={this.pause} className="mr-3">
                                                Pause
                                            </Button>
                                            <Button onClick={this.load} className="mr-3">
                                                Replay
                                            </Button>
                                        </div>
                                    </div>                                                                                                                    
                                </div>
                            </div> */}

                            <div className="card" >
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Video </h3>
                                </div>
                                <div className="card-body">

                                    <div class="input-group">

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01"
                                                aria-describedby="inputGroupFileAddon01" onChange={this.handleUloadVideo} />
                                            <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                        </div>

                                    </div>

                                    {this.state.processBarPercent > 0 ?
                                        (
                                            <div className="my-3">
                                                <div className="text-center">{this.state.processBarPercent}%</div>
                                                <Progress value={this.state.processBarPercent} />
                                            </div>
                                        ) : ''}

                                </div>
                            </div>

                            <div className="card" style={this.state.topic.topic_video ? { display: 'block' } : { display: 'none' }}>
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Video Preview</h3>
                                </div>
                                <div className="card-body">
                                    <Player

                                        poster={this.state.topic.topic_image ? this.state.baseApiURL + this.state.topic.topic_image : '/assets/poster.png'}
                                        ref={player => {
                                            this.player = player;
                                        }}
                                    //autoPlay
                                    >
                                        <BigPlayButton position="center" />
                                        <source src={this.state.baseApiURL + this.state.topic.topic_video} />
                                        <ControlBar autoHide={true} />
                                        <ControlBar>
                                            <PlaybackRateMenuButton rates={[5, 2, 1, 0.5, 0.1]} />
                                        </ControlBar>
                                    </Player>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value={this.state.topic.topic_description}

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>



                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='topic_status' onChange={this.handleChangeTaskStatus}>
                                                <option selected={this.state.topic.topic_status == 1} value="1">Active</option>
                                                <option selected={this.state.topic.topic_status == 0} value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer text-right">
                                
                                {this.state.fileUploading ? (<button type="button" disabled   className="btn btn-brand">Update</button>) : (<button type="submit" className="btn btn-brand">Update</button>)}
                                

                            </div>
                        </form>

                    </div>
                </div>
            </Fragment>
        )
    }
}

export default TaskUpdate
