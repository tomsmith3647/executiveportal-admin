import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';
const $ = window.$
class ModuleAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        module: {
            module_description: '',
            module_title: ''
        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 6000, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        fileUploading:false,
        access_permission:false,

    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activemodule').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });


        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const category_id = this.props.match.params.category_id;
            this.setState({ category_id: category_id });
            this.setState({ module: { category_id: category_id } });
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/detail/" + category_id,
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success       
                        this.setState({ module: { ...this.state.module, program_id: response.data.category.program_id } });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }



    // handleChangemodule = (e) => {
    //     const moduleData = this.state.module;
    //     moduleData[e.target.name] = e.target.value;
    //     this.setState({ module: moduleData });
    // }
    handleChangemodule = (e) => {
        this.setState({ module: { ...this.state.module, module_title: e.target.value } });
    }

    handleSummerNote = (e) => {
        this.setState({

            module: {
                ...this.state.module,
                module_description: e
            }
        })
    }




    handleThumnailImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose Thumnail image." });
            }
            else {
                const file = event.target.files[0];
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }                 
                else {
                    this.setState({
                        selected_thumnail: file,
                    })
                }
            }
        }
    }


    handleUloadVideo = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!event.target.files[0]) {
                this.setState({ fileName: 'Choose file' });
                this.setState({ showAlert: true, showAlertMessage: "Please choose video." });
            }
            else {
                const file = event.target.files[0];
                this.setState({ fileName: event.target.files[0].name });
                const t = file.type.split('/').pop().toLowerCase();
                if (t != "mp4") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }
                else if (file.size > (1024000 * this.state.video_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.video_size + "MB only" });
                    return false;
                }
                else {
                    this.setState({
                        selected_video: file,
                    })

                    const formData = new FormData()

                    formData.append('module_video', file)
                    var A = this;
                    this.setState({fileUploading:true});
                    axios({
                        url: API_URL + "admin/program/category/module/add/imagevideo/upload",
                        method: 'post',
                        data: formData,
                        headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token },
                        onUploadProgress: function (progressEvent) {
                            var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                            A.setState({ processBarPercent: percentCompleted })
                        }
                    })
                        .then(response => {

                            const error_code = response.data.error_code;
                            const error_message = response.data.error_message;
                            this.setState({fileUploading:false});
                            if (error_code == '200') {
                                //success                 
                                if (response.data.video_path) {
                                    this.setState({ module: { ...this.state.module, module_video: response.data.video_path } });
                                }
                                this.setState({ showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message, video_path: response.data.video_path });

                            }
                            else {
                                //fail
                                this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                            }
                        })
                        .catch(err => {
                            //error
                            console.log(err);
                        });
                }
            }
        }
    }




    //add module
    ModuleAddBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.module.module_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter title." });
                return false;
            }



            const imagefile = this.state.selected_thumnail;
            // if (!imagefile) {
            //     this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please choose thumnail image." });
            //     return false;
            // }

            const videofile = this.state.selected_video;
            // if (!videofile) {
            //     this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please choose video file." });
            //     return false;
            // }

            if (imagefile) {
                const image_extension = imagefile.type.split('/').pop().toLowerCase();
                if (image_extension != "jpeg" && image_extension != "jpg" && image_extension != "png" && image_extension != "bmp" && image_extension != "gif") {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid image file." });
                    return false;
                }
                else if (imagefile.size > (1024000 * this.state.image_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.image_size + "MB only" });
                    return false;
                }
            }


            if (videofile) {
                const video_extension = videofile.type.split('/').pop().toLowerCase();
                if (video_extension != "mp4") {
                    event.target.value = '';
                    event.target.value = null;

                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select a valid video file." });
                    return false;
                }
                else if (videofile.size > (1024000 * this.state.video_size)) {
                    event.target.value = '';
                    event.target.value = null;
                    this.setState({ data_default_file: this.state.data_default_file, showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Max Upload size is " + this.state.video_size + "MB only" });
                    return false;
                }
            }

            // else if (!this.state.module.module_description) {
            //     this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter description." });
            //     return false;
            // }

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/program/category/module/add",
                method: 'post',
                data: this.state.module,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success 

                        if (this.state.selected_thumnail || this.state.selected_video) {
                            const formData = new FormData()
                            formData.append('module_image', this.state.selected_thumnail)
                            formData.append('module_id', response.data.module.module_id)
                            this.setState({ showLoader: true });
                            axios({
                                url: API_URL + "admin/program/category/module/imagevideo/upload",
                                method: 'post',
                                data: formData,
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                        
                                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.category_id });
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: response.data.error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        }
                        else {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/' + this.state.category_id });
                        }


                        //this.setState({showAlertIcon : 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/program/category/details/'+this.state.category_id});                         
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Module</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/programs">Program List</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/detail/${this.state.module.program_id}`}>Program Details</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page"><NavLink to={`/program/category/details/${this.state.category_id}`}>Category</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Add Module</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-5 col-md-12">
                        <form className='form-horizontal' enctype="multipart/form-data" onSubmit={this.ModuleAddBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.module.module_title} name='module_title' onChange={this.handleChangemodule} required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Thumbnail  </h3>
                                </div>
                                <div className="card-body">
                                    <input type="file" name="thumnail" onChange={this.handleThumnailImage} data-height="200" className="dropify" data-default-file={this.state.data_default_file} />
                                </div>
                            </div>


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Upload Video   </h3>
                                </div>
                                <div className="card-body">

                                    <div class="input-group">

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="video"
                                                aria-describedby="inputGroupFileAddon01" onChange={this.handleUloadVideo} />
                                            <label class="custom-file-label" for="inputGroupFile01">{this.state.fileName}</label>
                                        </div>
                                    </div>
                                    {this.state.processBarPercent > 0 ?
                                        (
                                            <div className="my-3">
                                                <div className="text-center">{this.state.processBarPercent}%</div>
                                                <Progress value={this.state.processBarPercent} />
                                            </div>
                                        ) : ''}
                                </div>


                            </div>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value=""
                                        required
                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}

                                    />

                                </div>
                            </div>

                            <div className="card-footer text-right">                                
                                {this.state.fileUploading ? (<button type="button" disabled   className="btn btn-brand">Submit</button>) : (<button type="submit" className="btn btn-brand">Submit</button>)}
                            </div>
                        </form>


                    </div>


                    <div className="col-xl-7 col-md-12">
                        <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Topics</h3>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="card-header pb-0 justify-content-end">
                                        <button className='btn btn-brand' disabled={true}>Add Topic</button>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">

                                    <table id='moduleTaskTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Topic</th>
                                                <th class='text-center'>Status</th>
                                                <th class='text-center'>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot><tr><td colspan="4" className="text-center text-capitalize">Topic Not Found.</td></tr></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>

        )
    }
}

export default ModuleAdd
