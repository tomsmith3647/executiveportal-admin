import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

// import DropdownTreeSelect from 'react-dropdown-tree-select';
// import 'react-dropdown-tree-select/dist/styles.css';

import Container from './DropdownContainer';


import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';

import reactCSS from 'reactcss'
import {SketchPicker, PhotoshopPicker, ChromePicker} from 'react-color';

const $ = window.$
class EventCategoryDetails extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        event: {
            event_description: '',
            event_title: '',
            event_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        role_list:[],
        selectedMember:[],
        selected_members : [],
        startDate: new Date(),
        endDate: new Date(),
        currentDate: new Date(),
        selectedDate: '',
        pgcolor:'#F1CA29',
        pgcolorrgb:{},
        displayColorPicker:false,
        event_color:'#F1CA29',

        category:{},
        
    }

    componentDidMount() {

         
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            //const selectedDate = this.props.match.params.date;
            //this.setState({startDate:new Date(selectedDate), endDate:new Date(selectedDate)});
             
            
            this.getEventCategoryDetails();
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    getEventCategoryDetails = () => {
         
        
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        const category_id = this.props.match.params.category_id;
        axios({
            url: API_URL + "admin/event/category/details/"+category_id,
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ category: response.data.category });                                         
                }
                else {
                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });                    
                }
            })
            .catch(err => {
                console.log(err);
            });       
    }
 
 

 

    handleChange = (e) => {
        this.setState({ category: { ...this.state.category, category_name: e.target.value } });
    }
 
    handleSummerNote = (e) => {
        this.setState({
            category: {
                ...this.state.category,
                category_description: e
            }
        })
    }

 
    //add task
    EventCategoryDetailsBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const selected_member = this.state.selectedMember;
            
            if (!this.state.category.category_name) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter category name." });
                return false;

            }
            if ((this.state.category.category_name).trim().length ==0) {
                this.setState({ category: { ...this.state.category, category_name: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid category name." });
                return false;
            }                         
             
 

             
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/event/category/edit",
                method: 'post',
                data: this.state.category,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                          
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/event/categories'});                         
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions
    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }

    handleChangeCategoryStatus = (e) => {
        this.setState({ category: { ...this.state.category, category_status: e.target.value } });
    }


   
                
    render() {
         
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Category Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li> 
                                    <li className="breadcrumb-item"><NavLink to="/events">Event</NavLink></li>            
                                    <li className="breadcrumb-item"><NavLink to="/event/categories">Categories</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-xl-6 col-md-12">
                        <form className='form-horizontal' onSubmit={this.EventCategoryDetailsBtn}>
                         
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <span className="form-control">
                                            {this.state.category.category_name}
                                            &nbsp;
                                            </span>                                            
                                        </div>
                                    </div>                                
                                </div>
                            </div>    
 

                            {/* <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    
                                    <span className="form-control">
                                        <div dangerouslySetInnerHTML={{ __html: this.state.category.category_description }} />
                                        &nbsp;
                                    </span>
                                </div>
                            </div> */}

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                        <span className="form-control text-center">
                                        {this.state.category.category_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
 

                            
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default EventCategoryDetails
