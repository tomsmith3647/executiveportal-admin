import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class EventCategoryList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        categories: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_category_id: '',
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
             
            this.getEventCategoryList();


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }


    getEventCategoryList = () => {
         
        
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "admin/event/category/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ categories: response.data.categories });

                    
                        var categoryTable = $('#usersTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [4], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            //"pageLength": 500,
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                            
                        });

                        categoryTable.columns().iterator('column', function (ctx, idx) {
                            $(categoryTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        categoryTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = categoryTable.row(diff[i].node).data();
                                const category_id = $(rowData[1]).attr("data-id");
                                const catsort = { "category_id": category_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/category/category/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });
                   
                }
                else {

                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });       
    }



    getEventCategoryListAction = () => {
         
        
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "admin/event/category/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ categories: response.data.categories });                                         
                }
                else {

                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });       
    }


   

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    //categoryDelete    
    categoryDelete = () => (e) => {
        
        const category_id = e.currentTarget.dataset.id ;
        this.setState({ selected_category_id: category_id });
        this.setState({ showConfirmationAlert: true });
    }

    //categoryDeleteProcess
    categoryDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const category_id = this.state.selected_category_id;

            axios({
                url: API_URL + "admin/event/category/delete",
                method: 'post',
                data: {
                    category_id: category_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            category/category/module/details       
                        
                        this.getEventCategoryListAction();

                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }

    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }
 

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.categoryDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Categories</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item"><NavLink to="/events">Events</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">categories</li>
                        </ol>
                    </div>
                </div>
                <br />
                <div className="tab-pane" id="Holiday-all">

                    <div className="card-footer text-right">
                        <NavLink to="/event/category/add" className="btn btn-brand">Add Category</NavLink>
                    </div>

                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th >#</th>                                            
                                            <th className='text-left' >Title</th>                                                                                                                              
                                            <th className='text-left'>Status</th>                                                                                                                                     
                                            <th className='text-left' >Created Date</th>                                            
                                            <th className='text-center' >Action</th>                                             
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.categories.map((category, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>                                                    
                                                    <td className="text-left text-capitalize">                                                
                                                            <NavLink data-id={category.category_id} to={`/event/category/details/${category.category_id}`} className="text-info brandColor" title="View Detail">
                                                            {category.category_name}
                                                            </NavLink>                                                        
                                                    </td>
                                                    <td className='text-left'>
                                                        {category.category_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                    </td>
                                                    <td className='text-left'><span className='dateFormat'>{category.created_date}</span>{(new Date(this.convertUTCDateToLocalDate(new Date(category.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}</td>                                                                                                        
                                                    <td className='text-center'>
                                                    <NavLink to={`/event/category/edit/${category.category_id}`} className="text-info brandColor p-1" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>                                                    
                                                    <a href='javascript:void(0);' onClick={this.categoryDelete()} data-id={category.category_id} className="brandColor p-1" title="Edit"><i className="fa fa-trash text-danger"></i></a>
                                                    </td>                                                     
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                    {this.state.categories.length == 0 && (<tfoot><tr><td colspan="7" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'No category'}</td></tr></tfoot>)}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default EventCategoryList
