import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

// import DropdownTreeSelect from 'react-dropdown-tree-select';
// import 'react-dropdown-tree-select/dist/styles.css';

import Container from './DropdownContainer';


import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';

import reactCSS from 'reactcss'
import {SketchPicker, PhotoshopPicker, ChromePicker} from 'react-color';
 

const $ = window.$
class EventAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        event: {
            event_description: '',
            event_title: '',
            event_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        role_list:[],
        selectedMember:[],
        selected_members : [],
        startDate: new Date(),
        endDate: new Date(new Date().getTime() + 15*60000),
        currentDate: new Date(),
        selectedDate: '',
        pgcolor:'#F1CA29',
        pgcolorrgb:{},
        displayColorPicker:false,
        event_color:'#F1CA29',
        categories:[],
        minTime:'',
        
    }

    componentDidMount() {

        
       

        const rgbcolor = this.convertHex(this.state.pgcolor);
        this.setState({pgcolorrgb: rgbcolor});

        $('.dropify').dropify();
        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            //const selectedDate = this.props.match.params.date;
            //this.setState({startDate:new Date(selectedDate), endDate:new Date(selectedDate)});
             
            
            this.getRoleMembers();
            this.getEventCategories();
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    

    getEventCategories = () => {
         
        
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
        
        axios({
            url: API_URL + "admin/event/category/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ categories: response.data.categories });                                         
                }
                else {

                    this.setState({ showLoader: false, showProgaramTableMessage: error_message });
                    //fail
                    //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
            })
            .catch(err => {
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });       
    }

    convertHex = (hex,opacity=100) => {
        hex = hex.replace('#','');
        const r = parseInt(hex.substring(0,2), 16);
        const g = parseInt(hex.substring(2,4), 16);
        const b = parseInt(hex.substring(4,6), 16);
    
        //const result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
        const a = opacity/100;
        const result = {r,g,b,a}
        return result;
    }

    getRoleMembers = () => {         
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });        
        axios({
            url: API_URL + "admin/rolemember/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            const ApiURL = response.data.api_url;
            this.setState({ baseApiURL: ApiURL });
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success                        
                this.setState({ role_list: response.data.role_list });                                                     
            }
            else{
                //success                        
                this.setState({ role_list: [] });                                                       
            }            
        })
        .catch(err => {
            this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
        });        
    }
 

    handleChangeMembers = (currentNode, selectedNodes) => {
        this.setState
        ({
            selectedMember: selectedNodes
        });                          
    }
         
    removeSelectedMember = (myObjects,prop,valu) =>
    {
         return myObjects.filter(function (val) {
          return val[prop] !== valu;
      });
    }

    handleChange = (e) => {
        this.setState({ event: { ...this.state.event, event_title: e.target.value } });
    }
 
    handleSummerNote = (e) => {
        this.setState({
            event: {
                ...this.state.event,
                event_description: e
            }
        })
    }

    dateTimeUTC = (date) => {
        var now = new Date(date);
        var utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
        return utc;
    }

    dateFormatBackend = (selectedDate) => {
        var dt = new Date(selectedDate) ;

       return `${
            dt.getFullYear().toString().padStart(4, '0')}-${
            (dt.getMonth()+1).toString().padStart(2, '0')}-${
            dt.getDate().toString().padStart(2, '0')} ${
            dt.getHours().toString().padStart(2, '0')}:${
            dt.getMinutes().toString().padStart(2, '0')}:${
            dt.getSeconds().toString().padStart(2, '0')}`;
    }
    //add task
    EventAddBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const selected_member = this.state.selectedMember;
            if(selected_member.length == 0) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select role/member." });
                return false;
            }
            if (!this.state.event.event_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter event title." });
                return false;

            }
            if ((this.state.event.event_title).trim().length ==0) {
                this.setState({ event: { ...this.state.event, event_title: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid event title." });
                return false;
            }                         
            const selectedData = [];
            for(var u=0;u < selected_member.length; u++)
            {
                const selectData = selected_member[u];
                const rmname = selectData.label;
                const rmid = selectData.value;
                const rmchildren = selectData._children ?  selectData._children : Array();
                var selmember = {};
                if(rmchildren.length > 0)
                {
                    selmember = {name:rmname, id:rmid, type:'role'};
                    
                }
                else
                {
                    selmember = {name:rmname, id:rmid, type:'member'};
                }
                selectedData.push(selmember);
                
            }
 
            var postData = {
                title: this.state.event.event_title,
                description: this.state.event.event_description,
                start_date: this.dateFormatBackend(this.state.startDate),
                end_date: this.dateFormatBackend(this.state.endDate),

                utc_start_date: this.dateFormatBackend(this.dateTimeUTC(this.state.startDate)),
                utc_end_date: this.dateFormatBackend(this.dateTimeUTC(this.state.endDate)),

                color:this.state.event_color,
                category_id: this.state.event.category_id,
                selected_member: selectedData,
                recurring: this.state.event.recurring,
                recurring_value: this.state.event.recurring_value,
            }
            var A = this;   
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/event/create",
                method: 'post',
                data: postData,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                          
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/events'});                         
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });
        }
        //Get user permissions
    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL == 'refresh') 
        {
            window.location.reload();  
        } 
        else if(this.state.showAlertActionURL) 
        {
            window.location.href = this.state.showAlertActionURL;  
        }
    }


    
    handleChangeStartDate = (date) => {

        //const selectDate = this.convertUTCDateToLocalDate(date);        
        const timestamp = Date.parse(date);         
        if(timestamp < 1)
        {            
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please select / enter valid date', showAlertActionURL: '' });
            return false;
        }

        if(timestamp < Date.parse(new Date()))
        {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please select valid time', showAlertActionURL: '' });
            return false;
        }
         

        this.setState({
          startDate: timestamp
          
        });

       var edate =  new Date(new Date(date).getTime() + 15*60000);
        const etimestamp = Date.parse(edate);  

        this.setState({
            endDate: etimestamp  
             
          });

      };

 

      handleChangeEndDate = (date) => {

        //const selectDate = this.convertUTCDateToLocalDate(date);    
        
        const timestamp = Date.parse(date);         
        if(timestamp < 1)
        {            
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please select / enter valid date', showAlertActionURL: '' });
            return false;
        }

        if(timestamp < this.state.startDate)
        {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: 'Please select valid time', showAlertActionURL: '' });
            return false;
        }

        this.setState({
            endDate: timestamp
        });
      };


      //****************** Color Picker ***************/
      handleChangeColor = (color, event) => {
        this.setState({ pgcolor: color.hex, pgcolorrgb: color.rgb });
        this.setState({event_color: color.hex});        
      };
      handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
      };
    
      handleClose = () => {
        this.setState({ displayColorPicker: false })
      };
      //****************** End Color Picker ***************/

      
    handleChangeCategory = (e) => {
        this.setState({ event: { ...this.state.event, category_id: e.target.value } });
    }


    handleChangeRecurringValue = (e) => {
        this.setState({ event: { ...this.state.event, recurring_value: e.target.value } });
    }
 
    handleActionRecurring = (e) => {                 
            const isChecked = e.target.checked ? 1 : 0;
            this.setState({ event: { ...this.state.event, recurring: isChecked } });        
    }
      
                
    render() {
        var dropdownroledata = [];
        var roleData = this.state.role_list;
        for(var i =0; i < roleData.length; i++)
        {
            const rolemember = roleData[i];
            const role_id = rolemember.role_id;
            const role_title = rolemember.role_title;
            const members = rolemember.members;
            var dropdownmemberdata = [];
            for(var k =0; k < members.length; k++)
            {
                const member = members[k];
                const member_id = member.member_id;
                const member_first_name = member.first_name;
                const member_last_name = member.last_name;
                const member_full_name = member_first_name+' '+member_last_name;
                const memberdata = {label: member_full_name, value: member_id}
                dropdownmemberdata.push(memberdata);
            }
            const role_data = {label: role_title, value: role_id, children: dropdownmemberdata}
            dropdownroledata.push(role_data);            
        }

        const styles = reactCSS({
            'default': {
              color: {
                width: '36px',
                height: '14px',
                borderRadius: '2px',
                background: `rgba(${ this.state.pgcolorrgb.r }, ${ this.state.pgcolorrgb.g }, ${ this.state.pgcolorrgb.b }, ${ this.state.pgcolorrgb.a })`,
                //background:`${this.state.pgcolorrgb }`, 
              },
              swatch: {
                padding: '5px',
                background: '#fff',
                borderRadius: '1px',
                boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                display: 'inline-block',
                cursor: 'pointer',
              },
              popover: {
                position: 'absolute',
                zIndex: '2',
              },
              cover: {
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
              },
            },
          });


          
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Event</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li> 
                                    <li className="breadcrumb-item"><NavLink to="/events">Events</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Add Event</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-xl-6 col-md-12">
                        <form className='form-horizontal' onSubmit={this.EventAddBtn}>
                        <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Roles/Members</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                        <Container data={dropdownroledata} onChange={this.handleChangeMembers}    />
                                        </div>
                                    </div>                                
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Category</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                        <select className="form-control custom-select" name='category_id' onChange={this.handleChangeCategory} required >
                                            <option value="" hidden selected>Please Select Category</option>
                                            {
                                                this.state.categories.map((cat) => (
                                                    <option value={cat.category_id}>{cat.category_name}</option>
                                                ))
                                            }
                                        </select>
                                        </div>
                                    </div>                                
                                </div>
                            </div>    
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Title</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.event.event_title} name='event_title' onChange={this.handleChange}  />
                                        </div>
                                    </div>                                
                                </div>
                            </div>    

                            <div className="card">
                                
                                <div className="row">
                                <div className="col-md-6">
                                <div className="card-header pb-0">
        <h3 className="card-title">Start Date {this.state.selectedDate}</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <DatePicker                                                                                                                        
                                                className="form-control"
                                                selected={this.state.startDate}
                                                onChange={this.handleChangeStartDate}
                                                minDate={this.state.currentDate}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                //dateFormat="yyyy/MM/dd h:mm aa"
                                            />
                                        </div>
                                    </div>                                
                                </div>
                                </div>
                                <div className="col-md-6">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">End Date</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <DatePicker                                                                                                                        
                                                className="form-control"
                                                selected={this.state.endDate}
                                                onChange={this.handleChangeEndDate}
                                                minDate={this.state.startDate}

                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                //minTime={now.hours(now.hours()).minutes(now.minutes())}
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                 
                                            />

                                             

 
                                        </div>
                                    </div>                                
                                </div>
                                </div>
                                </div>

                                
                            </div>  


                            {/* <div className="card">
                                <div className="card-header pb-0">
                                <label class="checkbox-inline">
                                 
                                    <input type="checkbox"  className="check-box" name="recurring" value="1" onChange={this.handleActionRecurring} checked={this.state.event.recurring == 1 ? true : ''} defaultChecked={this.state.event.recurring == 1 ? true : ''} /> &nbsp; Repeat Event
                                    </label> 
                                </div>

                               {this.state.event.recurring ? ( <div className="card-body text-center">
                                    <select className="form-control custom-select" name='recurring_value' onChange={this.handleChangeRecurringValue}>
                                        <option selected={this.state.event.recurring_value == 'daily'} value="daily">Daily</option>
                                        <option selected={this.state.event.recurring_value == 'weekly'} value="weekly">Weekly</option>
                                        <option selected={this.state.event.recurring_value == 'monthly'} value="monthly">Monthly</option>
                                        <option selected={this.state.event.recurring_value == 'yearly'} value="yearly">Yearly</option>
                                    </select>                                                                                                                                               
                                </div>) : ''}
                            </div> */}


                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Description</h3>
                                </div>
                                <div className="card-body">
                                    <ReactSummernote
                                        value=""

                                        popover='false'
                                        options={{
                                            height: 150,
                                            toolbar: [
                                                ['style', ['style']],
                                                ['font', ['bold', 'underline', 'clear']],
                                                ['fontname', ['fontname']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['event', 'picture', 'video']],
                                                ['view', ['fullscreen', 'codeview']]
                                            ]
                                        }}
                                        onChange={this.handleSummerNote}
                                    />
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Color </h3>
                                </div>
                                <div className="card-body text-center">
                                    <div style={ styles.swatch } onClick={ this.handleClick }>
                                    <div style={ styles.color } />
                                    </div>
                                    { this.state.displayColorPicker ? <div style={ styles.popover }>
                                    <div style={ styles.cover } onClick={ this.handleClose }/>
                                    <SketchPicker color={ this.state.pgcolor } onChange={ this.handleChangeColor } />
                                    </div> : null }                                      
                                </div>
                            </div>


                            


                            <div className="card-footer text-right">                                
                                <button type="submit" className="btn btn-brand">Add</button>
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default EventAdd
