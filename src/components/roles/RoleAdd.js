import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class RoleAdd extends Component {

    state = {
        name: '',
        notes: '',
        program_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        role: {
            role_description: '',
            role_title: '',
            role_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const role_id = this.props.match.params.role_id;
            this.setState({ role_id: role_id });
            this.setState({ role: { role_id: role_id } });

            if(role_id)
            {
                axios({
                    url: API_URL + "admin/role/details/" + role_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ role: response.data.role, showAlertActionURL: '/program/category/role/details/' + this.state.role_id});
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            }


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }
 
    handleChangetask = (e) => {
        this.setState({ role: { ...this.state.role, role_title: e.target.value } });
    }
 

    //add task
    RoleAddBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.role.role_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter role title." });
                return false;

            }

            if ((this.state.role.role_title).trim().length ==0) {
                this.setState({ role: { ...this.state.role, role_title: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid role title." });
                return false;
            }

    
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/role/add",
                method: 'post',
                data: this.state.role,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success 
                        const role_id = response.data.role.role_id;
                        if(role_id)
                        {
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/role/details/' + role_id });
                        }
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Add Role</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li> 
                                    <li className="breadcrumb-item"><NavLink to="/roles">Role List</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Add Role</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div className="row">
                    <div className="col-xl-4 col-md-12">
                        <form className='form-horizontal' onSubmit={this.RoleAddBtn}>
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Role Name</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.role.role_title} name='role_title' onChange={this.handleChangetask} required />
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                             
                             
                            <div className="card-footer text-right">                                
                                <button type="submit" className="btn btn-brand">Add</button>
                            </div>
                        </form>


                    </div>

                </div>
            </Fragment>

        )
    }
}

export default RoleAdd
