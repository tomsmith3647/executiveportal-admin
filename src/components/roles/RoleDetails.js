import React, { Component, Fragment } from 'react'
import SectionBodyWrapper from '../auxiliary/SectionBodyWrapper';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'bootstrap/js/src/modal';

import 'bootstrap/js/src/tooltip';
import { NavLink } from 'react-router-dom'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

import { Button, Form, FormGroup, Label, Input, Progress } from 'reactstrap';


const $ = window.$
class RoleDetails extends Component {

    state = {
        name: '',
        notes: '',
        role_id: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        role: {
            role_description: '',
            role_title: '',
            role_video: ''

        },

        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        selected_thumnail: '',
        selected_video: '',
        data_default_file: '/assets/images/gallery/1.jpg',
        video_size: 6000, //20MB
        image_size: 2, //2MB
        fileName: 'Choose file',
        processBarPercent: 0,
        video_path: '',
        fileUploading:false,
        role_permissions:[],
        role_role_permissions:[],
        members:[],
    }

    componentDidMount() {

        $('.dropify').dropify();

        $('#activetask').DataTable({
            'columnDefs': [{
                'orderable': false, // set orderable false for selected columns
            }]
        });




        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })


            const role_id = this.props.match.params.role_id;
            this.setState({ role_id: role_id });
            this.setState({ role: { role_id: role_id } });

            if(role_id)
            {
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "admin/role/details/" + role_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ role: response.data.role});
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });

                axios({
                    url: API_URL + "admin/role/permission/list/" + role_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                           
                        this.setState({ role_permissions: response.data.role_permissions});        
                           
                        var permissiontbl = $('#permissiontbl').DataTable({

                            'columnDefs': [{
                                'targets': [2,3], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            "pageLength": 25,
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });
    
                        permissiontbl.columns().iterator('column', function (ctx, idx) {
                            $(permissiontbl.column(idx).header()).append('<span class="sort-icon"/>');
                        });             
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            }


            this.setState({ showLoader: true, showUsersTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/role/member/list",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message; //
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });

                if (error_code == '200') {
                    //success                        
                    this.setState({ members: response.data.members });

                    var userTable = $('#pusersTable').DataTable({

                        'columnDefs': [{
                            'targets': [1], // column index (start from 0)
                            'orderable': false, // set orderable false for selected columns
                        }],
                        "pageLength": 25,
                        'lengthMenu': [10, 25, 50, 100, 500, 1000],
                    });

                    userTable.columns().iterator('column', function (ctx, idx) {
                        $(userTable.column(idx).header()).append('<span class="sort-icon"/>');
                    });

                }
                else {
                    //fail
                    this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }
 
    handleChangetask = (e) => {
        this.setState({ role: { ...this.state.role, role_title: e.target.value } });
    }

    
    handleChangeStatus = (e) => {
        this.setState({ role: { ...this.state.role, status: e.target.value } });
    }
 

    //add task
    RoleUpdateBtn = (event) => {
        event.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            if (!this.state.role.role_title) {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter role title." });
                return false;

            }

            if ((this.state.role.role_title).trim().length ==0) {
                this.setState({ role: { ...this.state.role, role_title: '' } });
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid role title." });
                return false;
            }

            this.setState({ role: { ...this.state.role, status: '1' } });
  
            var A = this;
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/role/update",
                method: 'post',
                data: this.state.role,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {

                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success 
                        
                    this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message,  showAlertActionURL: '/roles'});
                    
                }
                else {
                    //fail
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
        }
        //Get user permissions

    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    handlePermission = (e) => {
        //PerMission
        e.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
        }
        
        const isChecked = e.target.checked ? 1 : 0;
        //alert(e.target.name +" - "+isChecked);
        var status = 0;
        if (e.target.checked) {
             status = 1;            
        }
         
        
        var A = this;
        var postData = {permission:e.target.name, status:status, role_id:this.state.role_id };

        this.setState({ showLoader: true });
        axios({
            url: API_URL + "admin/role/permission/update",
            method: 'post',
            data: postData,
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {

            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
            this.setState({ showLoader: false });
            if (error_code == '200') {
                //success 
                    
                this.setState({role_permissions: response.data.role_permissions, showAlertIcon: 'success', showAlert: false, showAlertMessage: error_message,  showAlertActionURL: '/role/details/' + this.state.role_id });
                //window.location.reload();
                
            }
            else {
                //fail
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
            }
        })
        .catch(err => {
            //error
            console.log(err);
        });
    }



    handlePermission22222 = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        if (e.target.checked) {


            // this.setState({
            //     ...this.state,
            //     valid_permission: [...this.state.valid_permission, test]
            // })

            if (this.state.valid_permission.length > 0) {
                if (this.state.valid_permission) {
                    var rrrr = [];
                    this.state.valid_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 1;
                            rrrr.push(item);
                            this.setState({ valid_permission: rrrr })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }
                    })
                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }




        }
        else {
            if (this.state.valid_permission.length > 0) {
                if (this.state.valid_permission) {
                    var eee = [];
                    this.state.valid_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 0;
                            eee.push(item);
                            this.setState({ valid_permission: eee })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }
                    })
                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }
        }
        console.log(this.state.valid_permission);
    }
    

    
    //handleMemberProgramMapping
    handleMemberProgramMapping = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        const status = isChecked;
        var role_id = this.state.role.role_id;                 
        if(!isChecked)
        {
            role_id = 0;
        }

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });             
            axios({
                url: API_URL + "admin/role/member/mapping/add",
                method: 'post',
                data: {
                    role_id: role_id,
                    isChecked: isChecked,    
                    member_id: e.target.value,                     
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            
                        
                        this.setState({ showLoader: true, showUsersTableMessage: 'Please wait...' });
                        axios({
                            url: API_URL + "admin/member/list",
                            method: 'get',
                            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                        })
                        .then(response => {
                            const error_code = response.data.error_code;
                            const error_message = response.data.error_message; //
                            const ApiURL = response.data.api_url;
                            this.setState({ baseApiURL: ApiURL });

                            if (error_code == '200') {
                                //success                        
                                this.setState({ members: response.data.members });                                
                            }
                            else {
                                //fail
                                this.setState({ showAlert: true, showAlertMessage: error_message, showUsersTableMessage: error_message });
                            }
                        })
                        .catch(err => {
                            console.log(err);
                        });


                        
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
    }



    render() {
        return (

            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Role Details</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>    
                                    <li className="breadcrumb-item"><NavLink to="/roles">Role List</NavLink></li>                                    
                                    <li className="breadcrumb-item active" aria-current="page">Role Details</li>


                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
                <br />

                <form className='form-horizontal' onSubmit={this.RoleUpdateBtn}> 
                <div className="row">
                
                
                    <div className="col-xl-12 col-md-12">
                        
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Role Name ({this.state.role.role_id})</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" value={this.state.role.role_title} name='role_title' onChange={this.handleChangetask} required />
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                             
                            <div className="card">
                                <div className="card-header pb-0">
                                    <h3 className="card-title">Status</h3>
                                </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <select className="form-control custom-select" name='status' onChange={this.handleChangeStatus}>
                                                <option selected={this.state.role.status == 1} value="1">Active</option>
                                                <option selected={this.state.role.status == 0} value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             
                            
                       



                        


                    </div>


                    <div className="col-xl-7 col-md-7">

                    <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Members</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table id='pusersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class='text-center'>Action</th>
                                                <th class='text-left'>Name</th>
                                                <th class='text-left'>Email</th>
                                                <th class='text-left'>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.members.map((member, index) => (
                                                    <tr>
                                                        <td>{index + 1} </td>
                                                        <td class='text-center'>
                                                            <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                <input type="checkbox" class="custom-control-input" name="member_id" value={member.member_id} onChange={this.handleMemberProgramMapping} checked={member.role_id == this.state.role_id ? true : ''} defaultChecked={member.role_id == this.state.role_id ? true : ''}  />               <span class="custom-control-label">&nbsp;</span>
                                                            </label>
                                                        </td>
                                                        <td class='text-left table_content'><NavLink to={`/user/detail/${member.member_id}`} className="btn btn-icon btn-sm text-info brandColor text-left" title="Member">{member.first_name} {member.last_name}</NavLink></td>
                                                        <td class='text-left'>{member.email}</td>
                                                        <td class='text-left'>
                                                            {member.status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        {this.state.members.length == 0 && (<tfoot><tr><td colspan="5" className="text-center text-capitalize">{this.state.showUsersTableMessage}</td></tr></tfoot>)}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-5 col-md-5">                                            
                    <div className="card">
                            <div className="row ">
                                <div className="col">
                                    <div className="card-header pb-0">
                                        <h3 className="card-title">Permissions</h3>
                                    </div>
                                </div>                                 
                            </div>
                            <div className="card-body">
                                 
                                <div className="table-responsive">
                                <table id='permissiontbl' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Permissions</th>
                                            <th className='text-center'>Read</th>
                                             <th className='text-center'>Write</th>
                                            {/*<th className='text-center'>Update</th> */}
                                            {/* <th className='text-center'>Status</th>                                         */}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.role_permissions.map((RolePer, index) => (
                                                <Fragment>
                                                    {RolePer ? 
                                                (<tr key={index}>
                                                    <td>{index + 1}</td>
                                                <td className="table_content">{RolePer ? RolePer.permission_title: ''} {RolePer.role_type == 'program' ? '('+RolePer.permission_id+')' : ''} </td>
                                                    <td className='text-center'>
                                                        <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" className="custom-control-input" name={RolePer.permission_id + '_r'} value={RolePer.r} onChange={this.handlePermission} checked={RolePer.r == 1 ? true : ''} defaultChecked={RolePer.r == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                        </label>
                                                    </td>

                                                     <td className='text-center'>
                                                        {RolePer.w != '3' && RolePer.role_type == 'member' ? (
                                                            <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                <input type="checkbox" className="custom-control-input" name={RolePer.permission_id + '_w'} value={RolePer.w} onChange={this.handlePermission} checked={RolePer.w == 1 ? true : ''} defaultChecked={RolePer.w == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                            </label>
                                                        ) : ''}
                                                        
                                                    </td>                                                    
                                                </tr>) : ''} </Fragment>
                                            ))
                                        }                                                                                  
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="col-xl-12 col-md-12">

                        <div className="card">
                                 
                                <div className="card-body">
                                    
                                    <div className="float-right">                                
                                    <button type="submit" className="btn btn-brand">Update</button>
                                    </div>
                                    </div>                                
                                 
                            </div>
                        
                    </div>
                   
                    
                </div>

                </form>
                
            </Fragment>

        )
    }
}

export default RoleDetails
