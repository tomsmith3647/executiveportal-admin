import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class RoleList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        roles: [],
        showAlert: false,
        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
        showProgaramTableMessage: '',
        selected_role_id: '',
        access_permission:false,
        access_permission_r:false,
        access_permission_w:false,
        access_permission_u:false,
    }

    componentDidMount() {
        this.setState({ showLoader: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true, showProgaramTableMessage: 'Please wait...' });
            axios({
                url: API_URL + "admin/role/list",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    const ApiURL = response.data.api_url;
                    this.setState({ baseApiURL: ApiURL });
                    this.setState({ showLoader: false });
                    this.setState({ access_permission_r: response.data.access_permission_r });
                    this.setState({ access_permission_w: response.data.access_permission_w });
                    this.setState({ access_permission_u: response.data.access_permission_u });
                    if (error_code == '309') {
                        this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                    else if (error_code == '200') {
                        //success                        
                        this.setState({ roles: response.data.roles });


                        var roleTable = $('#usersTable').DataTable({
                            'rowReorder': true,
                            'columnDefs': [{
                                'targets': [6], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        roleTable.columns().iterator('column', function (ctx, idx) {
                            $(roleTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });

                        roleTable.on('row-reorder', function (e, diff, edit) {
                            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
                            var sorting_order = [];
                            for (var i = 0, ien = diff.length; i < ien; i++) {
                                const rowData = roleTable.row(diff[i].node).data();
                                const role_id = $(rowData[1]).attr("data-id");
                                const catsort = { "role_id": role_id, "position": diff[i].newData };
                                sorting_order.push(catsort);
                            }
                            //update
                            axios({
                                url: API_URL + "admin/role/position/update",
                                method: 'post',
                                data: { sorting_order: sorting_order },
                                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                            })
                                .then(response => {
                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success                                         
                                    }
                                    else {
                                        //fail
                                        //this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                        });
                    }
                    else {
                        //fail
                        this.setState({ showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                    }
                })
                .catch(err => {
                    console.log(err);
                });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }
    }


    //roleDelete    
    roleDelete = () => (e) => {
        const role_id = e.currentTarget.value;
        this.setState({ selected_role_id: role_id });
        this.setState({ showConfirmationAlert: true });
    }

    //roleDeleteProcess
    roleDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const role_id = this.state.selected_role_id;

            axios({
                url: API_URL + "admin/role/delete",
                method: 'post',
                data: {
                    role_id: role_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            role/category/module/details         
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/roles' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.roleDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="d-flex justify-content-between align-items-center">
                    <div className="header-action">
                        <h1 className="page-title">Roles</h1>
                        <ol className="breadcrumb page-breadcrumb">
                            <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                            <li className="breadcrumb-item active" aria-current="page">Role List</li>
                        </ol>
                    </div>
                </div>
                <br />
                {this.state.access_permission_r && (
                <div className="tab-pane" id="Holiday-all">

{this.state.access_permission_w && (<div className="card-footer text-right">
                        <NavLink to="/role/create" className="btn btn-brand">Add Role</NavLink>
                    </div>)}

                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                    <thead>
                                        <tr>
                                            <th >#</th>
                                            <th className='text-left' >Role Id</th>
                                            <th className='text-left' >Role Title</th>
                                            {/* <th className='text-left'>Status</th>                                             */}
                                            <th className='text-left' >Created Date</th>
                                            <th className='text-left' >Last Activity</th>
                                            {this.state.access_permission_u && (<th className='text-center' >Action</th> )}

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.roles.map((role, index) => (
                                                <tr>
                                                    <td className='reorder text-left'>{index + 1}</td>
                                                    <td className="text-left text-capitalize">
                                                        <NavLink data-id={role.role_id} to={`/role/details/${role.role_id}`} className="text-info brandColor" title="View Detail">
                                                            {role.role_id}
                                                        </NavLink>
                                                    </td>
                                                    <td className="text-left text-capitalize table_content">
                                                        {role.role_title}
                                                    </td>
                                                    {/* <td className='text-left'>
                                                        {role.status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                    </td> */}
                                                     
                                                    <td className='text-left'><span className='dateFormat'>{role.created_date}</span>{(new Date(this.convertUTCDateToLocalDate(new Date(role.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}</td>
                                                    <td className='text-left'><span className='dateFormat'>{role.modified_date}</span>{(new Date(this.convertUTCDateToLocalDate(new Date(role.modified_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}</td>

                                                    {this.state.access_permission_u && (<td className='text-center'>
                                                        
                                                        
                                                        <button value={role.role_id} onClick={this.roleDelete()} className="btn btn-icon btn-sm " title="delete"><i className="fa fa-trash text-danger"></i></button>
                                                        
                                                    </td>)}
                                                     
                                                </tr>
                                            ))
                                        }

                                    </tbody>
                                    {this.state.roles.length == 0 && (<tfoot><tr><td colspan="8" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'No role'}</td></tr></tfoot>)}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>)}
            </Fragment>
        )
    }
}

export default RoleList
