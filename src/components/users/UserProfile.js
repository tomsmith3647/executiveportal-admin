import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import { NavLink } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

// Variable
const $ = window.$

export class UserProfile extends Component {


    constructor(props) {
        super(props);

        this.state = { member: [] };

        this.state = {
            member: [{ first_name: "", last_name: "" }]
        };

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            linkeinProfile: '',
            address: '',
            city: '',
            state_code: '',
            country_code: 'USA',
            countries: [],
            states: [],
            businessWebsite: '',
            businessDescription: '',
            showAlert: false,
            showAlertMessage: '',
            member: {},
            member: { profile_image: '' },
            member_permissions: [],
            member_program_permissions: [],
            valid_permission: [],
            valid_program_permission: [],
            selected_permission: {},
            crm_id: '',
            showAlertIcon: '',
            showAlert: false,
            showConfirmationAlert: false,
            showAlertActionURL: '',
            showLoader: false,
            reviews: [],
            booms: [],
            refers: [],
            member_id: '',
            roles:[],
            role_id:'',
            showConfirmationAlertBoom:false,
            boom_id:''

        }
    }






    componentDidMount() {
        // $('.dropify').dropify();

        this.table('salesTable')
        this.table('tableReview')
        this.table('tableReferral')


        //get user detail from memeber id
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const member_id = this.props.match.params.member_id;

            if (member_id) {
                //Get user profile details   
                this.setState({ member_id: member_id, showLoader: true });
                axios({
                    url: API_URL + "admin/member/detail/" + member_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        const ApiURL = response.data.api_url;
                        this.setState({ showLoader: false });
                        this.setState({ baseApiURL: ApiURL });
                        if (error_code == '200') {
                            //success

                            delete response.data.member.modified_date;
                            delete response.data.member.created_date;


                            this.setState({ member: response.data.member });
                            $('.dropify').dropify();

                            const default_country_code = this.state.member.country_code;
                            if (default_country_code) {
                                this.setState({ showLoader: true });

                                //get state List
                                axios({
                                    url: API_URL + "state/list/" + default_country_code,
                                    method: 'get',
                                })
                                    .then(response => {
                                        const error_code = response.data.error_code;
                                        const error_message = response.data.error_message;

                                        this.setState({ showLoader: false });
                                        if (error_code == '200') {
                                            //success                        
                                            this.setState({ states: response.data.states });
                                        }
                                        else {
                                            //fail
                                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                        }
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                            }

                            if (this.state.member.role_id)
                            {
                                 //Get user permissions
                                this.setState({ showLoader: true });
                                axios({
                                    url: API_URL + "admin/role/permission/list/" + this.state.member.role_id,
                                    method: 'get',
                                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                                })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success
                                        this.setState({ member_permissions: response.data.role_permissions });
                                        
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    console.log(err);
                                });
                            }
                        }
                        else {
                            //fail
                            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });


                // //Get user permissions
                // this.setState({ showLoader: true });
                // axios({
                //     url: API_URL + "admin/member/permissions/" + member_id,
                //     method: 'get',
                //     headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                // })
                // .then(response => {

                //     const error_code = response.data.error_code;
                //     const error_message = response.data.error_message;
                //     this.setState({ showLoader: false });
                //     if (error_code == '200') {
                //         //success
                //         this.setState({ member_permissions: response.data.member_permissions });
                //         this.setState({ member_program_permissions: response.data.member_program_permissions });
                //     }
                //     else {
                //         //fail
                //         this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                //     }
                // })
                // .catch(err => {
                //     //error
                //     console.log(err);
                // });


               



                //get review List
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "admin/member/review/list/" + member_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success                        
                            this.setState({ reviews: response.data.reviews });
                        }
                        else {
                            //fail
                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });


                //get boom List
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "admin/member/boom/list/" + member_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success                        
                            this.setState({ booms: response.data.booms });
                            this.setState({ total_boom: response.data.total_boom });

                        }
                        else {
                            //fail
                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });


                //get refer List
                this.setState({ showLoader: true });
                axios({
                    url: API_URL + "admin/member/refer/list/" + member_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success                        
                            this.setState({ refers: response.data.refers });
                        }
                        else {
                            //fail
                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });

            }
            else {
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Member id empty!" });
            }


            this.setState({ showLoader: true });
            //get state List
            axios({
                url: API_URL + "admin/role/list",
                method: 'get',
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;

                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ roles: response.data.roles });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }



        //get country List
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "country/list",
            method: 'get',
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ countries: response.data.countries });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });



    }



    table = (id) => {
        var table = $(`#${id}`).DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    },
                    render: function (datad) {
                        //return '<input type="checkbox" name="selectcompanyid">';
                        return '<label><input type="checkbox" name="test[]"></label>';
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'order': [[1, 'asc']]
        });
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })

    }





    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    handlePermission = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        if (e.target.checked) {


            // this.setState({
            //     ...this.state,
            //     valid_permission: [...this.state.valid_permission, test]
            // })

            if (this.state.valid_permission.length > 0) {
                if (this.state.valid_permission) {
                    var rrrr = [];
                    this.state.valid_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 1;
                            rrrr.push(item);
                            this.setState({ valid_permission: rrrr })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }
                    })

                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }




        }
        else {


            // const test = {
            //     'role': e.target.name,
            //     'status': isChecked
            // }


            // var joined = this.state.valid_permission.concat(test);
            // this.setState({ valid_permission: joined })


            if (this.state.valid_permission.length > 0) {
                if (this.state.valid_permission) {
                    var eee = [];
                    this.state.valid_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 0;
                            eee.push(item);
                            this.setState({ valid_permission: eee })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }
                    })

                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }




            //  const changeDesc = (key) => {
            //     for (var i in this.state.valid_permission) {
            //         if (this.state.valid_permission[i].role == key) {
            //             this.state.valid_permission[i].status = 0;
            //             break; //Stop this loop, we found it!
            //         }
            //     }
            // }


            // const selectedPermission = {
            //     'role': e.target.name,
            //     'status': isChecked
            // }

            // this.setState({
            //     ...this.state,
            //     valid_permission: [...this.state.valid_permission, selectedPermission]
            // })

        }
        console.log(this.state.valid_permission);
    }

    //handleProgramPermission
    handleProgramPermission = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        if (e.target.checked) {

            if (this.state.valid_program_permission.length > 0) {
                if (this.state.valid_program_permission) {
                    var rrrr = [];
                    this.state.valid_program_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 1;
                            rrrr.push(item);
                            this.setState({ valid_program_permission: rrrr })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_program_permission.concat(test);
                            this.setState({ valid_program_permission: joined })
                        }
                    })

                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_program_permission.concat(test);
                this.setState({ valid_program_permission: joined })
            }
        }
        else {
            if (this.state.valid_program_permission.length > 0) {
                if (this.state.valid_program_permission) {
                    var eee = [];
                    this.state.valid_program_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 0;
                            eee.push(item);
                            this.setState({ valid_program_permission: eee })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_program_permission.concat(test);
                            this.setState({ valid_program_permission: joined })
                        }
                    })

                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_program_permission.concat(test);
                this.setState({ valid_program_permission: joined })
            }
        }
        console.log(this.state.valid_program_permission);
    }

    updateUserPermissoin = () => {

        console.log(this.state.valid_permission);
        console.log(this.state.valid_program_permission);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const member_id = this.props.match.params.member_id;
            if (member_id) {
                const selected_permissions = this.state.valid_permission;
                const selected_program_permissions = this.state.valid_program_permission;
                axios({
                    url: API_URL + "admin/member/permission/edit/",
                    method: 'post',
                    data: {
                        member_id: member_id,
                        permissions: selected_permissions,
                        program_permissions: selected_program_permissions
                    },
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        if (error_code == '200') {
                            //success                        
                            this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: 'refresh' });

                        }
                        else {
                            //fail
                            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });
            }
            else {
                this.setState({ showAlert: true, showAlertMessage: "Member id empty!" });
            }

        }
        //Get user permissions
    }

    //End handleProgramPermission


    handleCountryChange = (e) => {



        const country_code = e.target.value

        if (!country_code) {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select country" });
            return false;
        }


        this.setState(
            { member: { ...this.state.member, country_code: country_code } }
        )

        //get state List
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "state/list/" + country_code,
            method: 'get',
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;

                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ states: response.data.states });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                console.log(err);
            });


        console.log(this.state.countries);
    }

    validateUrl = (url) => {
        var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        const result = urlregex.test(url);
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    checkUrl = (e) => {
        const url = e.target.value;
        if (this.validateUrl(url)) {
            return true;
        }
        else {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
            return false;
        }
    }

    handleChangeProfile = (e) => {

        const contact = this.state.member;
        contact[e.target.name] = e.target.value;
        this.setState({ member: contact });
    }

    ValidateEmail = (mail) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return true
        }

        return false
    }

    //update profile
    updateUserProfile = (e) => {
        e.preventDefault()
        console.log(this.state.member);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,

                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const words = ['linkedin.com'];
            const regex = new RegExp(words.join('|'));

            if (!this.ValidateEmail(this.state.member.email)) {
                e.preventDefault()
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid email address." });
                return false;
            }

            if(!this.state.member.linkedin_profile && !this.state.member.business_website)            
            {
                e.preventDefault()
                this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please provide your LinkedIn Profile or Business website." });
                return false;
            }

            if(this.state.member.linkedin_profile)
            {
                if (!this.validateUrl(this.state.member.linkedin_profile)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                    return false;
                }
                else if (!regex.test(this.state.member.linkedin_profile)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                    return false;
                }
            }
            
            if(this.state.member.business_website)
            {
                if (!this.validateUrl(this.state.member.business_website)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid Business Website URL." });
                    return false;
                }
            }
            

            axios({
                url: API_URL + "admin/member/profile/update",
                method: 'post',
                data: this.state.member,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success     
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/users' });

                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions

    }


    handleImage = (event) => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const member_id = this.props.match.params.member_id;
            if (!event.target.files[0]) {
                this.setState({ showAlert: true, showAlertMessage: "Please choose profile image" });
            }
            else {
                this.setState({
                    showLoader: true,
                })
                const imagedata = new FormData()
                imagedata.append('profileimage', event.target.files[0]);
                imagedata.append('member_id', member_id);
                axios({
                    url: API_URL + "admin/member/profile/image/upload",
                    method: 'post',
                    data: imagedata,
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        if (error_code == '200') {
                            //success                        
                            this.setState({ showLoader: false, showAlert: true, showAlertMessage: error_message });
                            //window.location.href = '/dashboard'
                        }
                        else {
                            //fail
                            this.setState({ showLoader: false, showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        console.log(err);
                    });

            }
        }
    }


    //resetPassword
    //update profile
    resetPassword = () => {
        console.log(this.state.member);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const member_id = this.props.match.params.member_id;
            axios({
                url: API_URL + "admin/member/password/reset",
                method: 'post',
                data: this.state.member,
                data: {
                    member_id: member_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success                        
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/users' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });
        }
        //Get user permissions
    }
    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            if (this.state.showAlertActionURL == 'refresh') {
                window.location.reload();
            }
            else {
                window.location.href = this.state.showAlertActionURL; // '/dashboard';
            }

        }

    }

    checkValidZipCode = (evt) => {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            evt.preventDefault();
            return false;
        }
        else {
            if (iKeyCode == 46) {
                evt.preventDefault();
                return false;
            }
            else {
                if (evt.target.value.length > 7) {
                    evt.preventDefault();
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }

    boomDelete = () => (e) => {
        const boom_id = e.currentTarget.value;
        this.setState({ selected_boom_id: boom_id });
        this.setState({ showConfirmationAlertBoom: true });
    }

    //subAdminDelete
    boomDeleteProcess = () => {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlertBoom: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const boom_id = this.state.selected_boom_id;
            const member_id = this.props.match.params.member_id;
            axios({
                url: API_URL + "admin/member/boom/delete",
                method: 'post',
                data: {
                    boom_id: boom_id,
                    member_id: member_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false, showLoader: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success            
                        this.setState({ booms: response.data.booms });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
        //Get user permissions
    }

    //handleReviewPublish
    handleReviewPublish = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        const status = isChecked;

        const member_id = this.props.match.params.member_id;
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/review/publish",
                method: 'post',
                data: {
                    status: isChecked,
                    review_id: e.target.value,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success            


                        //get review List
                        this.setState({ showLoader: true });
                        axios({
                            url: API_URL + "admin/member/review/list/" + member_id,
                            method: 'get',
                            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                        })
                            .then(response => {
                                const error_code = response.data.error_code;
                                const error_message = response.data.error_message;
                                this.setState({ showLoader: false });
                                if (error_code == '200') {
                                    //success                        
                                    this.setState({ reviews: response.data.reviews });
                                }
                                else {
                                    //fail
                                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });

                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    console.log(err);
                });


        }
    }

    render() {
        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };
        return (

            <Fragment>
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.boomDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlertBoom: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlertBoom}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Member Profile</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/users">Members</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Profile</li>
                                </ol>
                            </div>
                            <ul className="nav nav-tabs page-header-tab" role="tablist">
                                <li className="nav-item"><a className="nav-link active" data-toggle="tab" href="#profile" aria-expanded="true">Profile</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#permission" aria-expanded="true">Permissions</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#sales" aria-expanded="true">Sales</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#reviews" aria-expanded="true">Reviews</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#referrals" aria-expanded="true">Referrals</a></li>
                                <li className="nav-item"><button type="button" onClick={this.resetPassword} className="btn btn-brand">Reset password</button></li>
                                {/* <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#sales" aria-expanded="false">Sales</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#reviews" aria-expanded="false">Reviews</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#referrals" aria-expanded="false">Referrals</a></li> */}
                            </ul>
                        </div>
                    </div>

                </div>
                <br />
                <div className='user-profile white-container'>
                    <div className="row">
                        <div className="col-md-3">

                            <input type="file" name="file" onChange={this.handleImage} data-height="200" className="dropify profileImage" data-default-file={this.state.member.profile_image ? this.state.baseApiURL + this.state.member.profile_image : '/assets/images/placeholder-2.png'} />

                        </div>
                        <div className="col-md-9">

                            <div className="tab-content mt-3">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.updateUserProfile}>
                                        <p className='page-title mb-3'>User Profile </p>
                                        <div className="card-body">

                                        <div className="form-group required row">
                                                <label className="col-md-3 col-form-label">Role </label>
                                                <div className="col-md-9">
                                                    <select className="form-control custom-select" name='role_id' onChange={this.handleChangeProfile} required  >
                                                        <option value="" hidden selected>Please Select Role</option>
                                                        {                                                             
                                                            this.state.roles.map((role) => (
                                                                role.role_id == this.state.member.role_id ? (<option selected value={role.role_id}>({role.role_id}) {role.role_title}</option>) : (<option value={role.role_id}>({role.role_id}) {role.role_title}</option>)
                                                            ))
                                                        }
                                                    </select>

                                                    {/* {                                                             
                                                        this.state.roles.map((role) => (
                                                            role.role_id == this.state.member.role_id ? (<span className="form-control" >{'('+role.role_id+') '+ role.role_title}</span>) : ''
                                                        ))
                                                    } */}
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">CRM ID</label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" value={this.state.member.crm_id} name='crm_id' onChange={this.handleChangeProfile} />
                                                </div>
                                            </div>

                                            <div className="form-group required row">
                                                <label className="col-md-3 col-form-label">First Name </label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" value={this.state.member.first_name} name='first_name' onChange={this.handleChangeProfile} required />
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Last Name </label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" value={this.state.member.last_name} name='last_name' onChange={this.handleChangeProfile} />
                                                </div>
                                            </div>
                                            <div className="form-group required row">
                                                <label className="col-md-3 col-form-label">Email Address </label>
                                                <div className="col-md-9">
                                                    <input type="text" readonly="readonly" className="form-control" value={this.state.member.email} name='email' onChange={this.handleChangeProfile} required />
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">LinkedIn Profile </label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" value={this.state.member.linkedin_profile}   name='linkedin_profile' onChange={this.handleChangeProfile}  />
                                                    <br />
                                                    {this.state.member.linkedin_profile ? <a href={this.state.member.linkedin_profile} title='Linkedin Profile' target="_blank">View Linkedin Profile</a> : ''}
                                                </div>
                                            </div>
                                            <br />
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Address </label>
                                                <div className="col-md-9">
                                                    <textarea name="address" id="address" className="form-control" value={this.state.member.address} name='address' onChange={this.handleChangeProfile}></textarea>
                                                </div>
                                            </div>
                                            <div className="form-group required row">
                                                <label className="col-md-3 col-form-label">City </label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" value={this.state.member.city} name='city' onChange={this.handleChangeProfile} required />
                                                </div>
                                            </div>
                                            <div className="form-group required row">
                                                <label className="col-md-3 col-form-label">Country </label>
                                                <div className="col-md-9">
                                                    <select className="form-control custom-select" name='country_code' onChange={this.handleCountryChange} required >
                                                        <option value="" hidden selected>Please Select Country</option>
                                                        {
                                                            this.state.countries.map((cntry) => (
                                                                cntry.country_code == this.state.member.country_code ? (<option selected value={cntry.country_code}>{cntry.country_name}</option>) : (<option value={cntry.country_code}>{cntry.country_name}</option>)
                                                            ))
                                                        }
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-group required row">
                                                <label className="col-md-3 col-form-label">State / Province </label>
                                                <div className="col-md-9">
                                                    <select className="form-control custom-select" name='state_code' onChange={this.handleChangeProfile} required >
                                                        <option value="" hidden selected>Please Select State</option>
                                                        {
                                                            this.state.states.map((state) => (
                                                                state.state_code == this.state.member.state_code ? (<option selected value={state.state_code}>{state.state_name}</option>) : (<option value={state.state_code}>{state.state_name}</option>)
                                                            ))
                                                        }
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-group required row">
                                                <label className="col-md-3 col-form-label">Zip Code</label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" value={this.state.member.zip_code} name='zip_code' onKeyPress={this.checkValidZipCode} onChange={this.handleChangeProfile} required />
                                                </div>
                                            </div>
                                            <div className="form-group  row">
                                                <label className="col-md-3 col-form-label">Business Website</label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" value={this.state.member.business_website} name='business_website'  onChange={this.handleChangeProfile}  />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Business Description </label>
                                                <div className="col-md-9">
                                                    <textarea id="businessDescription" className="form-control" value={this.state.member.business_description} name='business_description' onChange={this.handleChangeProfile}></textarea>
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-md-3 col-form-label">Status</label>
                                                <div className="col-md-9">
                                                    <select className="form-control custom-select" name='status' onChange={this.handleChangeProfile}>
                                                        <option selected={this.state.member.status == 1} value="1">Active</option>
                                                        <option selected={this.state.member.status == 0} value="0">Inactive</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="card-footer text-right">
                                            <button type="submit" className="btn btn-brand">Update Profile</button>
                                        </div>

                                    </form>

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="permission" aria-expanded="false">

                                    <div className="row">
                                        <div className="col-sm-12">

                                            <p className='page-title mb-3'>User Permissions </p>
                                            <div className="card" style={{ padding: '20px 20px' }}>
                                                <form>
                                                    <div className="form-group row">
                                                        <div className="table-responsive">
                                                            <table className="table table-hover table-vcenter text-nowrap    table-striped  border-style spacing5">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Permissions</th>
                                                                        <th className='text-center'>Read</th>
                                                                         <th className='text-center'>Write</th>
                                                                       {/* <th className='text-center'>Update</th> */}
                                                                        {/* <th className='text-center'>Status</th>                                         */}
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {
                                                                        this.state.member_permissions.map((memberPer, index) => (
                                                                            <Fragment>
                                                                                {memberPer ? 
                                                                            (<tr key={index}>
                                                                                <td>{index + 1}</td>
                                                                                <td>{memberPer ? memberPer.permission_title: ''}</td>
                                                                                <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberPer.permission_id + '_r'} value={memberPer.r} onChange={this.handlePermission} defaultChecked={memberPer.r == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                <td className='text-center'>
                                                                                {memberPer.w != '3' && memberPer.role_type == 'member' ? (
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberPer.permission_id + '_w'} value={memberPer.w} onChange={this.handlePermission} defaultChecked={memberPer.w == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label> ) : '' }
                                                                                </td>

                                                                                 {/*<td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" className="custom-control-input" name={memberPer.permission_id + '_u'} value={memberPer.u} onChange={this.handlePermission} defaultChecked={memberPer.u == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td> */}


                                                                            </tr>) : ''} </Fragment>
                                                                        ))
                                                                    }
                                                                    {/* {

                                                                        this.state.member_program_permissions.map((memberProPer, indexpm) => (
                                                                            <tr key={indexpm}>
                                                                                <td>{indexpm + 1 + this.state.member_permissions.length}</td>
                                                                                <td>{memberProPer.program_detail.program_title} ({memberProPer.program_detail.program_code})</td>
                                                                                <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberProPer.permission_id + '_r'} value={memberProPer.r} onChange={this.handleProgramPermission} defaultChecked={memberProPer.r == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberProPer.permission_id + '_w'} value={memberProPer.w} onChange={this.handleProgramPermission} defaultChecked={memberProPer.w == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                 <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberProPer.permission_id + '_u'} value={memberProPer.u} onChange={this.handleProgramPermission} defaultChecked={memberProPer.u == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>  

                                                                            </tr>
                                                                        ))
                                                                    } */}

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>




                                                    {/* <div className="form-group row">
                            <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                <input type="checkbox" className="custom-control-input" name="checkPermission" value="option1" onChange={this.handlePermission} />
                                <span className="custom-control-label">&nbsp;</span>
                                View / Submit BOOM
                         </label>
                        </div>

                        <div className="form-group row">
                            <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                <input type="checkbox" className="custom-control-input" name="example-checkbox2" value="option2" onChange={this.handlePermission} />
                                <span className="custom-control-label">&nbsp;</span>
                                View Love
                        </label>
                        </div>

                        <div className="form-group row">
                            <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                <input type="checkbox" className="custom-control-input" name="example-checkbox3" value="option3" />
                                <span className="custom-control-label">&nbsp;</span>
                                Refer A Friend
                        </label>
                        </div> */}

                                                </form>
                                            </div>
                                        </div>
                                        {/* <div className="col-sm-6">
                <p> Admin </p>
                <div className="card" style={{ padding: '15px 20px' }}>
                    <form>
                        <div className="form-group row">
                            <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                <input type="checkbox" className="custom-control-input" name="checkPermission" value="option1" />
                                <span className="custom-control-label">&nbsp;</span>
                                View Admin
                         </label>
                        </div>

                        <div className="form-group row">
                            <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                <input type="checkbox" className="custom-control-input" name="example-checkbox2" value="option2" />
                                <span className="custom-control-label">&nbsp;</span>
                                Manage Programs
                        </label>
                        </div>
                        <div className="form-group row">
                            <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                <input type="checkbox" className="custom-control-input" name="example-checkbox3" value="option3" />
                                <span className="custom-control-label">&nbsp;</span>
                                Manage FAQ
                        </label>
                        </div>


                    </form>
                </div>
            </div> */}
                                    </div>

                                    {/* <div className="card-footer text-right">
                                        <button type="submit" onClick={this.updateUserPermissoin} className="btn btn-brand">Update</button>
                                    </div> */}

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="sales" aria-expanded="false">
                                    <p className='page-title mb-3'>Sales</p>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="table-responsive">
                                                <table id='categoriestbl' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th>Sno.</th>
                                                            <th>Amount($)</th>
                                                            <th className='text-center'>Date</th>
                                                            <th className='text-center'>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            this.state.booms.map((boom, bindex) => (
                                                                <tr>
                                                                    <td>{bindex + 1}</td>
                                                                    <td className="text-capitalize">
                                                                        ${boom.amount}
                                                                    </td>

                                                                    <td className='text-center'>
                                                                        {(new Date(boom.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                                    </td>
                                                                    <td className='text-center'>
                                                                        {/* {refer.refer_status == 1 ? (<NavLink to={`/user/referral/update/${this.state.member_id}/${refer.referral_code}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>) : ''} */}

                                                                        <button value={boom.id}  onClick={this.boomDelete()} className="btn btn-icon btn-sm " title="delete"><i className="fa fa-trash text-danger"></i></button>
                                                                    </td>
                                                                </tr>
                                                            ))
                                                        }
                                                    </tbody>
                                                    {this.state.booms.length == 0 && (<tfoot><tr><td colspan="4" className="text-center text-capitalize">{this.state.showCategoriesTableMessage ? this.state.showCategoriesTableMessage : 'No Sales'}</td></tr></tfoot>)}
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="reviews" aria-expanded="false">
                                    <p className='page-title mb-3'>Reviews</p>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="table-responsive">
                                                <table id='categoriestbl' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th>Sno.</th>
                                                            <th>Programs</th>
                                                            <th>Description</th>
                                                            <th className='text-center'>Rating</th>
                                                            <th className='text-center'>Date</th>
                                                            <th className='text-center'>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            this.state.reviews.map((review, rindex) => (
                                                                <tr key={rindex}>
                                                                    <td>{rindex + 1}</td>
                                                                    <td className="text-capitalize">
                                                                        {review.program_title}
                                                                    </td>
                                                                    <td className="text-capitalize">
                                                                        <div dangerouslySetInnerHTML={{ __html: review.description }} />
                                                                    </td>
                                                                    <td className='text-center brandColor'>

                                                                        <span className={review.rating == '1.00' ? '' : 'hidden'}>
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        </span>

                                                                        <span className={review.rating == '2.00' ? '' : 'hidden'}>
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        </span>

                                                                        <span className={review.rating == '3.00' ? '' : 'hidden'}>
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        </span>

                                                                        <span className={review.rating == '4.00' ? '' : 'hidden'}>
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <br />
                                                                        </span>

                                                                        <span className={review.rating == '5.00' ? '' : 'hidden'}>
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                            <i className="fa fa-star" aria-hidden="true"></i> &nbsp;
                                                                        </span>
                                                                    </td>
                                                                    <td className='text-center'>
                                                                        {(new Date(review.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                                            <input type="checkbox" class="custom-control-input" name="review_id" value={review.review_id} onChange={this.handleReviewPublish} checked={review.status == 1 ? true : ''} defaultChecked={review.status == 1 ? true : ''} />               <span class="custom-control-label">&nbsp;</span>
                                                                        </label>
                                                                    </td>
                                                                </tr>
                                                            ))
                                                        }
                                                    </tbody>
                                                    {this.state.reviews.length == 0 && (<tfoot><tr><td colspan="5" className="text-center text-capitalize">{this.state.showCategoriesTableMessage ? this.state.showCategoriesTableMessage : 'No Review'}</td></tr></tfoot>)}
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="referrals" aria-expanded="false">
                                    <p className='page-title mb-3'>Referrals</p>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="table-responsive">
                                                <table id='categoriestbl' className="table table-fixed table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th>Sno.</th>
                                                            <th className="text-left">Email</th>
                                                            <th className='text-center'>Staus</th>
                                                            <th className='text-center'>Earned($)</th>
                                                            <th className='text-center'>Date</th>
                                                            <th className='text-center'>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            this.state.refers.map((refer, refindex) => (
                                                                <tr>
                                                                    <td>{refindex + 1}</td>
                                                                    <td className="text-left">
                                                                        {refer.refer_email}
                                                                    </td>
                                                                    <td className='text-center'>
                                                                        {refer.refer_status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Pending</span>)}
                                                                    </td>
                                                                    <td className="text-center">
                                                                        ${refer.amount}
                                                                    </td>
                                                                    <td className='text-center'>
                                                                        {(new Date(refer.created_date)).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                                    </td>
                                                                    <td className='text-center'>
                                                                        {refer.refer_status == 1 ? (<NavLink to={`/user/referral/update/${this.state.member_id}/${refer.referral_code}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>) : ''}

                                                                        {/* <button value={refer.referral_code}  onClick={this.programCategoryDelete()} className="btn btn-icon btn-sm " title="delete"><i className="fa fa-trash text-danger"></i></button> */}
                                                                    </td>
                                                                </tr>
                                                            ))
                                                        }
                                                    </tbody>
                                                    {this.state.refers.length == 0 && (<tfoot><tr><td colspan="6" className="text-center text-capitalize">{this.state.showCategoriesTableMessage ? this.state.showCategoriesTableMessage : 'No Referral'}</td></tr></tfoot>)}
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>






                </div>


            </Fragment>





        )
    }
}

export default UserProfile
