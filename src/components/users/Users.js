import React, { Component,Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class UserListing extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        members: [],
        showAlert: false,
        baseApiURL:'',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL:'',
        showLoader: false,
        showConfirmationAlert:false,
        selected_member_id:'',
        access_permission:false,
        access_permission_r:false,
        access_permission_w:false,
        access_permission_u:false,
    }
    
    componentDidMount() {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showLoader: true });    
            axios({
                url: API_URL+"admin/member/list",
                method: 'get',
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {                   
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                const ApiURL = response.data.api_url;
                this.setState({ baseApiURL: ApiURL });
                this.setState({ showLoader: false });    
                this.setState({ access_permission: response.data.access_permission });
                
                this.setState({ access_permission_r: response.data.access_permission_r });
                this.setState({ access_permission_w: response.data.access_permission_w });
                this.setState({ access_permission_u: response.data.access_permission_u });
                if (error_code == '309') {
                    this.setState({showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message, showProgaramTableMessage: error_message });
                }
                else if (error_code == '200') {
                    //success                        
                    this.setState({ members: response.data.members });
                    console.log(this.state.members);                
                    var userTable = $('#usersTable').DataTable({                          
                        'columnDefs': [ {
                            'targets': [6], // column index (start from 0)
                            'orderable': false, // set orderable false for selected columns
                        }],
                        'lengthMenu': [10, 25, 50, 100, 500, 1000],
                        });

                        userTable.columns().iterator('column', function (ctx, idx) {
                            $(userTable.column(idx).header()).append('<span class="sort-icon"/>');
                        });
                }
                else {
                    //fail
                    this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message, showAlertActionURL: ''});                      
                }
            })
            .catch(err => {
                console.log(err);
            });
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }
    sweetalertok = () => { 
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL)
        {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }        
    }


    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }


    //userDelete    
    userDelete = () => (e) => {
        const member_id = e.currentTarget.value;   
        this.setState({ selected_member_id: member_id});     
        this.setState({ showConfirmationAlert: true});                
    }

    //subAdminDelete
    userDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false}); 
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,                 
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })  
            this.setState({ showConfirmationAlert: false, showLoader:true});
            //const admin_id = e.currentTarget.value;
            const member_id = this.state.selected_member_id;            
             
            axios({
                url: API_URL + "admin/member/delete",
                method: 'post',                 
                data: {
                    member_id: member_id,                             
                },  
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                this.setState({ showAlert: false});  
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                if (error_code == '200') {
                    //success            program/category/module/details         
                    this.setState({showAlertIcon : 'success', showLoader:false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/users'});                                              
                }
                else {
                    //fail
                    this.setState({showAlertIcon : 'warning',showLoader:false, showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                //error
                console.log(err);
            });
              
                         
        }
        //Get user permissions
    }

    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment> 
                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                   type={this.state.showAlertIcon}                    
                   title={this.state.showAlertMessage}
                   onConfirm={this.sweetalertok}
                   onCancel={() => this.setState({ showAlert: false })}
                   show={this.state.showAlert}
                /> 

                <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes, delete it!"
                confirmBtnBsStyle="danger"
                title="Are you sure?"
                onConfirm={this.userDeleteProcess}
                onCancel={() => this.setState({ showConfirmationAlert: false })}
                focusCancelBtn
                show={this.state.showConfirmationAlert}
                >
                {/* {this.state.showAlertMessage} */}
                </SweetAlert>


            <div className="row">
                <div className="col-md-12">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="header-action">
                            <h1 className="page-title">Members</h1>
                            <ol className="breadcrumb page-breadcrumb">
                                <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                                
                                <li className="breadcrumb-item active" aria-current="page">Members</li>                                
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <br />

            {this.state.access_permission &&
                 (<div className="tab-pane" id="Holiday-all">

                {this.state.access_permission_w &&
                 (<div className="card-footer text-right">
                    <NavLink to="/user/add" className="btn btn-brand">Add Member</NavLink>
                </div>)}


                <div className="card">
                    <div className="card-body">
                        <div className="table-responsive">
                            <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th >#</th>
                                        {/* <th class='text-center' >#</th> */}
                                        <th class='text-left' >Name</th>
                                        <th class='text-left' >Email</th>
                                        <th class='text-left'>Status</th>
                                        <th class='text-left' >Join Date</th>
                                        <th class='text-left' >Last Activity</th>
                                        {this.state.access_permission_u &&
                 (<th class='text-center' >Action</th>)}
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.members.map((member, index) => (
                                            <tr>
                                                <td>{index + 1}</td>
                                                {/* <td class='text-center'>                                                
                                                    <img class="avatar" src={member.profile_image ? this.state.baseApiURL + member.profile_image : '/assets/images/placeholder-2.png'} alt="" />
                                                </td> */}
                                                <td class='text-left table_content' style={{display: 'flex'}}>
                                                <img class="avatar" src={member.profile_image ? this.state.baseApiURL + member.profile_image : '/assets/images/placeholder-2.png'} alt="" />
                                                    <NavLink to={`/user/detail/${member.member_id}`} className="btn btn-icon btn-sm text-info brandColor" title="View Details">{member.first_name} {member.last_name}</NavLink></td>
                                                <td class='text-left'>{member.email}</td>
                                                <td class='text-left'>
                                                    {member.status == 1 ? (<span class="tag tag-success">Active</span>) : (<span class="tag tag-danger">Inactive</span>)}                                                   
                                                </td>
                                                

                                                <td className='text-left'>
                                                    <span className='dateFormat'>{member.created_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(member.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td>

                                                <td className='text-left'>
                                                    <span className='dateFormat'>{member.modified_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(member.modified_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td> 

                                                {this.state.access_permission_u &&
                                                    (<td class='text-center'>
                                                    {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                    <NavLink to={`/user/detail/${member.member_id}`} className="btn btn-icon btn-sm text-info brandColor" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>                                                    
                                                    <button value={member.member_id}  onClick={this.userDelete()} className="btn btn-icon btn-sm " title="delete"><i class="fa fa-trash text-danger"></i></button>
                                                </td>)}
                                            </tr>
                                        ))
                                    }

                                </tbody>
                                {this.state.members.length == 0 && (<tfoot><tr><td colspan="7" className="text-center text-capitalize">{this.state.showProgaramTableMessage ? this.state.showProgaramTableMessage : 'Data not found'}</td></tr></tfoot>)}
                            </table>
                        </div>
                    </div>
                </div>
            </div>)}
            </Fragment>
        )
    }
}

export default UserListing
