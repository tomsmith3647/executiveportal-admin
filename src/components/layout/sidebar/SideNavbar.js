
import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'

import axios from 'axios'
import { API_URL } from '../../../config/config'

// Variable
const $ = window.$

export class SideNavbar extends Component {

    state = {
        permissions: [],  
        user_id:'',
        auth_token:'',               
    }

    componentDidMount() {

        $('[data-toggle="tooltip"]').tooltip();
        $("#menu").metisMenu({
            toggle: false // disable the auto collapse. Default: true.
        });

        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
       
            this.getStaffPermission();            
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }

    getStaffPermission = () => {                 
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
         
        axios({
            url: API_URL + "admin/staff/permissions",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
        .then(response => {
            const error_code = response.data.error_code;
            const error_message = response.data.error_message;
           
           
            if (error_code == '200') {
                //success                        
                this.setState({permissions: response.data.permissions});   
                                      
            }
            else {
                this.setState({ showLoader: false, showProgaramTableMessage: error_message });                    
            }
        })
        .catch(err => {
            console.log(err);
        });                                        
    }
    
    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
        $("#menu").metisMenu({
            toggle: false // disable the auto collapse. Default: true.
        });
    }

    handleToggle = () => {
        $('body').toggleClass('offcanvas-active');
    }

    handleLogout = () => {
        window.location.reload()
        window.location.href = '/auth/signin'
        localStorage.removeItem('wasShown');

    }



    render() {

        const getPermissionOject = (value) => {
            const permissiondata = this.state.permissions.filter(function (e) {
                return e.permission_id == value;
            });

            if (permissiondata) {
                return permissiondata[0];
            }
        }



        //boom - 10588967
        const boomdata = getPermissionOject('10588967');
        var boom_r = 0;
        var boom_w = 0;
        var boom_u = 0;
        if (boomdata) {
            boom_r = boomdata.r;
            boom_w = boomdata.w;
            boom_u = boomdata.u;

        }

         

        //Notification - 42936533
        const notificationData = getPermissionOject('42936533');
        var notification_r = 0;
        var notification_w = 0;
        var notification_u = 0;
        if (notificationData) {
            notification_r = notificationData.r;
            notification_w = notificationData.w;
            notification_u = notificationData.u;
        }

        //User Dashboard // 55640434
        // const userDashboardData = getPermissionOject('55640434');
        var userdashboard_r = 1;
        var userdashboard_w = 1;
        var userdashboard_u = 1;
        // if (referAFriendData) {
        //     userdashboard_r = userDashboardData.r;
        //     userdashboard_w = userDashboardData.w;
        //     userdashboard_u = userDashboardData.u;
        // }

 


        //Program // 94012374
        //const programData = "";
        const programData = getPermissionOject('94012374');
        var program_r = 0;
        var program_w = 0;
        var program_u = 0;
        if (programData) {
            program_r = programData.r;
            program_w = programData.w;
            program_u = programData.u;
        }

        //Link // 91607899
        const linkData = getPermissionOject('91607899');
        var link_r = 0;
        var link_w = 0;
        var link_u = 0;
        if (linkData) {
            link_r = linkData.r;
            link_w = linkData.w;
            link_u = linkData.u;
        }

        //Support // 56519713
        const supportData = getPermissionOject('56519713');
        var support_r = 0;
        var support_w = 0;
        var support_u = 0;
        if (supportData) {
            support_r = supportData.r;
            support_w = supportData.w;
            support_u = supportData.u;
        }

        //FAQ // 16000381
        const faqData = getPermissionOject('16000381');
        var faq_r = 0;
        var faq_w = 0;
        var faq_u = 0;
        if (faqData) {
            faq_r = faqData.r;
            faq_w = faqData.w;
            faq_u = faqData.u;
        }

        //Event // 83407795
        const eventData = getPermissionOject('83407795');
        var eventData_r = 0;
        var eventData_w = 0;
        var eventData_u = 0;
        if (eventData) {
            eventData_r = eventData.r;
            eventData_w = eventData.w;
            eventData_u = eventData.u;
        }
 

         //permission // 69836958
         const permissionData = getPermissionOject('69836958');
         var permissionData_r = 0;
         var permissionData_w = 0;
         var permissionData_u = 0;
         if (permissionData) {
            permissionData_r = permissionData.r;
            permissionData_w = permissionData.w;
            permissionData_u = permissionData.u;
         }

          //permission // 54147970
          const referralsData = getPermissionOject('54147970');
          var referralsData_r = 0;
          var referralsData_w = 0;
          var referralsData_u = 0;
          if (referralsData) {
             referralsData_r = referralsData.r;
             referralsData_w = referralsData.w;
             referralsData_u = referralsData.u;
          }

          //users // 7573878
          const userData = getPermissionOject('7573878');
          var user_r = 0;
          var user_w = 0;
          var user_u = 0;
          if (userData) {
            user_r = userData.r;
            user_w = userData.w;
            user_u = userData.u;
          }

           


          if(this.state.user_id == '10001')
          {
            referralsData_r = 1;
            referralsData_w = 1;
            referralsData_u = 1;

            permissionData_r = 1;
            permissionData_w = 1;
            permissionData_u = 1;

            boom_r = 1;
            boom_w = 1;
            boom_u = 1;

            notification_r = 1;
            notification_w = 1;
            notification_u = 1;

            program_r = 1;
            program_w = 1;
            program_u = 1;

            link_r = 1;
            link_w = 1;
            link_u = 1;

            support_r = 1;
            support_w = 1;
            support_u = 1;  

            faq_r = 1;
            faq_w = 1;
            faq_u = 1;

            eventData_r = 1;
            eventData_w = 1;
            eventData_u = 1;

            user_r = 1;
            user_w = 1;
            user_u = 1;
          }

 

        return (

            <Fragment>
                {/* Start Main top header */}
                <div id="header_top" className="header_top">
                    <div className="container">
                        <div className="hleft">
                            <NavLink className="header-brand" to="/"><img src="/assets/images/brand-icon.png" alt="brand-icon" /></NavLink>
                            <div className="dropdown">

                                <NavLink to="/dashboard" className="nav-link icon app_inbox sideMenuIcons"><i className="fa fa-tachometer" data-toggle="tooltip" data-placement="right" title="Dashboard"></i></NavLink>
                                {user_r ?(<NavLink to="/users" className="nav-link icon app_file xs-hide sideMenuIcons"><i className="fa fa-users" data-toggle="tooltip" data-placement="right" title="Users"></i></NavLink>) : '' }
                                {program_r ?(<NavLink to="/programs" className="nav-link icon xs-hide sideMenuIcons"><i className="fa fa-folder-open" data-toggle="tooltip" data-placement="right" title="Programs"></i></NavLink>) : '' }
                                {faq_r ?(<NavLink to="/faqs" className="nav-link icon xs-hide sideMenuIcons"><i className="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="FAQ"></i></NavLink>) : '' }
                                {eventData_r ?(<NavLink to="/events" className="nav-link icon xs-hide sideMenuIcons"><i className="fa fa-calendar" data-toggle="tooltip" data-placement="right" title="Events"></i></NavLink>) : '' }
                                {notification_r ?(<NavLink to="/notifications" className="nav-link icon app_file xs-hide sideMenuIcons"><i class="fa fa-bell" data-toggle="tooltip" data-placement="right" title="Notification"></i></NavLink>) : '' }
                                {link_r ?(<NavLink to="/links" className="nav-link icon xs-hide sideMenuIcons"><i className="fa fa-link" data-toggle="tooltip" data-placement="right" title="Links"></i></NavLink>) : '' }
                                {this.state.user_id == '10001' ? (<NavLink to="/staff/list" className="nav-link icon app_file xs-hide sideMenuIcons"><i className="fa fa-users" data-toggle="tooltip" data-placement="right" title="Users"></i></NavLink>) : ''}
                                <NavLink to="/roles" className="nav-link icon app_file xs-hide sideMenuIcons"><i class="fa fa-tasks" data-toggle="tooltip" data-placement="right" title="Roles"></i></NavLink>
                                
                                {/* <NavLink to="/staff/roles" className="nav-link icon app_file xs-hide sideMenuIcons"><i class="fa fa-tasks" data-toggle="tooltip" data-placement="right" title="Roles"></i></NavLink>                                                                 */}
                                {/* <NavLink to="#" className="nav-link icon app_file xs-hide sideMenuIcons"><i class="fa fa-cogs" data-toggle="tooltip" data-placement="right" title="Instructions"></i></NavLink> */}

                            </div>
                        </div>
                        <div class="hright">
                            <span to="" className="nav-link icon menu_toggle" onClick={this.handleToggle}><i className="fe fe-align-center"></i></span>
                        </div>

                    </div>
                </div>
                {/* Start Rightbar setting panel  */}


                {/* Start Main leftbar navigation */}
                <div id="left-sidebar" className="sidebar">
                    <div className="brand-name brandColor">
                        <img src="/assets/images/brand-name.png" alt="brand-name" />
                    </div>
                    <nav className="sidebar-nav">
                        <ul className="metismenu" id="menu">
                            <li className="">
                                <NavLink to="/dashboard" className='sideMenuLinks' activeClassName='active'>
                                    {/* <i className="fa fa-dashboard"></i> */}

                                    <span>Dashboard</span>
                                </NavLink>
                            </li>
                            {user_r ?(<li>
                                <NavLink to="/users" className='sideMenuLinks' activeClassName='active'>

                                    <span>Members</span>
                                </NavLink>
                            </li>) : ''}
                           {program_r ?(<li>
                                <NavLink to="/programs" className='sideMenuLinks' activeClassName='active'>

                                    <span>Programs</span>
                                </NavLink>
                            </li>) : ''}
                            {faq_r ?(<li>
                                <NavLink to="/faqs" className='sideMenuLinks' activeClassName='active'>

                                    <span>Help Center</span>
                                </NavLink>
                            </li>) : ''}
                            {eventData_r ?(<li>
                                <NavLink to="/events" className='sideMenuLinks' activeClassName='active'>

                                    <span>Events</span>
                                </NavLink>
                            </li>) : ''}
                            {notification_r ?(<li>
                                <NavLink to="/notifications" className='sideMenuLinks' activeClassName='active'>
                                    <span>Notifications</span>
                                </NavLink>
                            </li>) : ''}
                            {link_r ?(<li>
                                <NavLink to="/links" className='sideMenuLinks' activeClassName='active'>

                                    <span>Links</span>
                                </NavLink>
                            </li>) : ''}
                            



                            
                            {this.state.user_id == '10001' ?
                            (<li>
                                <NavLink to="/staff/list" className='sideMenuLinks' activeClassName='active'>
                                    <span>Staff</span>
                                </NavLink>
                                
                            </li>) : ''}

                            <li>
                                <NavLink to="#" className='sideMenuLinks' activeClassName='active'>
                                <span className="fa arrow"></span>
                                    <span>Roles & Permissions</span>
                                </NavLink>
                                <ul aria-expanded="false">
                                {permissionData_r ?
                                    (<li>
                                        <NavLink to="/roles" className='' activeClassName='active'>
                                            <span>Member Roles</span>
                                        </NavLink>
                                    </li>):''}
                                    {this.state.user_id == '10001' ?
                                    (<li>
                                        <NavLink to="/staff/roles" className='' activeClassName='active'>
                                            <span>Staff Roles</span>
                                        </NavLink>
                                    </li>):''}
                                </ul>
                            </li>

                            {/* <li>
                                <NavLink to="/staff/roles" className='sideMenuLinks' activeClassName='active'>
                                    <span>Staff Roles & Permissions</span>
                                </NavLink>
                            </li> */}

                            
                           {support_r ? (<li>
                                <NavLink to="#" className='sideMenuLinks' activeClassName='active'>
                                    {/* <i className="fa fa-cogs" data-toggle="tooltip" data-placement="right" title="Support"></i> */}
                                    <span className="fa arrow"></span>
                                    <span>Instructions </span>
                                </NavLink>
                                <ul aria-expanded="false">
                                    <li>
                                        <NavLink to="/instruction/emailsupport" className='' activeClassName='active'>
                                            <span>Email Support</span>
                                        </NavLink>
                                    </li>
                                </ul>
                            </li>) : ''}




                        </ul>
                    </nav>
                </div>
                {/* Start project content area */}
            </Fragment>

        )
    }
}

export default SideNavbar
