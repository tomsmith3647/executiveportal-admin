import React, { Component, Fragment } from 'react'
import { API_URL } from '../../config/config'
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import LoadingOverlay from 'react-loading-overlay';
import { NavLink } from 'react-router-dom'
// Variable
const $ = window.$

export class StaffProfile extends Component {


    constructor(props) {
        super(props);

        this.state = { admin: [] };
        this.state = {
            admin: [{ first_name: "", last_name: "" }]
        };
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            linkeinProfile: '',
            address: '',
            city: '',
            state_code: '',
            country_code: 'USA',
            countries: [],
            states: [],
            businessWebsite: '',
            businessDescription: '',
            showAlert: false,
            showAlertMessage: '',
            admin: {},
            admin_permissions: [],
            valid_permission: [],
            selected_permission: {},
            crm_id: '',
            showLoader: false,
            staff_permissions:[],
            roles:[],
            showAlertActionURL:'',
        }
    }





    componentDidMount() {
        $('.dropify').dropify();

        this.table('salesTable')
        this.table('tableReview')
        this.table('tableReferral')


        //get user detail from memeber id
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const admin_id = this.props.match.params.admin_id;
            if (admin_id) {
                this.setState({ showLoader: true });
                //Get user profile details           
                axios({
                    url: API_URL + "admin/subadmin/detail/" + admin_id,
                    method: 'get',
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {
                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        this.setState({ showLoader: false });
                        if (error_code == '200') {
                            //success

                            delete response.data.admin.modified_date;
                            delete response.data.admin.created_date;


                            this.setState({ admin: response.data.admin });
                            this.setState({ admin: { ...this.state.admin, state_code: response.data.admin.state_code } });
                            const default_country_code = this.state.admin.country_code;
                            if (default_country_code) {
                                this.setState({ showLoader: true });
                                //get state List
                                axios({
                                    url: API_URL + "state/list/" + default_country_code,
                                    method: 'get',
                                })
                                    .then(response => {
                                        const error_code = response.data.error_code;
                                        const error_message = response.data.error_message;

                                        this.setState({ showLoader: false });
                                        if (error_code == '200') {
                                            //success                        
                                            this.setState({ states: response.data.states });
                                        }
                                        else {
                                            //fail
                                            //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                                        }
                                    })
                                    .catch(err => {
                                        this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                                    });
                            }

                            if (this.state.admin.role_id)
                            {
                                
                                 //Get user permissions
                                this.setState({ showLoader: true });
                                axios({
                                    url: API_URL + "admin/subadmin/role/permission/list/" + this.state.admin.role_id,
                                    method: 'get',
                                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                                })
                                .then(response => {

                                    const error_code = response.data.error_code;
                                    const error_message = response.data.error_message;
                                    this.setState({ showLoader: false });
                                    if (error_code == '200') {
                                        //success
                                        this.setState({ staff_permissions: response.data.role_permissions });
                                        
                                    }
                                    else {
                                        //fail
                                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                                    }
                                })
                                .catch(err => {
                                    //error
                                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                                });
                            }

                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                    });


              
                
            }
            else {
                this.setState({ showAlert: true, showAlertMessage: "admin id empty!" });
            }



            //get country List
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "country/list",
                method: 'get',
            })
                .then(response => {
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    this.setState({ showLoader: false });
                    if (error_code == '200') {
                        //success                        
                        this.setState({ countries: response.data.countries });
                    }
                    else {
                        //fail
                        //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });

                this.setState({ showLoader: true });
            //get state List
            axios({
                url: API_URL + "admin/staff/role/list",
                method: 'get',
            })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;

                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ roles: response.data.roles });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });




        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }



    }

    table = (id) => {
        var table = $(`#${id}`).DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    },
                    render: function (datad) {
                        //return '<input type="checkbox" name="selectcompanyid">';
                        return '<label><input type="checkbox" name="test[]"></label>';
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'order': [[1, 'asc']]
        });
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })

    }

    handleCountryChange = (e) => {



        const country_code = e.target.value

        if (!country_code) {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please select country" });
            return false;
        }


        this.setState(
            { admin: { ...this.state.admin, country_code: country_code } }
        )

        //get state List
        this.setState({ showLoader: true });
        axios({
            url: API_URL + "state/list/" + country_code,
            method: 'get',
        })
            .then(response => {
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;

                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ states: response.data.states });
                }
                else {
                    //fail
                    //this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                }
            })
            .catch(err => {
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });


        console.log(this.state.countries);
    }



    handleSubmit = (e) => {
        //Prevent Default()
        e.preventDefault()
    }


    handlePermission = (e) => {
        //PerMission

        const isChecked = e.target.checked ? 1 : 0;
        if (e.target.checked) {


            // this.setState({
            //     ...this.state,
            //     valid_permission: [...this.state.valid_permission, test]
            // })

            if (this.state.valid_permission.length > 0) {
                if (this.state.valid_permission) {
                    var rrrr = [];
                    this.state.valid_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 1;
                            rrrr.push(item);
                            this.setState({ valid_permission: rrrr })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }
                    })

                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }




        }
        else {


            // const test = {
            //     'role': e.target.name,
            //     'status': isChecked
            // }


            // var joined = this.state.valid_permission.concat(test);
            // this.setState({ valid_permission: joined })


            if (this.state.valid_permission.length > 0) {
                if (this.state.valid_permission) {
                    var eee = [];
                    this.state.valid_permission.map(item => {

                        if (item.role == e.target.name) {
                            item.status = 0;
                            eee.push(item);
                            this.setState({ valid_permission: eee })
                        }
                        else {
                            const test = {
                                'role': e.target.name,
                                'status': isChecked
                            }
                            var joined = this.state.valid_permission.concat(test);
                            this.setState({ valid_permission: joined })
                        }
                    })

                }
            }
            else {
                const test = {
                    'role': e.target.name,
                    'status': isChecked
                }
                var joined = this.state.valid_permission.concat(test);
                this.setState({ valid_permission: joined })
            }




            //  const changeDesc = (key) => {
            //     for (var i in this.state.valid_permission) {
            //         if (this.state.valid_permission[i].role == key) {
            //             this.state.valid_permission[i].status = 0;
            //             break; //Stop this loop, we found it!
            //         }
            //     }
            // }


            // const selectedPermission = {
            //     'role': e.target.name,
            //     'status': isChecked
            // }

            // this.setState({
            //     ...this.state,
            //     valid_permission: [...this.state.valid_permission, selectedPermission]
            // })

        }
        console.log(this.state.valid_permission);
    }


    updateUserPermissoin = () => {

        console.log(this.state.valid_permission);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const admin_id = this.props.match.params.admin_id;
            if (admin_id) {
                const selected_permissions = this.state.valid_permission;
                axios({
                    url: API_URL + "admin/subadmin/permission/edit/",
                    method: 'post',
                    data: {
                        admin_id: admin_id,
                        permissions: selected_permissions
                    },
                    headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
                })
                    .then(response => {

                        const error_code = response.data.error_code;
                        const error_message = response.data.error_message;
                        if (error_code == '200') {
                            //success                        
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                            window.location.reload();
                        }
                        else {
                            //fail
                            this.setState({ showAlert: true, showAlertMessage: error_message });
                        }
                    })
                    .catch(err => {
                        //error
                        this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                    });
            }
            else {
                this.setState({ showAlert: true, showAlertMessage: "admin id empty!" });
            }

        }
        //Get user permissions

    }

    handleChangeProfile = (e) => {

        const contact = this.state.admin;
        contact[e.target.name] = e.target.value;
        this.setState({ admin: contact });





    }


    validateUrl = (url) => {
        var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
        const result = urlregex.test(url);
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    checkUrl = (e) => {
        const url = e.target.value;
        if (this.validateUrl(url)) {
            return true;
        }
        else {
            this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid URL." });
            return false;
        }
    }

    //update profile
    updateStaffProfile = (e) => {

        e.preventDefault();
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,

                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })

            const words = ['linkedin.com'];
            const regex = new RegExp(words.join('|'));

            if(this.state.admin.linkedin_profile || this.state.admin.business_website)
            {
                if (!this.validateUrl(this.state.admin.linkedin_profile)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                    return false;
                }
                else if (!regex.test(this.state.admin.linkedin_profile)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid LinkedIn Profile URL." });
                    return false;
                }
                else if (!this.validateUrl(this.state.admin.business_website)) {
                    e.preventDefault()
                    this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: "Please enter valid Business Website URL." });
                    return false;
                }
            }

            axios({
                url: API_URL + "admin/subadmin/profile/update",
                method: 'post',
                data: this.state.admin,
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success                                                
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/staff/list' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });
        }
        //Get user permissions

    }



    //resetPassword
    //update profile
    resetPassword = () => {
        console.log(this.state.admin);
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            const admin_id = this.props.match.params.admin_id;
            axios({
                url: API_URL + "admin/subadmin/password/reset",
                method: 'post',
                data: this.state.admin,
                data: {
                    admin_id: admin_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {

                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success                                            
                        this.setState({ showAlertIcon: 'success', showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/staff/list' });
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });
        }
        //Get user permissions
    }


    sweetalertok = () => {
        this.setState({ showAlert: false });
        if (this.state.showAlertActionURL) {
            if (this.state.showAlertActionURL == 'refresh') {
                window.location.reload();
            }
            else {
                window.location.href = this.state.showAlertActionURL; // '/dashboard';
            }
        }
    }

    checkValidZipCode = (evt) => {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            evt.preventDefault();
            return false;
        }
        else {
            if (iKeyCode == 46) {
                evt.preventDefault();
                return false;
            }
            else {
                if (evt.target.value.length > 7) {
                    evt.preventDefault();
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }

    render() {
        return (

            <Fragment>


                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>
                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Staff Profile</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>
                                    <li className="breadcrumb-item"><NavLink to="/staff/list">Staff</NavLink></li>
                                    <li className="breadcrumb-item active" aria-current="page">Profile</li>
                                </ol>
                            </div>
                            <ul className="nav nav-tabs page-header-tab" role="tablist">
                                <li className="nav-item"><a className="nav-link active" data-toggle="tab" href="#profile" aria-expanded="true">Profile</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#permission" aria-expanded="true">Permission</a></li>


                                <div className="card-footer text-right resetPassword-btn">
                                    <button type="button" onClick={this.resetPassword} className="btn btn-brand">Reset password</button>
                                </div>
                                {/* <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#sales" aria-expanded="false">Sales</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#reviews" aria-expanded="false">Reviews</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#referrals" aria-expanded="false">Referrals</a></li> */}
                            </ul>

                        </div>
                    </div>
                </div>
                <br />
                <div className='user-profile'>


                    <div className="row">

                        <div className="col">

                            <div className="tab-content">
                                <div role="tabpanel" className="tab-pane vivify fadeIn active" id="profile" aria-expanded="false">
                                    <form className='form-horizontal' onSubmit={this.updateStaffProfile}>
                                        <div className="card">
                                            <div className="card-body">
                                                <h5 class="card-title">User Profile </h5>

                                                <div className="form-group required row">
                                                    <label className="col-md-3 col-form-label">Role </label>
                                                    <div className="col-md-7">
                                                        <select className="form-control custom-select" name='role_id' onChange={this.handleChangeProfile} required  >
                                                            <option value="" hidden selected>Please Select Role</option>
                                                            {                                                             
                                                                this.state.roles.map((role) => (
                                                                    role.role_id == this.state.admin.role_id ? (<option selected value={role.role_id}>({role.role_id}) {role.role_title}</option>) : (<option value={role.role_id}>({role.role_id}) {role.role_title}</option>)
                                                                ))
                                                            }
                                                        </select>

                                                        {/* {                                                             
                                                            this.state.roles.map((role) => (
                                                                role.role_id == this.state.member.role_id ? (<span className="form-control" >{'('+role.role_id+') '+ role.role_title}</span>) : ''
                                                            ))
                                                        } */}
                                                    </div>
                                                </div>

                                                <div className="form-group required row">

                                                    <label className="col-md-3 col-form-label">First Name </label>
                                                    <div className="col-md-7">
                                                        <input type="text" className="form-control" value={this.state.admin.first_name} name='first_name' onChange={this.handleChangeProfile} required />
                                                    </div>
                                                </div>
                                                <div className="form-group required row">
                                                    <label className="col-md-3 col-form-label">Last Name </label>
                                                    <div className="col-md-7">
                                                        <input type="text" readonly="readonly" className="form-control" value={this.state.admin.last_name} name='last_name' onChange={this.handleChangeProfile} required />
                                                    </div>
                                                </div>
                                                <div className="form-group required row">
                                                    <label className="col-md-3 col-form-label">Email address </label>
                                                    <div className="col-md-7">
                                                        <input type="text" className="form-control" value={this.state.admin.email} name='email' onChange={this.handleChangeProfile} required />
                                                    </div>
                                                </div>

                                                {/* <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">LinkedIn Profile </label>
                                                    <div className="col-md-7">
                                                        <input type="text" className="form-control" value={this.state.admin.linkedin_profile} onBlur={this.checkUrl} name='linkedin_profile' onChange={this.handleChangeProfile} />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Address </label>
                                                    <div className="col-md-7">
                                                        <textarea name="address" id="address" className="form-control" value={this.state.admin.address} name='address' onChange={this.handleChangeProfile}></textarea>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">City </label>
                                                    <div className="col-md-7">
                                                        <input type="text" className="form-control" value={this.state.admin.city} name='city' onChange={this.handleChangeProfile} />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Country </label>
                                                    <div className="col-md-7">
                                                        <select className="form-control custom-select" name='country_code' onChange={this.handleCountryChange} required >
                                                            <option value="" hidden selected>Please Select Country</option>
                                                            {
                                                                this.state.countries.map((cntry) => (
                                                                    cntry.country_code == this.state.admin.country_code ? (<option selected value={cntry.country_code}>{cntry.country_name}</option>) : (<option value={cntry.country_code}>{cntry.country_name}</option>)
                                                                ))
                                                            }
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">State / Province </label>
                                                    <div className="col-md-7">
                                                        <select className="form-control custom-select" name='state_code' onChange={this.handleChangeProfile} >
                                                            <option value="" hidden selected>Please Select State</option>
                                                            {
                                                                this.state.states.map((state) => (
                                                                    state.state_code == this.state.admin.state_code ? (<option selected value={state.state_code}>{state.state_name}</option>) : (<option value={state.state_code}>{state.state_name}</option>)
                                                                ))
                                                            }
                                                        </select>
                                                    </div>
                                                </div>
                                                 
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Zip Code</label>
                                                    <div className="col-md-7">
                                                        <input type="text" className="form-control" value={this.state.admin.zip_code} name='zip_code' onKeyPress={this.checkValidZipCode} onChange={this.handleChangeProfile} />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Business Website</label>
                                                    <div className="col-md-7">
                                                        <input type="text" className="form-control" value={this.state.admin.business_website} onBlur={this.checkUrl} name='business_website' onChange={this.handleChangeProfile} />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Business Description </label>
                                                    <div className="col-md-7">
                                                        <textarea id="businessDescription" className="form-control" value={this.state.admin.business_description} name='business_description' onChange={this.handleChangeProfile}></textarea>
                                                    </div>
                                                </div> */}

                                                <div className="form-group row">
                                                    <label className="col-md-3 col-form-label">Status</label>
                                                    <div className="col-md-7">
                                                        <select className="form-control custom-select" name='status' onChange={this.handleChangeProfile}>
                                                            <option selected={this.state.admin.status == 1} value="1">Active</option>
                                                            <option selected={this.state.admin.status == 0} value="0">Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div className="card-footer text-right">

                                                <button type="submit" className="btn btn-brand">Update Profile</button>
                                            </div>
                                        </div>


                                    </form>

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="permission" aria-expanded="false">

                                    <div className="row">
                                        <div className="col-sm-12">
                                            <form>
                                                <div className="card">

                                                    <div className="card-body">
                                                        <h5 class="card-title">User Permissions </h5>



                                                        <div className="form-group row">
                                                            <div className="table-responsive">
                                                            <table className="table table-hover table-vcenter text-nowrap    table-striped  border-style spacing5">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Permissions</th>
                                                                        <th className='text-center'>Read</th>
                                                                         <th className='text-center'>Write</th>
                                                                       {/* <th className='text-center'>Update</th> */}
                                                                        {/* <th className='text-center'>Status</th>                                         */}
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {
                                                                        this.state.staff_permissions.map((memberPer, index) => (
                                                                            <Fragment>
                                                                                {memberPer ? 
                                                                            (<tr key={index}>
                                                                                <td>{index + 1}</td>
                                                                                <td>{memberPer ? memberPer.permission_title: ''}</td>
                                                                                <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberPer.permission_id + '_r'} value={memberPer.r} onChange={this.handlePermission} defaultChecked={memberPer.r == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                <td className='text-center'>
                                                                                {memberPer.w != '3' && memberPer.role_type == 'member' ? (
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberPer.permission_id + '_w'} value={memberPer.w} onChange={this.handlePermission} defaultChecked={memberPer.w == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label> ) : '' }
                                                                                </td>

                                                                                 {/*<td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" className="custom-control-input" name={memberPer.permission_id + '_u'} value={memberPer.u} onChange={this.handlePermission} defaultChecked={memberPer.u == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td> */}


                                                                            </tr>) : ''} </Fragment>
                                                                        ))
                                                                    }
                                                                    {/* {

                                                                        this.state.member_program_permissions.map((memberProPer, indexpm) => (
                                                                            <tr key={indexpm}>
                                                                                <td>{indexpm + 1 + this.state.member_permissions.length}</td>
                                                                                <td>{memberProPer.program_detail.program_title} ({memberProPer.program_detail.program_code})</td>
                                                                                <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberProPer.permission_id + '_r'} value={memberProPer.r} onChange={this.handleProgramPermission} defaultChecked={memberProPer.r == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberProPer.permission_id + '_w'} value={memberProPer.w} onChange={this.handleProgramPermission} defaultChecked={memberProPer.w == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>

                                                                                 <td className='text-center'>
                                                                                    <label className="custom-control custom-checkbox custom-control-inline mr-0">
                                                                                        <input type="checkbox" disabled className="custom-control-input" name={memberProPer.permission_id + '_u'} value={memberProPer.u} onChange={this.handleProgramPermission} defaultChecked={memberProPer.u == 1 ? true : ''} />               <span className="custom-control-label">&nbsp;</span>
                                                                                    </label>
                                                                                </td>  

                                                                            </tr>
                                                                        ))
                                                                    } */}

                                                                </tbody>
                                                            </table>
                                                            </div>
                                                        </div>




                                                        {/* <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="checkPermission" value="option1" onChange={this.handlePermission} />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            View / Submit BOOM
                                                     </label>
                                                    </div>

                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox2" value="option2" onChange={this.handlePermission} />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            View Love
                                                    </label>
                                                    </div>

                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox3" value="option3" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            Refer A Friend
                                                    </label>
                                                    </div> */}


                                                    </div>

                                                    {/* <div className="card-footer text-right">
                                                        <button type="submit" onClick={this.updateUserPermissoin} className="btn btn-brand">Update</button>
                                                    </div> */}

                                                </div>
                                            </form>
                                        </div>
                                        {/* <div className="col-sm-6">
                                            <p> Admin </p>
                                            <div className="card" style={{ padding: '15px 20px' }}>
                                                <form>
                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="checkPermission" value="option1" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            View Admin
                                                     </label>
                                                    </div>

                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox2" value="option2" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            Manage Programs
                                                    </label>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label class="custom-control custom-checkbox custom-control-inline mr-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox3" value="option3" />
                                                            <span class="custom-control-label">&nbsp;</span>
                                                            Manage FAQ
                                                    </label>
                                                    </div>


                                                </form>
                                            </div>
                                        </div> */}
                                    </div>



                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="sales" aria-expanded="false">
                                    <p> Sales </p>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id='salesTable' class="table table-hover js-basic-example dataTable table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th><input type="checkbox" name="select_all" value="1" id="example-select-all" /></th>
                                                            <th>Date</th>
                                                            <th>Amount</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>November 12, 2019</td>
                                                            <td>$5,000</td>
                                                            <td>
                                                                <button type="button" className="btn btn-icon btn-sm" title="Edit"><i className="fa fa-edit"></i></button>
                                                                <button type="button" className="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i className="fa fa-trash-o text-danger"></i></button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>November 13, 2019</td>
                                                            <td>$5,000</td>
                                                            <td>
                                                                <button type="button" className="btn btn-icon btn-sm" title="Edit"><i className="fa fa-edit"></i></button>
                                                                <button type="button" className="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i className="fa fa-trash-o text-danger"></i></button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="reviews" aria-expanded="false">
                                    <p>
                                        Reviews
                                    </p>

                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id='tableReview' class="table table-hover js-basic-example dataTable table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Product</th>
                                                            <th>Author</th>
                                                            <th>Review</th>
                                                            <th>Rating</th>
                                                            <th>Date</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>Apple Watch Series 3</td>
                                                            <td>Sophhia Hale</td>
                                                            <td>The Series 4 is a significant step</td>
                                                            <td>****</td>
                                                            <td>12.07.2018 10:00</td>
                                                            <td>Published</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Apple Watch Series 4</td>
                                                            <td>Sophhia Hale</td>
                                                            <td>The Series 4 is a significant step</td>
                                                            <td>****</td>
                                                            <td>12.07.2018 10:00</td>
                                                            <td>Published</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" className="tab-pane vivify fadeIn" id="referrals" aria-expanded="false">
                                    <p>
                                        Referrals
                                    </p>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id='tableReferral' class="table table-hover js-basic-example dataTable table-striped table_custom border-style spacing5">
                                                    <thead>
                                                        <tr>
                                                            <th>Accepted</th>
                                                            <th>Date</th>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Earned</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>11/09/2019</td>
                                                            <td>BOB Smith</td>
                                                            <td>smith.bob@gmail.com</td>
                                                            <td>$5,000</td>
                                                            <td>
                                                                <button type="button" className="btn btn-icon btn-sm" title="Edit"><i className="fa fa-edit"></i></button>
                                                                <button type="button" className="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i className="fa fa-trash-o text-danger"></i></button>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="card-footer text-right">
                                        <button type="submit" className="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </Fragment>





        )
    }
}

export default StaffProfile
