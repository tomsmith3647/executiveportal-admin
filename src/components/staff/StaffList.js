import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';
const $ = window.$
export class StaffList extends Component {

    state = {
        user_id: '',
        email: '',
        full_name: '',
        auth_token: '',
        showAlert:false,
        showLoader:false,
        admins: [],
        showAlertActionURL:'',
        

        baseApiURL: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL: '',
        showLoader: false,
    }

    componentDidMount() {
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                email: userSession.email,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.getStaffList(1);
        }
        else {
            window.localStorage.removeItem('userSession');
            window.location.href = '/auth/signin'
        }
    }


    //subAdminDelete
    getStaffList = (str) => { 
        var strget = str ? str : 0;       
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true });
        axios({
            url: API_URL+"admin/subadmin/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {                   
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ admins: response.data.admins });  
                    if(strget)
                    {
                        $('#usersTable').DataTable({                          
                            'columnDefs': [ {
                               'targets': [6], // column index (start from 0)
                               'orderable': false, // set orderable false for selected columns
                            }],
                            'lengthMenu': [10, 25, 50, 100, 500, 1000],
                          });
                    }
                    
                 //   this.setState({showAlertIcon: 'warning',  showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message });
                }
                else {
                    //fail
                    this.setState({showAlertIcon: 'warning',  showAlert: true, showAlertMessage: error_message, showAlertActionURL:'' });                        
                }
            })
            .catch(err => {
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });                      
    }


    //subAdminDelete
    getStaffListDelete = () => { 
        
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        this.setState({ showLoader: true });
        axios({
            url: API_URL+"admin/subadmin/list",
            method: 'get',
            headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
        })
            .then(response => {                   
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                this.setState({ showLoader: false });
                if (error_code == '200') {
                    //success                        
                    this.setState({ admins: response.data.admins });                                        
                }
                else {
                    //fail
                    this.setState({showAlertIcon: 'warning',  showAlert: true, showAlertMessage: error_message, showAlertActionURL:'' });                        
                }
            })
            .catch(err => {
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });                      
    }
 

    convertUTCDateToLocalDate =(date)=> {

        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);    
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();    
        newDate.setHours(hours - offset);
        return newDate;   
    }

    //subAdminDelete
    subAdminDelete = () => (e) => {
        
      
        const admin_id = e.currentTarget.dataset.id ;
        this.setState({ selected_admin_id: admin_id });
        this.setState({ showConfirmationAlert: true });
        
        
    }
 

    //subAdminDeleteProcess
    subAdminDeleteProcess = () => {
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })
            this.setState({ showConfirmationAlert: false, showLoader: true });
            //const admin_id = e.currentTarget.value;
            const admin_id = this.state.selected_admin_id;

            axios({
                url: API_URL + "admin/subadmin/delete",
                method: 'post',
                data: {
                    admin_id: admin_id,
                },
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
                .then(response => {
                    this.setState({ showAlert: false });
                    const error_code = response.data.error_code;
                    const error_message = response.data.error_message;
                    if (error_code == '200') {
                        //success    
                                                      
                        this.setState({ showAlertIcon: 'success', showLoader: false, showAlert: true, showAlertMessage: error_message, showAlertActionURL: '/staff/list' });

                        
                    }
                    else {
                        //fail
                        this.setState({ showAlertIcon: 'warning', showLoader: false, showAlert: true, showAlertMessage: error_message });
                    }
                })
                .catch(err => {
                    //error
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
                });


        }
        //Get user permissions
    }

    //subAdminDelete
    subAdminDeleteProcessOLD= () => {
        
        this.setState({ showConfirmationAlert: false });
        const userSession = JSON.parse(window.localStorage.getItem('userSession'));
        if (userSession.user_id) {
            this.setState({
                user_id: userSession.user_id,                 
                full_name: userSession.full_name,
                auth_token: userSession.auth_token,
            })  
            //const admin_id = e.currentTarget.value;
            const admin_id = this.state.selected_admin_id;
            console.log(admin_id);
             
            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/subadmin/delete",
                method: 'post',
                data:this.state.admin,
                data: {
                    admin_id: admin_id,                             
                },  
                headers: { 'user_id': userSession.user_id, 'auth_token': userSession.auth_token }
            })
            .then(response => {
                this.setState({ showAlert: false});  
                const error_code = response.data.error_code;
                const error_message = response.data.error_message;
                if (error_code == '200') {
                    //success                        
                    this.getStaffListDelete();
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true,  showAlertMessage: error_message, showAlertActionURL:'' });                      
                }
                else {
                    //fail
                    this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true,  showAlertMessage: error_message, showAlertActionURL:'' });                      
                }
            })
            .catch(err => {
                //error
                this.setState({showAlertIcon: 'warning',  showLoader: false, showAlert: true, showAlertMessage: 'Oops! Something went wrong. Please try again later.', showAlertActionURL:'refresh'});
            });
              
                         
        }
        //Get user permissions
    }

    sweetalertok = () => {
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL == 'refresh') 
        {
            window.location.reload();  
        } 
        else if(this.state.showAlertActionURL) 
        {
            window.location.href = this.state.showAlertActionURL;  
        }
    }


    render() {

        //{  weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

        const DATE_OPTIONS = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

        return (
            <Fragment> 

                
                
<LoadingOverlay
                    active={this.state.showLoader}
                    //active='true'
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                    type={this.state.showAlertIcon}
                    title={this.state.showAlertMessage}
                    onConfirm={this.sweetalertok}
                    onCancel={() => this.setState({ showAlert: false })}
                    show={this.state.showAlert}
                />

                <SweetAlert
                    warning
                    showCancel
                    confirmBtnText="Yes, delete it!"
                    confirmBtnBsStyle="danger"
                    title="Are you sure?"
                    onConfirm={this.subAdminDeleteProcess}
                    onCancel={() => this.setState({ showConfirmationAlert: false })}
                    focusCancelBtn
                    show={this.state.showConfirmationAlert}
                >
                    {/* {this.state.showAlertMessage} */}
                </SweetAlert>


                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header-action">
                                <h1 className="page-title">Staff List</h1>
                                <ol className="breadcrumb page-breadcrumb">
                                    <li className="breadcrumb-item"><NavLink to="/dashboard">Dashboard</NavLink></li>                                                                
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <br />

            <div className="tab-pane" id="Holiday-all">
                 
                <div className="card-footer text-right">
                    <NavLink to="/staff/add" className="btn btn-brand">Add Staff</NavLink>
                    {/* &nbsp; &nbsp; <NavLink to="/staff/roles" className="btn btn-brand">Roles & Permissions</NavLink> */}
                </div>


                <div className="card">
                    <div className="card-body">
                        <div className="table-responsive">
                            <table id='usersTable' className="table table-hover table-vcenter text-nowrap table-striped table_custom border-style spacing5">
                                <thead>
                                    <tr>
                                        <th >#</th>
                                        <th class='text-left' >Name</th>
                                        <th class='text-left' >Email</th>
                                        <th class='text-left'>Status</th>
                                        <th class='text-left' >Join Date</th>
                                        <th class='text-left' >Last Activity</th>
                                        <th class='text-center' >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.admins.map((admin, index) => (
                                            <tr>
                                                <td>{index + 1}</td>
                                                <td class='text-left table_content'>
                                                <NavLink to={`/staff/details/${admin.admin_id}`} className="btn btn-icon btn-sm text-info brandColor" title="View Details">{admin.first_name} {admin.last_name}</NavLink>
                                                </td>
                                                <td class='text-left'>{admin.email}</td>
                                                <td class='text-left'>
                                                    
                                                    {admin.status == 1 ? (<span className="tag tag-success">Active</span>) : (<span className="tag tag-danger">Inactive</span>)}
                                                </td>
                                       
                                                <td className='text-left'>
                                                    <span className='dateFormat'>{admin.created_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(admin.created_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td>

                                                <td className='text-left'>
                                                    <span className='dateFormat'>{admin.modified_date}</span>
                                                    {(new Date(this.convertUTCDateToLocalDate(new Date(admin.modified_date)))).toLocaleDateString('en-US', DATE_OPTIONS)}
                                                </td>

                                                <td class='text-center'>
                                                    {/* <button type="button" className="btn btn-icon btn-sm" title="View"><i className="fa fa-eye"></i></button> */}
                                                    <NavLink to={`/staff/details/${admin.admin_id}`} className="text-info brandColor p-1" title="Edit"><i className="fa fa-pencil-square-o"></i></NavLink>                                                    
                                                    
                                                    <a href='javascript:void(0);' onClick={this.subAdminDelete()} data-id={admin.admin_id} className="brandColor p-1" title="Edit"><i className="fa fa-trash text-danger"></i></a>
                                                </td>
                                            </tr>
                                        ))
                                    }

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </Fragment>
        )
    }
}

export default StaffList
