import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import { API_URL } from '../../config/config'
import SweetAlert from 'react-bootstrap-sweetalert'
import 'sweetalert/dist/sweetalert.css';
import LoadingOverlay from 'react-loading-overlay';

export class SignIn extends Component {


    state = {
        email: '',
        password: '',
        showAlertIcon: '',
        showAlert: false,
        showConfirmationAlert: false,
        showAlertActionURL:'',
        showLoader: false,
        showProgaramTableMessage:''
    };


    componentDidMount() {
        if (window.localStorage.getItem('userSession')) {
            const memberSession = JSON.parse(window.localStorage.getItem('userSession'));
            if (memberSession.user_id) {
                window.location.href = '/dashboard'
            }
        }

    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };



    handleLogin = (e) => {


        e.preventDefault()

        const { email, password } = this.state
        if (!email) {
            //error
        }
        else if (!password) {

        }
        else {

            this.setState({ showLoader: true });
            axios({
                url: API_URL + "admin/login",
                method: 'post',
                data: {
                    email: email,
                    password: password
                },
                headers: {}
            })
                .then(response => {
                    this.setState({ showLoader: false });
                    const data = response.data;
                    const error_message = data.error_message;
                    if (data.error_code == '200') {
                        const user_id = data.user_id;
                        const auth_token = data.auth_token;
                        const first_name = data.admin.first_name;
                        const last_name = data.admin.last_name;
                        const fullname = first_name;
                        const full_name = fullname.trim();
                        const email = data.admin.email;

                        const userData = { user_id: user_id, auth_token: auth_token, email: email, full_name: full_name };
                        window.localStorage.setItem('userSession', JSON.stringify(userData));
                        window.location.href = '/dashboard'
                    }
                    else {
                        //alert("Error Message : "+ error_message);
                        this.setState({showAlertIcon : 'warning', showAlert: true, showAlertMessage: error_message });
                        //return false;
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }

    }

    sweetalertok = () => { 
        this.setState({ showAlert: false });
        if(this.state.showAlertActionURL)
        {
            window.location.href = this.state.showAlertActionURL; // '/dashboard';
        }        
    }

    render() {


        return (
            <Fragment>

                <LoadingOverlay
                    active={this.state.showLoader}
                    spinner
                    text='Please wait...'>
                </LoadingOverlay>

                <SweetAlert
                   type={this.state.showAlertIcon}                    
                   title={this.state.showAlertMessage}
                   onConfirm={this.sweetalertok}
                   onCancel={() => this.setState({ showAlert: false })}
                   show={this.state.showAlert}
                />
                <div className="auth option2">
                    <div className="auth_left">

                        <div className="card">
                            <div className="card-body">
                                <div className="text-center">
                                    <NavLink className="header-brand" to="/auth/signin">
                                    <img src="/assets/images/brand-full-logo.png" alt="small_logo" className='img-fluid' style={{marginBottom: '10px'}} />
                                    </NavLink>
                                    <div className="card-title mt-3">admin</div>
                                    {/* <button type="button" className="btn btn-facebook"><i className="fa fa-facebook mr-2"></i>Facebook</button>
                                <button type="button" className="btn btn-google"><i className="fa fa-google mr-2"></i>Google</button> */}
                                    {/* <h6 className="mt-3 mb-3">Or</h6> */}
                                </div>
                                <form onSubmit={this.handleLogin} >
                                    <div className="form-group">
                                        <input type="email" name="email" onChange={this.handleChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                                    </div>
                                    <div className="form-group">
                                        {/* <label className="form-label"><NavLink to="/auth/forgot-password" className="float-right small">I forgot password</NavLink></label> */}
                                        <input type="password" name="password" onChange={this.handleChange} className="form-control" id="exampleInputPassword1" placeholder="Password" />
                                    </div>
                                    <div className="form-group">
                                        <label className="custom-control custom-checkbox">
                                            <input type="checkbox" className="custom-control-input" />
                                            {/* <span className="custom-control-label">Remember me</span> */}
                                        </label>
                                    </div>
                                    <div className="text-center">
                                        <button type='submit' className="btn btn-brand btn-block" title="" >Sign in</button>
                                        {/* <div className="text-muted mt-4">Don't have account yet? <NavLink to="/auth/register">Sign up</NavLink></div> */}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default SignIn
