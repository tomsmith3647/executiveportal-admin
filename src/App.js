import React, { Component, Fragment } from 'react';
import './scss/main.css'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import SideNavbar from './components/layout/sidebar/SideNavbar';
import Loader from './components/layout/Loader';
import DashBoard from './components/dashboard/DashBoard';
import PageWrapper from './components/auxiliary/PageWrapper';
import Header from './components/header/Header';
import Breadcrumb from './components/breadcrumb/Breadcrumb';
import SectionBodyWrapper from './components/auxiliary/SectionBodyWrapper';
import SignIn from './components/auth/SignIn';
import Register from './components/auth/Register'
import ForgotPassword from './components/auth/ForgotPassword';
import Users from './components/users/Users';
import UserProfile from './components/users/UserProfile';
import AddUser from './components/users/AddUser';
//SubAdmin
 
 
import Program from './components/programs/Program';
import ProgramsList from './components/programs/ProgramsList';
import ProgramsDetail from './components/programs/ProgramsDetail';
import ProgramAdd from './components/programs/ProgramAdd';
import CategoryAdd from './components/programs/CategoryAdd';
import CategoryDetails from './components/programs/CategoryDetails';
import ModuleAdd from './components/programs/ModuleAdd';
import ModuleDetails from './components/programs/ModuleDetails';
import ItemAdd from './components/programs/ItemAdd';
import TaskAdd from './components/programs/TaskAdd';
import ItemUpdate from './components/programs/ItemEdit';
import TaskUpdate from './components/programs/TaskEdit';
import TranscriptUpdate from './components/programs/TranscriptEdit';
import EditReferral from './components/users/EditReferral';
import ResourceAdd from './components/programs/ResourceAdd';
import ResourceUpdate from './components/programs/ResourceUpdate';
import RoleAdd from './components/roles/RoleAdd';
import RoleDetails from './components/roles/RoleDetails';
import RoleList from './components/roles/RoleList';
import LinkDetails from './components/link/LinkDetails';
import LinkList from './components/link/LinkList';
import LinkAdd from './components/link/LinkAdd';
import DashboardEditReferral from './components/dashboard/DashboardEditReferral';
import DashBoardMember from './components/dashboard/DashBoardMember';
import FaqList from './components/faq/FaqList';
import FaqDetails from './components/faq/FaqDetails';
import FaqAdd from './components/faq/FaqAdd';
import FaqCategoryAdd from './components/faq/FaqCategoryAdd';
import FaqCategoryList from './components/faq/FaqCategoryList';
import FaqCategoryUpdate from './components/faq/FaqCategoryUpdate';
import DashboardEditSales from './components/dashboard/DashboardEditSales';
import DashboardReviewDetails from './components/dashboard/DashboardReviewDetails';
import LinkEdit from './components/link/LinkEdit';
import DashboardEditSalesTest from './components/dashboard/DashboardEditSalesTest';
import LinkIframe from './components/link/LinkIframe';
import EmailSupport from './components/instructions/EmailSupport';
import NotificationList from './components/notification/NotificationList';
import NotificationAdd from './components/notification/NotificationAdd';
import NotificationEdit from './components/notification/NotificationEdit';
import NotificationDetails from './components/notification/NotificationDetails';
import EventAdd from './components/events/EventAdd';
import EventList from './components/events/EventList';
import EventCategoryList from './components/events/EventCategoryList';
import EventCategoryAdd from './components/events/EventCategoryAdd';
import EventCategoryEdit from './components/events/EventCategoryEdit';
import EventCategoryDetails from './components/events/EventCategoryDetails';
import EventEdit from './components/events/EventEdit';
import EventDetails from './components/events/EventDetails';
import StaffRoleAdd from './components/staffroles/StaffRoleAdd';
import StaffRoleDetails from './components/staffroles/StaffRoleDetails';
import StaffRoleList from './components/staffroles/StaffRoleList';
import StaffProfile from './components/staff/StaffProfile';
import AddStaff from './components/staff/AddStaff';
import StaffList from './components/staff/StaffList';





export class App extends Component {


  state = {
    islogin: localStorage.getItem('userSession')
  }

  render() {



    const { islogin } = this.state

    return (
      <Router>
        {/* 
        <Route exact path='/auth/register' component={Register} /> */}

        <Route exact path='/auth/forgot-password' component={ForgotPassword} />
        <Route exact path='/auth/signin' component={SignIn} />

        {!islogin ? (
          <Redirect to='/auth/signin' />
        ) : (
            <Fragment>
              <SideNavbar />
              <PageWrapper classes='header-sticky'>
                <SectionBodyWrapper id='page_top'>
                  <Header />
                </SectionBodyWrapper>
                <SectionBodyWrapper>
                  <Breadcrumb />
                </SectionBodyWrapper>
              </PageWrapper>


              <Switch>
                <PageWrapper>
                  <SectionBodyWrapper classes='mt-4 content-wrapper'>
                    <Route exact path='/' component={DashBoard} />                    
                    <Route exact path='/dashboard' component={DashBoard} />
                    <Route exact path='/dashboard/:member_id' component={DashBoardMember} />                                  
                    {/* Auth */}
                    <Route exact path='/programs' component={ProgramsList} />
                    <Route path='/program/details/:program_id' component={ProgramsDetail} />
                    <Route exact path='/program/add' component={ProgramAdd} />                     
                    <Route exact path='/program/category/add/:program_id' component={CategoryAdd} />
                    <Route exact path='/program/category/details/:category_id' component={CategoryDetails} />
                    <Route exact path='/program/category/module/add/:category_id' component={ModuleAdd} />
                    <Route exact path='/program/category/module/details/:module_id' component={ModuleDetails} /> 
                    <Route exact path='/program/category/item/add/:category_id' component={ItemAdd} />
                    <Route exact path='/program/category/item/update/:item_id' component={ItemUpdate} />                    
                    <Route exact path='/program/category/module/topic/add/:module_id' component={TaskAdd} />                     
                    <Route exact path='/program/category/module/topic/update/:task_id' component={TaskUpdate} />
                    <Route exact path='/program/category/module/transcript/details/:module_id' component={TranscriptUpdate} />   
                    <Route exact path='/program/category/module/resource/add/:module_id' component={ResourceAdd} />
                    <Route exact path='/program/category/module/resource/update/:resource_id' component={ResourceUpdate} />   
                    <Route exact path='/users' component={Users} />
                    <Route path='/user/detail/:member_id' component={UserProfile} />
                    <Route path='/users/:member_id' component={UserProfile} />
                    <Route exact path='/user/add' component={AddUser} />
                    <Route path='/user/referral/update/:member_id/:referral_code' component={EditReferral} />
                    

                    <Route exact path='/staff/add' component={AddStaff} />
                    <Route path='/staff/details/:admin_id' component={StaffProfile} />
                    <Route exact path='/staff/list' component={StaffList} />

                    {/* <Route exact path='/program' component={Program} /> */}
                    <Route exact path='/role/create' component={RoleAdd} />
                    <Route exact path='/role/details/:role_id' component={RoleDetails} />
                    <Route exact path='/roles' component={RoleList} />
                    <Route exact path='/faq/create' component={FaqAdd} />
                    <Route exact path='/faq/details/:faq_id' component={FaqDetails} />
                    <Route exact path='/faqs' component={FaqList} />
                    <Route exact path='/faq/category/add' component={FaqCategoryAdd} />
                    <Route exact path='/faq/category/update/:faq_category_id' component={FaqCategoryUpdate} />
                    <Route exact path='/faq/category' component={FaqCategoryList} />


                    <Route exact path='/link/add' component={LinkAdd} />
                    <Route exact path='/link/details/:link_id' component={LinkDetails} />
                    <Route exact path='/link/edit/:link_id' component={LinkEdit} />

                    <Route exact path='/link/open/:link_id' component={LinkIframe} />

                    

                    <Route exact path='/links' component={LinkList} />

                    <Route exact path='/notifications' component={NotificationList} />
                    <Route exact path='/notification/add' component={NotificationAdd} />
                    <Route exact path='/notification/edit/:notification_id' component={NotificationEdit} />
                    <Route exact path='/notification/details/:notification_id' component={NotificationDetails} />



                    <Route exact path='/events' component={EventList} />
                    <Route exact path='/event/add/' component={EventAdd} />
                    <Route exact path='/event/edit/:event_id' component={EventEdit} />
                    <Route exact path='/event/details/:event_id' component={EventDetails} />

                   
                    <Route exact path='/event/categories' component={EventCategoryList} />
                    <Route exact path='/event/category/add' component={EventCategoryAdd} />
                    <Route exact path='/event/category/edit/:category_id' component={EventCategoryEdit} />
                    <Route exact path='/event/category/details/:category_id' component={EventCategoryDetails} />

                   


                    <Route exact path='/instruction/emailsupport' component={EmailSupport} />
                    
                    <Route path='/referral/update/:referral_code' component={DashboardEditReferral} />
                    <Route path='/member/referral/update/:member_id/:referral_code' component={DashboardEditReferral} />
                    
                    <Route path='/boom/sales/update/:boom_id' component={DashboardEditSales} />
                    <Route path='/member/boom/sales/update/:member_id/:boom_id' component={DashboardEditSales} />

                    <Route path='/test/boom/sales/update/:boom_id' component={DashboardEditSalesTest} />
                    
                    
                    

                    <Route path='/dashboard/review/details/:review_id' component={DashboardReviewDetails} />
                    <Route path='/dashboard/review/details/:member_id/:review_id' component={DashboardReviewDetails} />


                    <Route exact path='/staff/role/create' component={StaffRoleAdd} />
                    <Route exact path='/staff/role/details/:role_id' component={StaffRoleDetails} />
                    <Route exact path='/staff/roles' component={StaffRoleList} />
                    
                    {/* <Route path='/users/:user' component={} /> */}
                  </SectionBodyWrapper>
                </PageWrapper>
              </Switch>
            </Fragment>
          )}


      </Router>
    )
  }
}

export default App
